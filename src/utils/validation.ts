import { RuleObject } from 'ant-design-vue/lib/form/interface'

/**
 * 验证是否为整数
 * @param {string} value 待验证的值
 */
export function checkInteger(value: string): boolean {
  if (value === '' || value === null || value === undefined) return true

  const reg = /^\d*$/
  return reg.test(value)
}

/**
 * 验证是否有效的货值，整数或两位小数
 * @param {string} value 待验证的值
 */
export function checkValidCurrency(value: string): boolean {
  if (value === '' || value === null || value === undefined) return true

  const reg = /^\d+(.\d{1,2})?$/
  return reg.test(value)
}

/**
 * 验证数字值的有效范围
 * @param {string} value 待验证的值
 * @param {number} min 最小值
 * @param {number} max 最大值
 */
export function checkNumberRange(value: string, min = Number.MIN_VALUE, max = Number.MAX_VALUE): boolean {
  if (value === '' || value === null) return true

  const numValue = Number(value)

  return numValue > Number(min) && numValue < Number(max)
}

/**
 * 产生验证规则，请应用单一指责设计模式进行验证
 * @param {Function} checkFunction 验证函数
 * @param  {...any} args 验证函数参数
 */
export function generateRule(
  checkFunction: (value: string) => boolean
): (rule: RuleObject, value: string) => Promise<void> {
  return async (rule: RuleObject, value: string) => {
    if (checkFunction(value)) {
      return Promise.resolve()
    } else {
      return Promise.reject(rule.message)
    }
  }
}

/**
 * 验证是否手机号
 * @param {string} value 待验证的值
 */
export function checkPhoneNumber(value: string): boolean {
  if (value === '' || value === null) return true

  const reg = /^[1][3,4,5,6,7,8,9][0-9]{9}$/
  return reg.test(value)
}

/**
 * 验证身份证号码
 * @param {string} value 待验证的值
 */
export function checkIdCardNumber(value: string): boolean {
  if (value === '' || value === null) return true

  const reg = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/
  return reg.test(value)
}
/**
 * 验证16进制颜色
 * @param {string} value 待验证的值
 */
export function checkColorHexadecimal(value: string): boolean {
  if (value === '' || value === null) return true

  const reg = /^#([A-F\d]{6}|[A-F\d]{3})$/i
  return reg.test(value)
}
