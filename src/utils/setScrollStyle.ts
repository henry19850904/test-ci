/**
 * Windows系统浏览器添加滚动条样式修改
 * PS: 火狐浏览器仅支持两个属性的修改,修改有限,且为实验性功能,未来版本可能有变化
 */
if (
  window.navigator.userAgent.match(/Windows|Firefox/g) !== null &&
  window.navigator.userAgent.match(/Windows|Firefox/g)?.length === 2
) {
  const style = document.createElement('style')
  style.innerHTML = `
  *{
    scrollbar-width: thin;
    scrollbar-color: #d9d8d8 transparent;
  }
  `
  document.getElementsByTagName('head')[0].appendChild(style)
} else if (
  window.navigator.userAgent.match(/Windows/g) !== null &&
  window.navigator.userAgent.match(/Windows/g)?.length !== 0
) {
  const style = document.createElement('style')
  style.innerHTML = `
  ::-webkit-scrollbar{
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-thumb{
    border-radius: 4px;
    background-color: transparent;
  }
  ::-webkit-scrollbar-track{
    border-radius: 4px;
    background-color: transparent;
  }
  :hover::-webkit-scrollbar-thumb {
    background-color: #d9d8d8;
    border-radius: 4px;
  }
  `
  document.getElementsByTagName('head')[0].appendChild(style)
}
