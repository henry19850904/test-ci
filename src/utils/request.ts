import axios, { AxiosError } from 'axios'
import store from '@/store'
import storage from 'store'
import { notification } from 'ant-design-vue'
import { ACCESS_TOKEN } from '@/store/mutationTypes'
import sha256 from 'crypto-js/sha256'
import { v4 as uuidV4 } from 'uuid'
import { API_HOST } from '@/config/http'

// const SUCCESS_CODE = 0
// 创建 axios 实例
const request = axios.create({
  // API 请求的默认前缀
  baseURL: API_HOST + process.env.VUE_APP_API_BASE_URL,
  timeout: 6000, // 请求超时时间
})

// 异常拦截处理器
const errorHandler = (error: AxiosError) => {
  if (!error.response) {
    const description = '抱歉, 出错啦。请您检查网络或刷新页面。'
    notification.error({
      message: 'Exception',
      description,
    })
  }

  if (error.response) {
    let { code } = error.response.data
    const { msg = '服务端异常，请联系技术支持', data } = error.response.data

    // 从 localstorage 获取 token
    const token = storage.get(ACCESS_TOKEN)
    if (error.response.status === 403) {
      notification.error({
        message: 'Forbidden',
        description: msg,
      })
    }
    if (error.response.status === 401) {
      notification.error({
        message: 'Unauthorized',
        description: 'Authorization verification failed',
      })
      if (token) {
        store.dispatch('logout').then(() => {
          setTimeout(() => {
            window.location.reload()
          }, 1500)
        })
      }
    } else {
      // 后台未能捕获异常
      if (!code) {
        code = error.response.status
      }

      notification.error({
        message: 'Error',
        description: msg,
      })

      return Promise.resolve({ data: { code, msg, data } })
    }
  }
  return Promise.reject(error)
}

// request interceptor
request.interceptors.request.use(config => {
  const token = storage.get(ACCESS_TOKEN)
  const appId = '5dfc3007c3c24e4fb65daf49c27c5f6a' // 固定值
  const timestamp = new Date().getTime() // 时间戳
  const seqId = uuidV4() // uuid
  const sha = `App-Id=${appId}&Seq-Id=${seqId}&Timestamp=${timestamp}&`
  // 如果 token 存在
  // 让每个请求携带自定义 token 请根据实际情况自行修改
  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`
  }
  config.headers['Seq-Id'] = seqId
  config.headers['App-Id'] = appId
  config.headers['Timestamp'] = timestamp
  config.headers['Sign'] = sha256(sha).toString()
  return config
}, errorHandler)

// response interceptor
request.interceptors.response.use(response => {
  return response
}, errorHandler)

export default request
