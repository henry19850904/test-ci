import { ref, watch } from 'vue'

// eslint-disable-next-line
export default function useModalControl() {
  const modalVisible = ref(false)
  const modalVisibleDelay = ref(false)

  // 解决无法彻底销毁modal中元素问题
  watch(modalVisible, newVal => {
    if (newVal === false) {
      setTimeout(() => {
        modalVisibleDelay.value = newVal
      }, 500)
      return
    }
    modalVisibleDelay.value = newVal
  })

  return { modalVisible, modalVisibleDelay }
}
