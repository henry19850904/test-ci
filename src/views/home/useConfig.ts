import { computed } from 'vue'
import { useStore } from '@/store'
import {
  CHANGE_HOME_CONFIG,
  COMMIT_HOME_CONFIG,
  CANCEL_HOME_CONFIG,
  RESET_HOME_CONFIG,
  TOGGLE_CONFIG_ITEM,
  GET_HOME_CONFIG,
  changeConfig,
  toggleParams,
} from '@/store/modules/home'

// eslint-disable-next-line
export default function useConfig() {
  const store = useStore()

  const config = computed(() => store.state.home.homeConfig)

  const defaultConfig = computed(() => store.state.home.defaultConfig)

  const getConfig = () => store.dispatch(`home/${GET_HOME_CONFIG}`)

  const changeConfigOrder = (payload: changeConfig) => store.dispatch(`home/${CHANGE_HOME_CONFIG}`, payload)

  const commitConfig = () => store.dispatch(`home/${COMMIT_HOME_CONFIG}`)

  const cancelConfig = () => store.commit(`home/${CANCEL_HOME_CONFIG}`)

  const resetConfig = () => store.commit(`home/${RESET_HOME_CONFIG}`)

  const toggleConfigItem = (params: toggleParams) => store.commit(`home/${TOGGLE_CONFIG_ITEM}`, params)

  return {
    config,
    defaultConfig,
    getConfig,
    changeConfigOrder,
    toggleConfigItem,
    commitConfig,
    cancelConfig,
    resetConfig,
  }
}
