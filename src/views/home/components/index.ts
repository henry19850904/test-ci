import SubscribeNews from './SubscribeNews.vue'
import WritingStatistics from './WritingStatistics.vue'
import WritingTasks from './WritingTasks.vue'
import MyNotes from './MyNotes.vue'
import HotWords from './hot-words/index.vue'
import MessageCenter from './MessageCenter.vue'

export { SubscribeNews, WritingStatistics, WritingTasks, MyNotes, HotWords, MessageCenter }
