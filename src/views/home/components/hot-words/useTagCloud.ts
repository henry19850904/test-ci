import DataSet from '@antv/data-set'
import { registerShape, Chart, Util } from '@antv/g2'
import imgUrl from './tagCloudShape.png'
/* eslint-disable */
export interface TagCloudProps {
  data: {
    name: string
    value: string | number
  }[]
  height?: number
  onClick: Function
}

// function createImgMask() {
//   const canvas = document.createElement('canvas')
//   canvas.width = 300
//   canvas.height = 150
//   const context = canvas.getContext('2d')
//   context && (context.lineWidth = 10)
//   context && (context.strokeStyle = 'black')
//   ParamEllipse(context, 150, 75, 140, 70) //椭圆
//   function ParamEllipse(context: CanvasRenderingContext2D | null, x: number, y: number, a: number, b: number) {
//     //max是等于1除以长轴值a和b中的较大者
//     //i每次循环增加1/max，表示度数的增加
//     //这样可以使得每次循环所绘制的路径（弧线）接近1像素
//     var step = a > b ? 1 / a : 1 / b
//     if (!context) return
//     context.beginPath()
//     context.moveTo(x + a, y) //从椭圆的左端点开始绘制
//     for (var i = 0; i < 2 * Math.PI; i += step) {
//       //参数方程为x = a * cos(i), y = b * sin(i)，
//       //参数为i，表示度数（弧度）
//       context.lineTo(x + a * Math.cos(i), y + b * Math.sin(i))
//     }
//     context.closePath()
//     context.fill()
//     context.stroke()
//   }
//   return canvas.toDataURL()
// }

export default function useTagCloud(container: string, props: TagCloudProps) {
  const { data } = props
  function getTextAttrs(cfg: any) {
    return {
      ...cfg.defaultStyle,
      ...cfg.style,
      fontSize: cfg.data.size,
      text: cfg.data.text,
      textAlign: 'center',
      fontFamily: cfg.data.font,
      fill: cfg.color || cfg.defaultStyle.stroke,
      textBaseline: 'Alphabetic',
    }
  }
  // 给point注册一个词云的shape
  registerShape('point', 'cloud', {
    draw(cfg: any, container) {
      const attrs = getTextAttrs(cfg)
      const textShape = container.addShape('text', {
        attrs: {
          ...attrs,
          x: cfg.x,
          y: cfg.y,
        },
      })
      if (cfg.data.rotate) {
        Util.rotate(textShape, (cfg.data.rotate * Math.PI) / 180)
      }
      return textShape
    },
  })

  const imageMask = new Image()
  imageMask.crossOrigin = ''
  imageMask.src = imgUrl //createImgMask()
  imageMask.onload = render
  // TODO:响应式优化
  function render() {
    const dv = new DataSet.View().source(data)
    const range = dv.range('value')
    const min = range[0]
    const max = range[1]
    const textColors = [
      '#58975f',
      '#ff5c47',
      '#01808c',
      '#89a522',
      '#ba5153',
      '#6236ff',
      '#007ae4',
      '#ffbb41',
      '#ffbb41',
      '#ff8576',
      '#c80000',
    ].join('-')
    dv.transform({
      type: 'tag-cloud',
      fields: ['name', 'value'],
      imageMask,
      size: [280, 110],
      font: 'PingFangSC-Regular',
      padding: 0,
      rotate: () => 0,
      fontSize(d) {
        if (d.value) {
          return ((d.value - min) / (max - min)) * (18 - 10) + 10
        }
        return 0
      },
      fontWeight(d) {
        if (d.value) {
          return ((d.value - min) / (max - min)) * (500 - 400) + 400
        }
        return 400
      },
    })
    const chart = new Chart({
      container,
      autoFit: false,
      width: 280,
      height: 110,
      padding: 0,
    })
    chart.data(dv.rows)
    chart.scale({
      x: { nice: false },
      y: { nice: false },
    })
    chart.legend(false)
    chart.axis(false)
    chart.tooltip(false)
    chart.coordinate().reflect('x')
    chart
      .point()
      .position('x*y')
      .color('text', textColors)
      .style({
        stroke: '#fff',
      })
      .shape('cloud')
    chart.interaction('element-active', {
      start: [{ trigger: 'element:mouseenter', action: 'cursor:pointer' }],
    })
    // eslint-disable-next-line
    chart.on('point:click', (ev: any) => {
      props.onClick(ev.data.data)
    })
    chart.render()
  }
}
