import { onMounted, ref } from 'vue'
import { Business } from '@/models/hotNews'
import hotNewsApi from '@/api/hotNews'

// eslint-disable-next-line
export default function useFiltrate() {
  const noteList = ref<Business[]>([])
  onMounted(async () => {
    noteList.value = (await hotNewsApi.getNoteList()).data.list
  })

  return {
    noteList,
  }
}
