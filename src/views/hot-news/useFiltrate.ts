import { onMounted, ref } from 'vue'
import { Business } from '@/models/hotNews'
import hotNewsApi from '@/api/hotNews'

// eslint-disable-next-line
export default function useFiltrate() {
  const searchTime = ref('1')
  const businessSelect = ref('all')
  const territorySelect = ref('all')
  const businessList = ref<Business[]>([])
  const territoryList = ref<Business[]>([])
  const commonFiltrateList = ref<Business[]>([])
  const businessActive = ref(false)
  const territoryActive = ref(false)
  function collapse(type: string): void {
    if (type === 'business') businessActive.value = !businessActive.value
    if (type === 'territory') territoryActive.value = !territoryActive.value
  }
  onMounted(async () => {
    businessList.value = (await hotNewsApi.getBusiness()).data.list
    territoryList.value = (await hotNewsApi.getTerritory()).data.list
    commonFiltrateList.value = (await hotNewsApi.getCommonFiltrate()).data.list
  })

  return {
    searchTime,
    businessSelect,
    territorySelect,
    businessList,
    territoryList,
    commonFiltrateList,
    businessActive,
    territoryActive,
    collapse,
  }
}
