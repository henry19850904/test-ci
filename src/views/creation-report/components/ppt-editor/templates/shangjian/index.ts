import { TemplatePages } from '@/models/pptTemplate'
const shangjian: TemplatePages = {
  name: '熵简模板',
  key: 'shangjian',
  pageTypes: [
    {
      key: 'cover',
      name: '封面',
      background: 'report/template/shangjian/cover.png',
      textboxes: [
        {
          key: 'title',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 1.625em; color: #354052;">研报主标题</span></p>',
          config: {
            x: 216,
            y: 312,
            width: 208,
            height: 36,
          },
          textStyle: {
            'font-size': '1.625em',
            color: '#354052',
          },
        },
        {
          key: 'subtitle',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 1.625em; color: #b74857">研报副标题</span></p>',
          config: {
            x: 504,
            y: 312,
            width: 208,
            height: 36,
          },
          textStyle: {
            'font-size': '1.625em',
            color: '#b74857',
          },
        },
        {
          key: 'text',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 0.875em; color:#7f8fa4;">补充说明</span></p>',
          config: {
            x: 216,
            y: 379,
            width: 99,
            height: 22,
          },
          textStyle: {
            'font-size': '0.875em',
            color: '#7f8fa4',
          },
        },
      ],
    },
    {
      key: 'content1',
      name: '内容1',
      background: 'report/template/shangjian/content.png',
      textboxes: [
        {
          key: 'title',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 1.625em; color: #b74857">大标题</span></p>',
          config: {
            x: 20,
            y: 20,
            width: 186,
            height: 36,
          },
          textStyle: {
            'font-size': '1.625em',
            color: '#b74857',
          },
        },
        {
          key: 'subtitle',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 1.125em; color: #354052;">副标题</span></p>',
          config: {
            x: 20,
            y: 63,
            width: 111,
            height: 28,
          },
          textStyle: {
            'font-size': '1.125em',
            color: '#354052',
          },
        },
        {
          key: 'caption_title',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 0.875em; color: #354052">章节标题</span></p>',
          config: {
            x: 20,
            y: 555,
            width: 89,
            height: 22,
          },
          textStyle: {
            'font-size': '0.875em',
            color: '#354052',
          },
        },
        {
          key: 'caption_text',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 0.75em; color: #7f8fa4">章节文字</span></p>',
          config: {
            x: 20,
            y: 587,
            width: 600,
            height: 54,
          },
          textStyle: {
            'font-size': '0.75em',
            color: '#7f8fa4',
          },
        },
      ],
      indicators: [
        {
          indicatorId: null,
          indicator: null,
          config: {
            x: 50,
            y: 205,
            width: 280,
            height: 202,
          },
        },
        {
          indicatorId: null,
          indicator: null,
          config: {
            x: 471,
            y: 205,
            width: 280,
            height: 202,
          },
        },
      ],
      images: [
        {
          id: 'sadsa',
          src: 'https://img0.baidu.com/it/u=2676935521,922112450&fm=11&fmt=auto&gp=0.jpg',
          config: {
            x: 180,
            y: 20,
            width: 180,
            height: 136,
          },
        },
      ],
    },
    {
      key: 'content2',
      name: '内容2',
      background: 'report/template/shangjian/content.png',
      textboxes: [
        {
          key: 'title',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 1.625em; color: #b74857">大标题</span></p>',
          config: {
            x: 20,
            y: 20,
            width: 186,
            height: 36,
          },
          textStyle: {
            'font-size': '1.625em',
            color: '#b74857',
          },
        },
        {
          key: 'subtitle',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 1.125em; color: #354052;">副标题</span></p>',
          config: {
            x: 20,
            y: 63,
            width: 111,
            height: 28,
          },
          textStyle: {
            'font-size': '1.125em',
            color: '#354052',
          },
        },
        {
          key: 'caption_title',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 0.875em; color: #354052">章节标题</span></p>',
          config: {
            x: 20,
            y: 555,
            width: 89,
            height: 22,
          },
          textStyle: {
            'font-size': '0.875em',
            color: '#354052',
          },
        },
        {
          key: 'caption_text',
          text: '',
          id: '',
          placeholder: '<p><span style="font-size: 0.75em; color: #7f8fa4">章节文字</span></p>',
          config: {
            x: 20,
            y: 587,
            width: 860,
            height: 54,
          },
          textStyle: {
            'font-size': '0.75em',
            color: '#7f8fa4',
          },
        },
      ],
      indicators: [
        {
          indicatorId: null,
          indicator: null,
          config: {
            x: 45,
            y: 155,
            width: 610,
            height: 302,
          },
        },
      ],
    },
    {
      key: 'end',
      name: '尾页',
      background: 'report/template/shangjian/end.png',
    },
  ],
}
export default shangjian
