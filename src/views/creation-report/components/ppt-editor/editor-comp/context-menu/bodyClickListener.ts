/* eslint-disable */

/**
 * When listening for an outside click, we set useCapture = true.
 * This way, we can prevent other click listeners from firing when performing the 'click-out'.
 * If useCapture is set to false, the handlers fire backwards
 */
function createBodyClickListener(fn: any) {
  let isListening = false

  /* === public api ========================================== */
  return {
    get isListening() {
      return isListening
    },

    start(cb: any) {
      window.addEventListener('click', _onclick, true)
      window.addEventListener('keyup', _onescape, true)
      isListening = true
      if (typeof cb === 'function') cb()
    },

    stop(cb: any) {
      window.removeEventListener('click', _onclick, true)
      window.removeEventListener('keyup', _onescape, true)
      isListening = false
      if (typeof cb === 'function') cb()
    },
  }

  /* === private helpers ===================================== */
  function _onclick(e: MouseEvent) {
    e.preventDefault()
    if (typeof fn === 'function') fn(e)
  }

  function _onescape(e: any) {
    if (e.key === 'Escape') _onclick(e)
  }
}

export default createBodyClickListener
