/* eslint-disable */
import PageDraggable from "./PageDraggable.vue";

export default {
  props: {
    preview: {
      type: Boolean,
      default: false
    },
    type: {
      type: String
    }
  },
  components: {
    PageDraggable
  },
  methods: {
    clearActive() {
      this.$refs.draggable.clearActive();
    }
  }
};
