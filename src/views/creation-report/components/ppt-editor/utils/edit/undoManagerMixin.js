/* eslint-disable */
import UndoManager from 'undo-manager'
export default {
  data() {
    return {
      undoManager: null,
      disableUndo: true,
      disableRedo: true,
    }
  },
  methods: {
    undo() {
      this.undoManager.undo()
    },
    redo() {
      this.undoManager.redo()
    },
    clear() {
      this.undoManager.clear()
    },
    undoCallback() {
      this.disableUndo = !this.undoManager.hasUndo()
      this.disableRedo = !this.undoManager.hasRedo()
    },
  },
  created() {
    this.undoManager = new UndoManager()
    this.undoManager.setCallback(this.undoCallback)
  },
}
