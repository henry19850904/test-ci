/* eslint-disable */
import saveShortKeyMixin from './saveShortKeyMixin'
export default {
  mixins: [saveShortKeyMixin],
  data() {
    return {
      fromTemp: false,
    }
  },
  methods: {
    handleUnload(event) {
      if (this.checkNeedSave()) {
        this.saveDataToLocalStorage()
        event.preventDefault()
        event.returnValue = ''
      }
    },

    checkNeedSave() {
      return false
    },

    saveDataToLocalStorage() {
      console.log('saveDataToLocalStorage')
    },
    giveUpSave() {
      console.log('giveUpSave')
    },
  },
  async beforeRouteLeave(to, from, next) {
    const needSave = this.checkNeedSave()
    if (!needSave || to.name === 'login') {
      next()
      return
    }
    try {
      await this.$SjConfirm(`离开将丢失尚未保存的编辑资料`, null, {
        confirmText: '去保存',
        cancelText: '直接离开',
      })
      try {
        await this.save(false)
        next()
      } catch (e) {
        next(false)
      }
    } catch {
      await this.giveUpSave()
      next()
    }
  },
  created() {
    window.addEventListener('beforeunload', this.handleUnload)
  },
  beforeUnmount() {
    window.removeEventListener('beforeunload', this.handleUnload)
  },
}
