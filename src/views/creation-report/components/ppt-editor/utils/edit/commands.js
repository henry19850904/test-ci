/* eslint-disable */
/**
 * ppt 操作
 */
import undoManagerMixin from './undoManagerMixin'
import { isEqual } from 'lodash'

export default {
  mixins: [undoManagerMixin],
  methods: {
    //commands
    changeFileName(newFileName) {
      const oldFileName = this.fileName
      this.fileName = newFileName
      this.undoManager.add({
        undo: () => {
          this.fileName = oldFileName
        },
        redo: () => {
          this.fileName = newFileName
        },
      })
    },

    changeCurrentPage(newPageId) {
      this.selectedPageId = newPageId
    },

    insertPage(page, insertIndex) {
      const oldPageId = this.selectedPageId

      const insert = () => {
        this.pages.splice(insertIndex, 0, page)
        this.selectedPageId = page.pageId
      }

      insert()

      this.undoManager.add({
        undo: () => {
          this.pages.splice(insertIndex, 1)
          this.selectedPageId = oldPageId
        },
        redo: () => {
          insert()
        },
      })
    },

    removePage(pageIndex) {
      const oldSelectedPageId = this.selectedPageId
      const oldPageIndex = this.selectedPageIndex
      const oldPage = this.pages[pageIndex]
      const remove = () => {
        this.pages.splice(pageIndex, 1)
        if (this.pages.length === 0) {
          this.selectedPageId = null
        } else {
          if (oldPageIndex === pageIndex) {
            if (pageIndex >= this.pages.length) {
              this.selectedPageId = this.pages[this.pages.length - 1].pageId
            } else {
              this.selectedPageId = this.pages[pageIndex].pageId
            }
          }
        }
      }
      remove()
      this.undoManager.add({
        undo: () => {
          this.pages.splice(oldPageIndex, 0, oldPage)
          this.selectedPageId = oldSelectedPageId
        },
        redo: () => {
          remove()
        },
      })
    },

    changePageOrder(newIndex, oldIndex) {
      const move = () => {
        this.pages.splice(newIndex, 0, this.pages.splice(oldIndex, 1)[0])
      }
      move()
      this.undoManager.add({
        undo: () => {
          this.pages.splice(oldIndex, 0, this.pages.splice(newIndex, 1)[0])
        },
        redo: () => {
          move()
        },
      })
    },

    removeElement(type, index) {
      const selectedPage = this.pages[this.selectedPageIndex]
      let removedItem
      if (type === 'indicator') {
        removedItem = selectedPage.indicators[index]
      }
      if (type === 'textbox') {
        removedItem = selectedPage.textboxes[index]
      }
      if (type === 'image') {
        removedItem = selectedPage.images[index]
      }
      const remove = () => {
        if (type === 'indicator') {
          selectedPage.indicators.splice(index, 1)
        }
        if (type === 'textbox') {
          selectedPage.textboxes.splice(index, 1)
        }
        if (type === 'image') {
          selectedPage.images.splice(index, 1)
        }
      }
      remove()
      this.undoManager.add({
        undo: () => {
          if (type === 'indicator') {
            selectedPage.indicators.splice(index, 0, removedItem)
          }
          if (type === 'textbox') {
            selectedPage.textboxes.splice(index, 0, removedItem)
          }
          if (type === 'image') {
            selectedPage.images.splice(index, 0, removedItem)
          }
        },
        redo: () => {
          remove()
        },
      })
    },

    addTextBox(textbox) {
      const selectedPage = this.pages[this.selectedPageIndex]
      const add = () => {
        selectedPage.textboxes.push(textbox)
      }

      add()

      this.undoManager.add({
        undo: () => {
          const index = selectedPage.textboxes.indexOf(textbox)
          selectedPage.textboxes.splice(index, 1)
        },
        redo: () => {
          add()
        },
      })
    },

    changeTextbox(index, text) {
      const textbox = this.pages[this.selectedPageIndex].textboxes[index]
      const oldText = textbox.text
      if (oldText === text) {
        return
      }
      const change = () => {
        this.$set(textbox, 'text', text)
      }
      change()
      this.undoManager.add({
        undo: () => {
          this.$set(textbox, 'text', oldText)
        },
        redo: () => {
          change()
        },
      })
    },

    addImageBox(imageBox) {
      const selectedPage = this.pages[this.selectedPageIndex]
      const add = () => {
        selectedPage.images.push(imageBox)
      }

      add()

      this.undoManager.add({
        undo: () => {
          const index = selectedPage.images.indexOf(imageBox)
          selectedPage.images.splice(index, 1)
        },
        redo: () => {
          add()
        },
      })
    },

    changeImageSrc(index, imageSrc) {
      const imageBox = this.pages[this.selectedPageIndex].images[index]
      const oldSrc = imageBox.src
      const change = () => {
        this.$set(imageBox, 'src', imageSrc)
      }
      change()
      this.undoManager.add({
        undo: () => {
          this.$set(imageBox, 'src', oldSrc)
        },
        redo: () => {
          change()
        },
      })
    },

    addIndicator(indicator) {
      const selectedPage = this.pages[this.selectedPageIndex]
      const add = () => {
        selectedPage.indicators.push(indicator)
      }

      add()

      this.undoManager.add({
        undo: () => {
          const index = selectedPage.indicators.indexOf(indicator)
          selectedPage.indicators.splice(index, 1)
        },
        redo: () => {
          add()
        },
      })
    },

    changeIndicator(index, { indicator_id, timeRange, selectedTimeIndex }) {
      const indicator = this.pages[this.selectedPageIndex].indicators[index]
      const oldIndicatorId = indicator.indicatorId
      const oldRangeValue = indicator.rangeValue
      const oldSelectedTimeIndex = indicator.selectedTimeIndex
      if (
        indicator_id === oldIndicatorId &&
        isEqual(oldRangeValue, timeRange) &&
        selectedTimeIndex === oldSelectedTimeIndex
      )
        return
      const change = () => {
        this.$set(indicator, 'indicatorId', indicator_id)
        this.$set(indicator, 'rangeValue', timeRange)
        this.$set(indicator, 'selectedTimeIndex', selectedTimeIndex)
      }
      change()
      this.undoManager.add({
        undo: () => {
          this.$set(indicator, 'indicatorId', oldIndicatorId)
          this.$set(indicator, 'rangeValue', oldRangeValue)
          this.$set(indicator, 'selectedTimeIndex', oldSelectedTimeIndex)
        },
        redo: () => {
          change()
        },
      })
    },

    updateElement(type, index, config) {
      const selectedPage = this.pages[this.selectedPageIndex]
      let item
      if (type === 'indicator') {
        item = selectedPage.indicators[index]
      }
      if (type === 'textbox') {
        item = selectedPage.textboxes[index]
      }
      if (type === 'image') {
        item = selectedPage.images[index]
      }

      if (!item) {
        console.error('updateElement: 未知的node type:', type)
        return
      }
      const oldConfig = item.config
      const update = () => {
        this.$set(item, 'config', config)
      }
      update()
      this.undoManager.add({
        undo: () => {
          this.$set(item, 'config', oldConfig)
        },
        redo: () => {
          update()
        },
      })
    },
  },
}
