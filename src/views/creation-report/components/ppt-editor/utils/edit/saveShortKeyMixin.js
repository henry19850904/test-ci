/* eslint-disable */
export default {
  computed: {
    isWin() {
      return this.$store.state.isWin
    },
  },
  methods: {
    handleSaveKeyDown(event) {
      const { isWin } = this
      if (event.key === 'S' || event.key === 's') {
        if ((event.ctrlKey && isWin) || (event.metaKey && !isWin)) {
          event.preventDefault()
          event.stopPropagation()
          this.save()
        }
      }
    },
  },
  created() {
    window.addEventListener('keydown', this.handleSaveKeyDown)
  },
  beforeUnmount() {
    window.removeEventListener('keydown', this.handleSaveKeyDown)
  },
}
