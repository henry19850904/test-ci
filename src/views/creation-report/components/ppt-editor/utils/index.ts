// import { OSS_SPIDER_BUCKET } from "@/config";
export const OSS_SPIDER_BUCKET = ''

import store from '@/store'
import Moment from 'moment'

// eslint-disable-next-line
export const PageTimeFormatter = (time: Moment.Moment, type: string) => {
  switch (type) {
    case 'week': {
      const firstDay = Moment(time).startOf('isoWeek')
      const lastDay = Moment(time).endOf('isoWeek')
      return `${firstDay.format('YYYY年MM月DD日')}~${lastDay.format('YYYY年MM月DD日')}`
    }
    case 'month': {
      return time.format('YYYY年MM月')
    }
    case 'season': {
      return `${time.format('YYYY')}Q${time.format('Q')}`
    }
    case 'half_year': {
      const half = time.quarter() > 2 ? 2 : 1
      return `${time.format('YYYY')}H${half}`
    }
    case 'year': {
      return time.format('YYYY年')
    }

    default:
      return null
  }
}

// eslint-disable-next-line
export async function openHref(node: HTMLElement) {
  let href = node.getAttribute('href') || ''
  const type = node.getAttribute('doc_type')
  if (type === 'pdf') {
    const client = await store.dispatch('getOssClient', OSS_SPIDER_BUCKET)
    href = client.signatureUrl(href, {
      method: 'GET',
    })
  }
  window.open(href, '_blank')
}

// eslint-disable-next-line
export function handlePageTextClick(ev: MouseEvent, parent: HTMLElement) {
  const findRef = (target: HTMLElement) => {
    let node = target
    let res = null
    while (node !== parent) {
      if (node.classList.contains('refer')) {
        res = node
        break
      }
      node = node.parentElement as HTMLElement
    }
    return res
  }
  const target = ev.target
  const topRef = findRef(target as HTMLElement)
  if (topRef) {
    openHref(topRef)
  }
}

// 获取全屏元素
export function fullscreenElement(): Element | (() => void) | null {
  class Dom extends Document {
    public webkitFullscreenElement?: () => void
    public mozFullScreenElement?: () => void
    public msFullScreenElement?: () => void
  }
  const docum = document as Dom
  //注意：要在用户授权全屏后才能获取全屏的元素，否则 fullscreenEle为null
  return (
    docum.fullscreenElement ||
    docum.mozFullScreenElement ||
    docum.webkitFullscreenElement ||
    docum.msFullScreenElement ||
    null
  )
}
