/**
 * 编辑器工具栏配置项
 */

// 字体大小: ql-size
const sizes = []
const quillSizes = []
for (let fontSize = 8; fontSize <= 100; fontSize += 2) {
  sizes.push(`${fontSize / 16}em`)
  quillSizes.push(`${fontSize}px`)
}
export const sizeWhitelist = sizes
export const quillShowList = quillSizes

// 字体样式: ql-font
export const quillFonts = [
  { value: 'SimSun', label: '宋体' },
  { value: 'SimHei', label: '黑体' },
  { value: 'Microsoft-YaHei', label: '微软雅黑' },
  { value: 'KaiTi', label: '楷体' },
  { value: 'FangSong', label: '仿宋' },
  { value: 'Arial', label: 'Arial' },
  { value: 'sans-serif', label: 'sans-serif' },
]
export const fonts = quillFonts.map(ele => ele.value)
