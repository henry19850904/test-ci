/**
 * ppt编辑相关常量
 */

export const OSS_SPIDER_BUCKET = ''
export const OSS_PRODUCT_ENV_BUCKET = 'env_storage'

const { VUE_APP_OSS_ENV_BUCKET_PREFIX, VUE_APP_OSS_LOCAL_PIC_BUCKET } = process.env

export const OSS_ENV_BUCKET_PREFIX = VUE_APP_OSS_ENV_BUCKET_PREFIX
export const OSS_LOCAL_PIC_BUCKET = VUE_APP_OSS_LOCAL_PIC_BUCKET
