import { computed, ref } from 'vue'
import { TemplatePages, PageTypes, PagesType, Textboxes, Images, Indicators, BoxConfig } from '@/models/pptTemplate'
import { cloneDeep } from 'lodash'

// eslint-disable-next-line
export default function useEditor(temp: TemplatePages) {
  const editable = ref(true)
  const template = ref({} as TemplatePages)
  // ppt展示比例
  const aspectRatio = computed(() => {
    if (temp && temp.aspectRatio) {
      return temp.aspectRatio
    }
    return 4 / 3
  })

  // resize 尺寸计算
  const realWidth = ref(900)
  const handleResized = (width: number) => {
    realWidth.value = width
  }
  /**
   * Page - 选择-新增-删除-复制-粘贴
   */
  const selectedPageId = ref('')
  const pages = ref([] as Partial<PagesType>[])

  // 复制用
  const copiedType = ref('')
  const copiedElement = ref()
  const pagePreviewContextMenuRef = ref()
  const selectedPageIndex = computed(() => {
    return pages.value.findIndex(({ pageId }) => pageId === selectedPageId.value)
  })
  const pagePreviewContextMenu = computed(() => {
    const menu = [
      {
        icon: 'icondelete',
        text: '删除',
        key: 'delete',
      },
      {
        icon: 'icondelete',
        text: '复制',
        key: 'copy',
      },
    ]
    if (copiedType.value === 'page' && copiedElement.value) {
      menu.push({
        icon: 'icondelete',
        text: '粘贴',
        key: 'paste',
      })
    }
    return menu
  })
  const afterPageLoad = () => {
    selectedPageId.value = pages.value[0].pageId || ''
    // state.undoManager.clear();
  }
  // 生成新page PagesType
  const getNewPage = (pageType: Partial<PageTypes>) => {
    const { indicators = [], textboxes = [], background, key, images = [] } = cloneDeep(pageType)
    const page: Partial<PagesType> = {
      template: key,
      indicators: indicators.map((item: Indicators) => ({
        ...item,
        id: uuid(),
      })),
      textboxes: textboxes.map((item: Textboxes) => ({
        ...item,
        id: uuid(),
      })),
      pageId: uuid(),
      background,
      images: images.map((item: Images) => ({
        ...item,
        id: uuid(),
      })),
    }
    return page
  }
  const initPageByTemplate = (initTemp: TemplatePages) => {
    // fileName = "新建研报";
    template.value = initTemp
    pages.value = initTemp.pageTypes.map(pageType => getNewPage(pageType))
    afterPageLoad()
  }
  initPageByTemplate(temp)
  const changeCurrentPage = (newPageId: string) => {
    selectedPageId.value = newPageId
  }
  // 新增页
  const insertPage = (page: Partial<PagesType>, insertIndex: number) => {
    // const oldPageId = selectedPageId.value
    const insert = () => {
      pages.value.splice(insertIndex, 0, page)
      selectedPageId.value = page.pageId || ''
    }
    insert()
    // undoManager.add({
    //   undo: () => {
    //     pages.splice(insertIndex, 1);
    //     selectedPageId = oldPageId;
    //   },
    //   redo: () => {
    //     insert();
    //   }
    // });
  }
  //根据pagetype插入新页面
  const addPage = (pageType: Partial<PageTypes>) => {
    const page: Partial<PagesType> = getNewPage(pageType)
    let insertIndex = 0
    if (selectedPageId.value) {
      insertIndex = selectedPageIndex.value + 1
    }
    insertPage(page, insertIndex)
  }
  const removePage = (pageIndex: number) => {
    // const oldSelectedPageId = selectedPageId.value
    const oldPageIndex = selectedPageIndex.value
    // const oldPage = pages.value[pageIndex]
    const remove = () => {
      pages.value.splice(pageIndex, 1)
      if (pages.value.length === 0) {
        selectedPageId.value = ''
      } else {
        if (oldPageIndex === pageIndex) {
          if (pageIndex >= pages.value.length) {
            selectedPageId.value = pages.value[pages.value.length - 1].pageId || ''
          } else {
            selectedPageId.value = pages.value[pageIndex].pageId || ''
          }
        }
      }
    }
    remove()
    // undoManager.add({
    //   undo: () => {
    //     pages.splice(oldPageIndex, 0, oldPage);
    //     selectedPageId = oldSelectedPageId;
    //   },
    //   redo: () => {
    //     remove();
    //   }
    // });
  }
  const copyPage = (page: PagesType) => {
    copiedElement.value = cloneDeep(page)
    copiedType.value = 'page'
  }
  const handlePageOrderChange = (newIndex: number, oldIndex: number) => {
    const move = () => {
      pages.value.splice(newIndex, 0, pages.value.splice(oldIndex, 1)[0])
    }
    move()
    // undoManager.value.add({
    //   undo: () => {
    //     pages.value.splice(oldIndex, 0, pages.value.splice(newIndex, 1)[0]);
    //   },
    //   redo: () => {
    //     move();
    //   }
    // });
  }
  const pastePage = async (index: number) => {
    if (!(copiedElement.value && copiedType.value === 'page')) return

    copiedElement.value.indicators.map((item: Indicators) => {
      return {
        ...item,
        id: uuid(),
      }
    })

    const indicatorIdCache: { newId: string | null; oldIndex: number }[] = []
    const copyIndicatorError = false

    await Promise.all(
      copiedElement.value.indicators.map((item: Indicators, index: number) => {
        return new Promise(resolve => {
          if (item.indicatorId === null) {
            indicatorIdCache.push({
              newId: null,
              oldIndex: index,
            })
            // resolve()
            resolve('ok')
            return
          }
          // $apis
          //   .post_indicator_v2_copy_indicator({
          //     param: {
          //       indicator_id: item.indicatorId
          //     }
          //   })
          //   .then(res =>
          //     indicatorIdCache.push({
          //       newId: res.indicator_id,
          //       oldIndex: index
          //     })
          //   )
          //   .catch(() => (copyIndicatorError = true))
          //   .finally(() => resolve());
        })
      })
    )

    // const newIndicatorList = await getIndicatorDetails(indicatorIdCache.map(item => item.newId))

    // const indicators = newIndicatorList.map((item, index) => {
    //   const indexObj = indicatorIdCache[index]
    //   if (!indexObj) {
    //     console.error('数据错位')
    //     copyIndicatorError = true
    //     return {}
    //   }
    //   const copyItem = copiedElement.value.indicators[indexObj.oldIndex]
    //   const newCopyItem = JSON.parse(JSON.stringify(copyItem))
    //   newCopyItem.id = uuid()
    //   newCopyItem.indicatorId = item ? item.indicator_id : null

    //   if (newCopyItem.indicatorId) {
    //     $store.commit("page/" + SET_INDICATOR, {
    //       id: newCopyItem.indicatorId,
    //       indicator: item
    //     });
    //   }
    //   return newCopyItem;
    // })

    if (copyIndicatorError) {
      console.log('复制过程中发生错误')
      // $SjNotify({
      //   type: "error",
      //   message: "复制过程中发生错误"
      // });
    }

    const pastePage = {
      ...copiedElement.value,
      pageId: uuid(),
      // indicators,
      textboxes: copiedElement.value.textboxes.map((item: Textboxes) => ({
        ...item,
        id: uuid(),
      })),
      images: copiedElement.value.images.map((item: Images) => ({
        ...item,
        id: uuid(),
      })),
    }
    insertPage(pastePage, index + 1)
  }
  const handlePagePreviewRightClick = ($event: MouseEvent, page: PagesType, index: number) => {
    if (!editable.value) return
    pagePreviewContextMenuRef.value.open($event, (key: string) => {
      if (key === 'delete') {
        removePage(index)
      }
      if (key === 'copy') {
        copyPage(page)
      }
      if (key === 'paste') {
        pastePage(index)
      }
    })
  }

  // 下载用
  const getTemplatePage = async (pageType: Partial<PageTypes>, idDownload: boolean) => {
    let path
    if (pageType.background) {
      path = require(`../templates/${pageType.background.replace('page/template/', '')}`)
    }
    if (idDownload) {
      return {
        title: pageType.key,
        bkgd: {
          path,
        },
      }
    } else {
      return {
        ...pageType,
        background: path,
      }
    }
  }
  // 工具栏操作
  const fileName = ref('新建研报')
  const changeFileName = (newFileName: string) => {
    // const oldFileName = fileName.value
    fileName.value = newFileName
    // undoManager.add({
    //   undo: () => {
    //     fileName = oldFileName;
    //   },
    //   redo: () => {
    //     fileName = newFileName;
    //   }
    // });
  }

  // 添加文本
  const addTextBox = (textbox: Textboxes) => {
    const selectedPage = pages.value[selectedPageIndex.value] as PagesType
    const add = () => {
      selectedPage.textboxes.push(textbox)
    }

    add()

    // undoManager.add({
    //   undo: () => {
    //     const index = selectedPage.textboxes.indexOf(textbox);
    //     selectedPage.textboxes.splice(index, 1);
    //   },
    //   redo: () => {
    //     add();
    //   }
    // });
  }
  const handleAddTextbox = () => {
    const textbox = {
      id: uuid(),
      text: '',
      config: {
        width: 100,
        height: 50,
        x: 300,
        y: 212.5,
      },
      textStyle: {
        'font-size': '1em',
      },
    } as Textboxes
    addTextBox(textbox)
  }

  const handleAddIndicator = () => {
    console.log('indicator')
  }
  // 添加指标图表
  const addIndicator = (indicator: Indicators) => {
    const selectedPage = pages.value[selectedPageIndex.value] as PagesType
    const add = () => {
      selectedPage.indicators.push(indicator)
    }

    add()

    // undoManager.add({
    //   undo: () => {
    //     const index = selectedPage.indicators.indexOf(indicator);
    //     selectedPage.indicators.splice(index, 1);
    //   },
    //   redo: () => {
    //     add();
    //   }
    // });
  }
  // 设置背景图片
  const handleChangeBGImage = (src?: string) => {
    const url =
      'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Felement_origin_min_pic%2F16%2F07%2F04%2F16577a1d978d3df.jpg%21%2Ffwfh%2F804x625%2Fquality%2F90%2Funsharp%2Ftrue%2Fcompress%2Ftrue&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1626516257&t=9370ef7bf85b4ae92edf87cda13703df'
    pages.value[selectedPageIndex.value].background = src == '' ? '' : url
  }
  // 添加图片
  const handleAddImage = async () => {
    // const fileList = await $SjUpload({
    //   accept: "image/png,image/jpg,image/jpeg,image/gif"
    // });

    // const resource = fileList[0];
    // if (!resource) {
    //   console.error("report 获取上传选择图片失败");
    //   return;
    // }
    // const url = await uploadImage(resource);
    const url = 'https://img0.baidu.com/it/u=2676935521,922112450&fm=11&fmt=auto&gp=0.jpg'
    const imageBox = {
      id: uuid(),
      src: url,
      config: {
        width: 360,
        height: 250,
        x: 300,
        y: 212.5,
      },
    }
    addImageBox(imageBox)
  }
  const addImageBox = (imageBox: Images) => {
    const selectedPage = pages.value[selectedPageIndex.value]
    const add = () => {
      selectedPage?.images?.push(imageBox)
    }
    add()

    // undoManager.add({
    //   undo: () => {
    //     const index = selectedPage.images.indexOf(imageBox);
    //     selectedPage.images.splice(index, 1);
    //   },
    //   redo: () => {
    //     add();
    //   }
    // });
  }

  const changeImageSrc = (index: number, imageSrc: string) => {
    const selectedPage = pages.value[selectedPageIndex.value] as PagesType
    const imageBox = selectedPage.images[index]
    // const oldSrc = imageBox.src
    const change = () => {
      imageBox.src = imageSrc
      // $set(imageBox, "src", imageSrc);
    }
    change()
    // undoManager.add({
    //   undo: () => {
    //     imageBox.src = oldSrc
    //     // $set(imageBox, "src", oldSrc);
    //   },
    //   redo: () => {
    //     change();
    //   }
    // });
  }

  // 预览
  const preview = ref(false)
  const togglePreview = () => {
    preview.value = !preview.value
  }
  // 全屏
  const fullScreen = ref(false)
  const toggleFullScreen = () => {
    fullScreen.value = !fullScreen.value
  }
  // 导出
  const exportDisabled = ref(false)

  // 元素操作

  const elementContextMenuRef = ref()
  const elementContextMenu = computed(() => {
    return [
      {
        icon: 'icondelete',
        text: '删除',
        key: 'delete',
      },
      {
        icon: 'icondelete',
        text: '复制',
        key: 'copy',
      },
    ]
  })
  const pageContextMenuRef = ref()
  const pageContextMenu = computed(() => {
    const menu = []
    if (copiedElement.value && copiedType.value && copiedType.value !== 'page') {
      menu.push({
        icon: 'icondelete',
        text: '粘贴',
        key: 'paste',
      })
    }
    return menu
  })

  const copyElement = (type: string, item: Indicators | Images | Textboxes | undefined) => {
    copiedElement.value = cloneDeep(item)
    copiedType.value = type
  }
  const removeElement = (type: string, index: number) => {
    const selectedPage = pages.value[selectedPageIndex.value] as PagesType
    // let removedItem
    // if (type === 'indicator') {
    //   removedItem = selectedPage.indicators[index]
    // }
    // if (type === 'textbox') {
    //   removedItem = selectedPage.textboxes[index]
    // }
    // if (type === 'image') {
    //   removedItem = selectedPage.images[index]
    // }
    const remove = () => {
      if (type === 'indicator') {
        selectedPage.indicators.splice(index, 1)
      }
      if (type === 'textbox') {
        selectedPage.textboxes.splice(index, 1)
      }
      if (type === 'image') {
        selectedPage.images.splice(index, 1)
      }
    }
    remove()
    // undoManager.add({
    //   undo: () => {
    //     if (type === "indicator") {
    //       selectedPage.indicators.splice(index, 0, removedItem);
    //     }
    //     if (type === "textbox") {
    //       selectedPage.textboxes.splice(index, 0, removedItem);
    //     }
    //     if (type === "image") {
    //       selectedPage.images.splice(index, 0, removedItem);
    //     }
    //   },
    //   redo: () => {
    //     remove();
    //   }
    // });
  }
  const getIndicatorDetails = async (indicatorIds: (string | null)[] = []) => {
    let res: (string | null)[] = []
    // try {
    //   res = await indicatorApi.postIndicatorDetail(
    //     {
    //       indicator_id: indicatorIds,
    //       open_verify: false
    //     },
    //     {
    //       disableErrorHandler: true
    //     }
    //   );
    // } catch (e) {
    res = indicatorIds.map(() => null)
    // }

    // indicatorIds
    //   .filter(id => id)
    //   .forEach((id, index) => {
    //     $store.commit("report/" + SET_INDICATOR, {
    //       id,
    //       indicator: res[index]
    //     });
    //   })

    return res
  }

  const pasteElement = async () => {
    if (!(copiedElement.value && copiedType.value && copiedType.value !== 'page')) return
    if (copiedType.value === 'indicator') {
      const newIndicatorCopyResult = { indicator_id: null }
      // const newIndicatorCopyResult = copiedElement.value.indicatorId
      //   ? await $apis.post_indicator_v2_copy_indicator({
      //       param: {
      //         indicator_id: copiedElement.value.indicatorId
      //       }
      //     })
      //   : { indicator_id: null };

      // const newIndicatorList = await getIndicatorDetails([newIndicatorCopyResult.indicator_id])

      // if (newIndicatorCopyResult.indicator_id) {
      //   $store.commit("report/" + SET_INDICATOR, {
      //     id: newIndicatorCopyResult.indicator_id,
      //     indicator: newIndicatorList[0]
      //   });
      // }

      const newElement = JSON.parse(JSON.stringify(copiedElement.value))
      newElement.id = uuid()
      newElement.indicatorId = newIndicatorCopyResult.indicator_id
      const newEle = {
        ...newElement,
        id: uuid(),
        config: {
          ...copiedElement.value.config,
          x: copiedElement.value.config.x + 10,
          y: copiedElement.value.config.y + 10,
        },
      }
      addIndicator(newEle)
    }

    if (copiedType.value === 'textbox') {
      const newEle = {
        ...copiedElement.value,
        id: uuid(),
        config: {
          ...copiedElement.value.config,
          x: copiedElement.value.config.x + 10,
          y: copiedElement.value.config.y + 10,
        },
      }
      addTextBox(newEle)
    }

    if (copiedType.value === 'image') {
      const newEle = {
        ...copiedElement.value,
        id: uuid(),
        config: {
          ...copiedElement.value.config,
          x: copiedElement.value.config.x + 10,
          y: copiedElement.value.config.y + 10,
        },
      }
      addImageBox(newEle)
    }
  }
  const updateElement = (type: string, index: number, config: BoxConfig) => {
    const selectedPage = pages.value[selectedPageIndex.value]
    let item: Indicators | Textboxes | Images | undefined
    if (type === 'indicator') {
      item = (selectedPage?.indicators as Indicators[])[index]
    }
    if (type === 'textbox') {
      item = (selectedPage?.textboxes as Textboxes[])[index]
    }
    if (type === 'image') {
      item = (selectedPage?.images as Images[])[index]
    }

    if (!item) {
      console.error('updateElement: 未知的node type:', type)
      return
    }
    // const oldConfig = item.config
    const update = () => {
      ;(item as Indicators | Textboxes | Images).config = config
    }
    update()
    // undoManager.add({
    //   undo: () => {
    //     $set(item, "config", oldConfig);
    //   },
    //   redo: () => {
    //     update();
    //   }
    // });
  }
  //组件位置大小发生改变
  const handleElementUpdate = (type: string, index: number, config: BoxConfig) => {
    updateElement(type, index, config)
  }
  const handleElementRightClick = (
    event: MouseEvent,
    type: string,
    item: Indicators | Images | Textboxes,
    index: number
  ) => {
    if (!editable.value) return
    elementContextMenuRef.value.open(event, (key: string) => {
      if (key === 'delete') {
        removeElement(type, index)
      }
      if (key === 'copy') {
        copyElement(type, item)
      }
    })
  }
  const handlePageRightClick = ($event: MouseEvent) => {
    if (!editable.value) return
    pageContextMenuRef.value.open($event, (key: string) => {
      if (key === 'paste') {
        pasteElement()
      }
    })
  }

  // 编辑文本
  const editType = ref('')
  const editIndex = ref(-1)
  const editElement = ref()
  const showTextEditor = (index: number) => {
    editType.value = 'text'
    editIndex.value = index
    editElement.value = (pages.value[selectedPageIndex.value].textboxes as Textboxes[])[index] as Textboxes
  }
  const cancelChangeTextbox = () => {
    editType.value = ''
    editIndex.value = -1
    editElement.value = null
  }
  const changeTextboxVal = (textbox: Textboxes) => {
    if (editIndex.value != -1) {
      ;(pages.value[selectedPageIndex.value].textboxes as Textboxes[])[editIndex.value] = textbox
    }
    cancelChangeTextbox()
  }

  // const slideMasters = computed(async () => {
  //   await Promise.all(template.pageTypes.map(page => getTemplatePage(page, true))!)
  // })
  // const templateInsteadBG = computed(async () => {
  //   await Promise.all(template.pageTypes.map(page => getTemplatePage(page, false))!)
  // })

  // 事件绑定-键盘鼠标

  const stopEvent = (event: Event) => {
    event.stopPropagation()
    event.preventDefault()
  }
  const editContainer = ref()
  const isWin = computed(() => {
    // return store.state.isWin;
    return true
  })
  // 下一页
  const goNextPage = () => {
    const index = selectedPageIndex.value + 1
    if (index >= pages.value.length) {
      // document?.webkitExitFullscreen();
    } else {
      selectedPageId.value = pages.value[index].pageId || ''
    }
  }
  // 上一页
  const goPrevPage = () => {
    const index = selectedPageIndex.value - 1
    if (index >= 0) {
      selectedPageId.value = pages.value[index].pageId || ''
    }
  }
  const handleKeyup = (event: KeyboardEvent) => {
    if (
      // indicatorDialog.dialogVisible ||
      elementContextMenuRef.value.ctxVisible ||
      pageContextMenuRef.value.ctxVisible ||
      editType.value !== '' ||
      editIndex.value >= 0 ||
      editElement.value
      // indicatorEditDialog.visible ||
      // PublishDialog.dialogVisible
    ) {
      return
    }

    if (!editable.value) return
    // if (isPublishing.value || fileNameEditing.value) return;

    switch (event.key.toLowerCase()) {
      case 'z':
        // if ((event.ctrlKey && isWin.value) || (event.metaKey && !isWin.value)) {
        //   if (event.shiftKey) {
        //     redo();
        //   } else {
        //     undo();
        //   }
        // }
        break
      case 'escape':
        if (fullScreen.value) {
          fullScreen.value = false
        }
        break
      case 'delete':
      case 'backspace':
        {
          const activeElement = editContainer.value.getActiveElement()
          if (activeElement) {
            const { type, index } = activeElement
            removeElement(type, index)
          } else {
            removePage(selectedPageIndex.value)
          }
        }
        break
      case 'c':
        if ((event.ctrlKey && isWin.value) || (event.metaKey && !isWin.value)) {
          const activeElement = editContainer.value.getActiveElement()
          if (activeElement) {
            const { type, index } = activeElement
            const selectedPage = pages.value[selectedPageIndex.value]
            let item
            if (type === 'indicator') {
              item = (selectedPage?.indicators as Indicators[])[index]
            }
            if (type === 'textbox') {
              item = (selectedPage?.textboxes as Textboxes[])[index]
            }
            if (type === 'image') {
              item = (selectedPage?.images as Images[])[index]
            }
            copyElement(type, item)
          } else {
            copyPage(pages.value[selectedPageIndex.value] as PagesType)
          }
        }
        break
      case 'v':
        if ((event.ctrlKey && isWin.value) || (event.metaKey && !isWin.value)) {
          if (copiedElement.value) {
            if (copiedType.value === 'page') {
              pastePage(selectedPageIndex.value)
            } else {
              pasteElement()
            }
          }
        }
        break

      default:
        if (preview.value) {
          if (event.key === 'ArrowUp' || event.key === 'ArrowLeft') {
            goPrevPage()
          } else if (event.key === 'ArrowDown' || event.key === 'ArrowRight') {
            goNextPage()
          }
        }
        break
    }
    // !createDialogVisible && stopEvent(event);
  }

  const handleMouseDown = (event: MouseEvent) => {
    if (preview.value) {
      if (event.button === 2 || event.button === 4) {
        goPrevPage()
        stopEvent(event)
      } else if (event.button === 0 || event.button === 3) {
        goNextPage()
        stopEvent(event)
      }
    } else {
      let node = event.target as Node
      let isCancel = true
      while (node.nodeName.toLowerCase() !== 'body') {
        const classList = (node as HTMLElement).classList
        if (classList.contains('page-content') || classList.contains('quill-toolbar')) {
          isCancel = false
          break
        }
        node = node.parentNode as Node
      }

      isCancel && cancelChangeTextbox()
    }
  }

  function uuid() {
    return Math.random().toFixed(10).toString().split('.')[1]
  }

  return {
    aspectRatio,
    // slideMasters,
    // templateInsteadBG,
    template,
    pages,
    selectedPageId,
    selectedPageIndex,
    editable,
    copiedType,
    copiedElement,
    pagePreviewContextMenuRef,
    pagePreviewContextMenu,
    elementContextMenu,
    elementContextMenuRef,
    pageContextMenu,
    pageContextMenuRef,
    editContainer,
    fileName,
    preview,
    fullScreen,
    exportDisabled,
    realWidth,
    editType,
    editIndex,
    editElement,
    // func
    changeCurrentPage,
    getNewPage,
    insertPage,
    addPage,
    afterPageLoad,
    initPageByTemplate,
    removePage,
    copyPage,
    pastePage,
    handlePageOrderChange,
    handlePagePreviewRightClick,
    copyElement,
    removeElement,
    pasteElement,
    handleElementUpdate,
    handleElementRightClick,
    handlePageRightClick,
    getTemplatePage,
    changeFileName,
    handleAddImage,
    handleChangeBGImage,
    handleAddTextbox,
    handleAddIndicator,
    togglePreview,
    toggleFullScreen,
    handleResized,
    changeImageSrc,
    getIndicatorDetails,
    handleKeyup,
    handleMouseDown,
    showTextEditor,
    cancelChangeTextbox,
    changeTextboxVal,
    uuid,
  }
}
