/**
 * 图片处理
 */
import { useStore } from '@/store'
export default function ossImageMixin(): {
  getFinalImageSrc: (src: string) => null | string | Promise<string>
  getImageBase64: (src: string) => Promise<string | null>
} {
  const store = useStore()
  const getFinalImageSrc = async (src: string) => {
    if (!src) return null
    if (src.startsWith('data:image/png;')) return src
    const client = await store.dispatch('getOssClient')
    return client.signatureUrl(`${src}`, {
      method: 'GET',
    })
  }
  const getImageBase64 = async (src: string) => {
    if (!src) return null
    if (src.startsWith('data:image/png;')) return src
    try {
      const client = await store.dispatch('getOssClient')
      const res = await client.get(`${src}`)
      const base64 = new Buffer(await res.arrayBuffer()).toString('base64')
      //const contentType = res.headers.get("content-type")
      const contentType = 'image/png'
      return `data:${contentType};base64,${base64}`
    } catch (e) {
      console.log(e)
      return null
    }
  }
  return {
    getFinalImageSrc,
    getImageBase64,
  }
}
