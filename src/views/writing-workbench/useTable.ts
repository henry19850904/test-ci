import { reactive, ref } from 'vue'
import { ListResult } from '@/models/apiResult'
import { PagingParam } from '@/models/apiResult'

// eslint-disable-next-line
export default function useTable<T>(getListApi?: (params: PagingParam)=> Promise<ListResult<T>>) {
  const tableLoading = ref(true)
  const dataList = ref<T[]>([])

  const paginationOption = reactive({
    total: 0,
    defaultCurrent: 1,
    defaultPageSize: 10, //每页中显示10条数据
    showSizeChanger: true,
    pageSizeOptions: ['10', '20', '50', '100'], //每页中显示的数据
    showQuickJumper: true,
    showLessItems: true,
    size: 'default',
  })

  // async function pageChange(pagination: TableState['pagination'] = { current: 1, pageSize: 10 }) {
  //   const res = await getListApi({ page: pagination?.current, size: pagination?.pageSize })
  //   dataList.value = res.data.list
  //   paginationOption.total = res.data.total
  // }

  return {
    tableLoading,
    paginationOption,
    dataList,
  }
}
