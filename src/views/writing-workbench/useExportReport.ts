/* eslint-disable */
import { nextTick, reactive, ref } from 'vue'
import html2canvas from 'html2canvas'
import jsPDF from 'jspdf'
import { asBlob } from 'html-docx-js-typescript'
import { saveAs } from 'file-saver'

// eslint-disable-next-line
export default function useExportReport() {
  const A4Width = 595.28 // A4纸宽度 595.28
  const A4Height = 841.89 // A4纸高度 841.89
  const pdfDomContainer = ref<HTMLElement | null>(null)
  const htmlString = ref('')

  const domContainerStyle = {
    opacity: 0,
    position: 'absolute',
    top: 0,
    left: 0,
    overflow: 'hidden',
    'max-height': '10px',
    'max-width': '10px',
    'z-index': -10,
  }
  const domStyle = reactive({
    width: A4Width + 'px',
    padding: '40px 10px 10px',
    'white-space': 'pre-wrap',
  })

  // function validImageUrl(url: string) {
  //   return /http(s)?:\/\/(\w+:?\w*@)?(\S+)(:\d+)?((?<=\.)\w+)+(\/([\w#!:.?+=&%@!\-/])*)?/gi.test(url)
  // }

  /**
   * 将图片资源转换为base64数据
   * @param img Image实例
   */
  function getBase64Image(img: HTMLImageElement) {
    var canvas = document.createElement('canvas')
    canvas.width = img.width
    canvas.height = img.height
    var ctx = canvas.getContext('2d')
    ctx?.drawImage(img, 0, 0, canvas.width, canvas.height)
    var dataURL = canvas.toDataURL('image/jpeg', 0.92)
    return dataURL
  }
  /**
   * 创建Image实例, 获取图片资源
   * @param imgUrl 图片的URL
   */
  function createImgWithCors(imgUrl: string) {
    return new Promise((resolve, reject) => {
      const image = new Image()
      image.src = imgUrl + '?' + new Date().getTime()
      image.crossOrigin = 'anonymous'
      image.onload = function () {
        resolve(getBase64Image(image))
      }
      image.onerror = function () {
        reject('data:,')
      }
    })
  }

  /**
   * 获取html字符串中图片的地址并转换为base64后返回新的html字符串
   * @param html HTML字符串
   */
  async function getImageSrc(html: string): Promise<string> {
    const matchReg = /(?<=src=").*?(?=")/gi
    const srcArr = html.match(matchReg) as string[]
    const promiseArr = srcArr.map(item => {
      return createImgWithCors(item)
    })
    const result = await Promise.all(promiseArr)
    srcArr.forEach((item, index) => {
      html = html.replace(item, result[index] as string)
    })
    return html
  }
  /**
   * 通过计算分页排版dom结构(未完成)
   * @param domContainer 已经渲染的dom容器
   * @param pageHeight 一页的高度,默认使用A4纸张高度
   */
  function setPage(domContainer: HTMLElement, pageHeight: number = A4Height) {
    // console.log(domContainer.children)
    let len = domContainer.children.length
    for (let i = 0; i < len; i++) {
      let clientHeight = domContainer.children[i].clientHeight
      let offsetTop = (domContainer.children[i] as HTMLElement).offsetTop
      if (clientHeight + offsetTop > pageHeight) {
        console.log('超出的元素', domContainer.children[i])
        if (domContainer.children[i].children.length === 0 && i > 0) {
          let preNodeOffsetTopAndClientHeight =
            (domContainer.children[i - 1] as HTMLElement).offsetTop + domContainer.children[i - 1].clientHeight
          ;(domContainer.children[i - 1] as HTMLElement).style.marginBottom =
            pageHeight - preNodeOffsetTopAndClientHeight + 40 + 'px'
        } else {
          // setPage(domContainer.children[i] as HTMLElement, pageHeight - (domContainer.children[i] as HTMLElement).offsetTop)
        }
      }
    }
  }

  /**
   * 创建PDF文件入口方法 此方法主要创建canvas
   * @param html HTML字符串
   * @param fileName 可选 文件名称, 默认为本地时间的字符串
   */
  async function createPDF(html: string, fileName: string = new Date().toLocaleString()): Promise<void> {
    htmlString.value = await getImageSrc(html)
    await nextTick() // 等待渲染html
    // setPage(pdfDomContainer.value as HTMLElement)
    const canvas = document.createElement('canvas')
    const context = canvas.getContext('2d') as CanvasRenderingContext2D
    const width = pdfDomContainer.value?.clientWidth ?? 1
    const height = pdfDomContainer.value?.clientHeight ?? 1
    const scale = 3 // 用于提高canvas绘制清晰度
    canvas.width = width * scale
    canvas.height = height * scale
    context.fillStyle = '#ffffff'
    context.fillRect(0, 0, canvas.width, canvas.height)
    context.scale(scale, scale)
    const opts = {
      scale: 1,
      width: width, //dom 原始宽度
      height: height,
      canvas: canvas,
      useCORS: true, //允许canvas画布内可以跨域请求外部链接图片, 允许跨域请求。
      allowTaint: true, // 允许污染
    }
    if (pdfDomContainer.value) {
      html2canvas(pdfDomContainer.value, opts).then(canvas => {
        createPDFForA4(canvas, fileName)
        // downloadPDF(canvas, scale, fileName)
      })
    }
  }

  /**
   * 触发浏览器下载创建好的PDF文件 (A4纸大小)
   * @param canvas canvas实例
   */
  function createPDFForA4(canvas: HTMLCanvasElement, fileName: string) {
    const contentWidth = canvas.width
    const contentHeight = canvas.height
    const pageHeight = (contentWidth / A4Width) * A4Height
    let leftHeight = contentHeight
    // pdf页面向上偏移
    let position = 0
    // a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
    const imgWidth = A4Width
    const imgHeight = (A4Width / contentWidth) * contentHeight
    const pageData = canvas.toDataURL('image/jpeg', 1.0) //转换图片为dataURL
    const pdf = new jsPDF(undefined, 'pt', 'a4')
    // 有两个高度需要区分，一个是html页面的实际高度，和生成pdf的页面高度(841.89)
    // 当内容未超过pdf一页显示的范围，无需分页
    if (leftHeight < pageHeight) {
      pdf.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight)
    } else {
      while (leftHeight > 0) {
        pdf.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight)
        leftHeight -= pageHeight
        position -= A4Height
        //避免添加空白页
        if (leftHeight > 0) {
          pdf.addPage()
        }
      }
    }
    pdf.save(`${fileName}.pdf`)
  }

  /**
   * 触发浏览器下载创建好的PDF文件 (自定义宽高)
   * @param canvas canvas实例
   * @param scale 缩放比例,用于提高清晰度,默认值为3 (此方法中将pdf缩小scale倍)
   */
  function downloadPDF(canvas: HTMLCanvasElement, scale: number, fileName: string) {
    let contentWidth = canvas.width / scale
    let contentHeight = canvas.height / scale
    let pdf = new jsPDF(undefined, 'pt', [contentWidth, contentHeight]) //自定义宽高
    let pageData = canvas.toDataURL('image/png', 1.0) //转换图片为dataURL
    pdf.addImage(pageData, 'JPEG', 0, 0, contentWidth, contentHeight) //添加图像到页面
    pdf.save(`${fileName}.pdf`)
  }

  /*************************************** 导出Word **************************************/
  /**
   * 创建Word方法
   * @param html HTML字符串
   * @param fileName 可选 文件名称, 默认为本地时间的字符串
   */
  async function createDocx(html: string, fileName: string = new Date().toLocaleString()) {
    html = await getImageSrc(html)
    asBlob(html).then(data => {
      saveAs(data as Blob, `${fileName}.docx`)
    })
  }

  return {
    domContainerStyle,
    domStyle,
    htmlString,
    pdfDomContainer,
    createPDF,
    createDocx,
  }
}
