/* eslint-disable */
import ParameterName from './ParameterName.vue'
import { defineComponent } from 'vue'
import { cloneDeep } from 'lodash-es'

export default defineComponent({
  components: { ParameterName },
  props: {
    format: String,
    globalStyles: Object,
    data: Object,
    isSystem: {
      type: Boolean,
      default: false,
    },
    parameter: Object,
    userParams: Array,
    systemParams: Array,
    collapsed: Boolean,
    dblclickable: {
      type: Boolean,
      default: false,
    },
    // TODO 后续看是否去掉
    comparable: {
      type: Boolean,
      default: true,
    },
  },
  computed: {
    removable() {
      if (!this.isSystem) return true
      return !this.userParams.some(param => param.config.formula.some(item => item.id === this.parameter.param_id))
    },
  },
  watch: {
    data: {
      immediate: true,
      deep: true,
      handler(val) {
        this.config = cloneDeep(val)
      },
    },
  },
})
