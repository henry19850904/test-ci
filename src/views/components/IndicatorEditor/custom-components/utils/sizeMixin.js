/* eslint-disable */
export default {
  props: {
    size: {
      type: String,
      default: 'default',
      validator(v) {
        return ['large', 'middle', 'default', 'small', 'mini'].indexOf(v) !== -1
      },
    },
  },
}
