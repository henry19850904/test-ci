/* eslint-disable */
/**
 * Created by Xinhe on 2018/6/26.
 */
const { isNumber } = require('lodash-es')

export const ComponentInstall = (...Component) =>
  Component.reduce((prev, comp) => {
    console.log(comp.name)
    if (!comp.name) {
      console.error('missing component name', comp)
    }
    comp.install = function (Vue) {
      Vue.component(comp.name, comp)
    }
    return {
      ...prev,
      [comp.name]: comp,
    }
  }, {})

export const MethodInstall = (Method, MethodName) => {
  Method.install = function (Vue) {
    Vue.prototype[`$Sj${MethodName}`] = Method
  }
  return {
    [MethodName]: Method,
  }
}

export const DirectiveInstall = (Directive, DirectiveName) => {
  Directive.install = function (Vue) {
    Vue.directive(DirectiveName, Directive)
  }
  return {
    [DirectiveName]: Directive,
  }
}

export const numberFormatUnit = (number, toFixedLength = null, formatKilo = false) => {
  const myNumber = parseFloat(number)
  if (!isNumber(myNumber) || Number.isNaN(myNumber)) {
    return '-'
  }

  const format = number => {
    if (toFixedLength === null) {
      return number.toLocaleString(undefined, {
        maximumFractionDigits: 2,
      })
    } else {
      return number.toFixed(toFixedLength)
    }
  }

  if (Math.abs(myNumber) >= 100000000) {
    return `${format(parseFloat(myNumber / 100000000))}亿`
  }
  if (Math.abs(myNumber) >= 10000) {
    return `${format(parseFloat(myNumber / 10000))}万`
  }
  if (formatKilo && Math.abs(myNumber) >= 1000) {
    return `${format(parseFloat(myNumber / 1000))}K`
  }

  if (Math.abs(myNumber) < 1 && Math.abs(myNumber) > 0) {
    return myNumber.toLocaleString(undefined, {
      maximumSignificantDigits: 3,
    })
  }
  return format(myNumber)
}

export const mappingCheck = (arr, value, key = 'key') =>
  arr.reduce(
    (prev, item) => ({
      ...prev,
      [item[key]]: value,
    }),
    {}
  )

export function isNumeric(n) {
  return !Number.isNaN(parseFloat(n))
}
export function getDefaultProp(prop, defaultValue) {
  const getValue = (v = defaultValue) => v
  return getValue(prop)
}
