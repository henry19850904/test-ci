/* eslint-disable */
export default {
  inject: {
    sjFormItem: {
      default: '',
    },
  },
  computed: {
    validateState() {
      return this.sjFormItem ? this.sjFormItem.validateState : null
    },
  },
}
