/**
 * Created by Xinhe on 2019/9/9.
 */
export default {
  props: {
    fontFamily: {
      type: String,
      default: 'icon_com',
    },
    iconClass: String,
  },
}
