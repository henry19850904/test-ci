/* eslint-disable */
let zIndex = 2000
const instances = {}

const PopupManager = {
  getInstance(id) {
    return instances[id]
  },
  register(id, instance) {
    if (id && instance) {
      instances[id] = instance
    }
  },
  deregister(id) {
    if (id) {
      instances[id] = null
      delete instances[id]
    }
  },
  nextZIndex() {
    return PopupManager.zIndex++
  },
  modalStack: [],
  openModal(id) {
    this.modalStack.push({ id })
  },

  closeModal() {
    const modalStack = this.modalStack
    if (modalStack.length > 0) {
      modalStack.pop()
    }
  },
}

Object.defineProperty(PopupManager, 'zIndex', {
  configurable: true,
  get() {
    return zIndex
  },
  set(value) {
    zIndex = value
  },
})

const getTopPopup = function () {
  if (PopupManager.modalStack.length > 0) {
    const topPopup = PopupManager.modalStack[PopupManager.modalStack.length - 1]
    if (!topPopup) return
    const instance = PopupManager.getInstance(topPopup.id)
    return instance
  }
}

// if (process.env.VUE_ENV !== 'server') {
//   window.addEventListener('keydown', event => {
//     if (event.key === 'Escape') {
//       const topPopup = getTopPopup()

//       if (topPopup && topPopup.closeOnEsc) {
//         topPopup.handleClose
//           ? topPopup.handleClose()
//           : topPopup.handleAction
//           ? topPopup.handleAction('cancel')
//           : topPopup.close()
//       }
//     }
//   })
// }

export default PopupManager
