/* eslint-disable */
import PopupManager from './popupManager'

let idSeed = 1

export default {
  props: {
    visible: {
      type: Boolean,
      default: false,
    },
    zIndex: {},
  },
  beforeMount() {
    this._popupId = `popup-${idSeed++}`
    PopupManager.register(this._popupId, this)
  },
  mounted() {
    this.$el.style.zIndex = PopupManager.nextZIndex()
  },
  beforeUnmount() {
    PopupManager.deregister(this._popupId)
  },
  methods: {
    pop() {
      PopupManager.closeModal()
    },
    push() {
      PopupManager.openModal(this._popupId)
    },
  },
}
