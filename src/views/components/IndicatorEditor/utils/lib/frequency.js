/* eslint-disable */

/*
 * @Author: WLL
 * @Date: 2020-07-20 15:45:57
 * @LastEditors: WLL
 * @LastEditTime: 2020-10-29 14:24:43
 * @Description: frequency related methods
 */
export const frequencyMap = {
  day: '日度',
  week: '周度',
  'ten-day': '旬',
  month: '月度',
  quarter: '季度',
  'half-year': '半年度',
  year: '年度',
}

export const frequencyTimeRangeMap = {
  day: 1,
  week: 3,
  'ten-day': 3,
  month: 5,
  quarter: 5,
  'half-year': 15,
  year: 'max',
}

export function getFrequencyOptions() {
  const options = []
  for (const key in frequencyMap) {
    options.push({
      label: frequencyMap[key],
      value: key,
    })
  }

  return options
}

export function getMergedFrequency(frequencies = []) {
  const filterFrequencies = [...new Set(frequencies.filter(item => item))]
  if (frequencies.length === 0) return null
  if (
    (filterFrequencies.includes('week') && !filterFrequencies.every(f => f === 'week')) ||
    (filterFrequencies.includes('ten-day') && filterFrequencies.length > 1)
  ) {
    return 'day'
  } else {
    const frequencyList = Object.keys(frequencyMap)
    return filterFrequencies.sort((a, b) => {
      return frequencyList.indexOf(a) - frequencyList.indexOf(b)
    })[0]
  }
}

export function getFrequencyOptionsList(originFrequency) {
  const initPeriodOptions = Object.keys(frequencyMap).map(key => ({
    value: key,
    label: frequencyMap[key],
  }))
  const frequency = originFrequency
  const index = initPeriodOptions.findIndex(({ value }) => value === frequency)
  return [
    ...initPeriodOptions.slice(index + 1).filter(v => frequency !== 'week' || v.value !== 'ten-day'), // 最小是月度(没有周度)
  ]
}
