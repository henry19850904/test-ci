/* eslint-disable */

/*
 * @Author: WLL
 * @Date: 2020-07-20 16:10:13
 * @LastEditors: WLL
 * @LastEditTime: 2020-09-17 00:42:09
 * @Description: time related methods
 */
import Moment from 'moment'
import { isNumber as IsNumber } from 'lodash'

export function dateFormat(time) {
  if (!time) return null
  return Moment(time).format('YYYY-MM-DD')
}

export function dateTimeFormat(time) {
  if (!time) return null
  return Moment(time).format('YYYY-MM-DD HH:mm:ss')
}

export const tenDayLabels = [
  {
    label: '上旬',
    value: '01日',
  },
  {
    label: '中旬',
    value: '11日',
  },
  {
    label: '下旬',
    value: '21日',
  },
]

export function getTenDayLabel(time) {
  const tenNum = Math.floor(Moment(time).date() / 10)
  return tenDayLabels[tenNum]
}

export function timeFormatter(time, frequency, weekRange = false) {
  let tenDayLabel = ''
  switch (frequency) {
    case 'day':
      return Moment(time).format('YYYY-MM-DD')
    case 'week':
      return `${Moment(time).format('GGGG第WW周')}${
        weekRange
          ? `(${Moment(time).startOf('isoWeek').format('YYYY年MM月DD日')}~
          ${Moment(time).endOf('isoWeek').format('YYYY年MM月DD日')})`
          : ''
      }`
    case 'ten-day':
      tenDayLabel = getTenDayLabel(time)
      return `${Moment(time).format('YYYY年MM月')}${tenDayLabel && tenDayLabel.label}`
    case 'month':
      return Moment(time).format('YYYY年MM月')
    case 'quarter':
      return Moment(time).format('YYYY第Q季')
    case 'half-year':
      return `${Moment(time).format('YYYY年')}${Moment(time).quarter() > 2 ? '下半年' : '上半年'}`
    case 'year':
      return Moment(time).format('YYYY年')
    default:
      return null
  }
}

// 格式化时间戳
export function timestampFormatter(timestamp, frequency) {
  if (!frequency) {
    return timestamp
  }
  if (!IsNumber(timestamp)) {
    return timestamp
  }
  return timeFormatter(timestamp, frequency)
}

export function getNextTime(time, frequency) {
  let tenNum = 0
  switch (frequency) {
    case 'day':
      return Moment(time).add(1, 'd')
    case 'week':
      return Moment(time).add(1, 'w')
    case 'ten-day':
      tenNum = Math.floor(Moment(time).date() / 10)
      if (tenNum < 2) {
        return Moment(time).add(10, 'd')
      } else {
        return Moment(time).subtract(20, 'd').add(1, 'M')
      }
    case 'month':
      return Moment(time).add(1, 'M')
    case 'quarter':
      return Moment(time).add(1, 'Q')
    case 'half-year':
      return Moment(time).add(6, 'M')
    case 'year':
      return Moment(time).add(1, 'y')
    default:
      return null
  }
}

export function getTimeList(minDate, maxDate, frequency) {
  const data = []
  if (frequency) {
    let startTime = Moment(minDate)
    const endTime = Moment(maxDate)
    while (startTime && startTime.isSameOrBefore(endTime)) {
      const dateTime = startTime.valueOf()
      data.push(dateTime)
      startTime = getNextTime(dateTime, frequency)
    }
  }
  return data
}

// hack 夏令时匹配
export function dateHacker(date) {
  return Math.round(date / 86400000)
}
