/* eslint-disable */

/*
 * @Author: WLL
 * @Date: 2020-09-04 11:39:01
 * @LastEditors: WLL
 * @LastEditTime: 2020-11-13 18:58:08
 * @Description: common methods
 */
import { isNumber as isNumber2 } from 'lodash-es'

export const isNumber = isNumber2

export const numberFormatUnit = (
  number,
  toFixedLength = null,
  formatKilo = false,
  locale = 'zh',
  toLocalString = false,
  offset = 1
) => {
  const myNumber = parseFloat(number)
  if (!isNumber(myNumber) || Number.isNaN(myNumber)) {
    return '-'
  }

  const format = number => {
    if (toLocalString || toFixedLength === null) {
      return number.toLocaleString(undefined, {
        maximumFractionDigits: toFixedLength,
      })
    } else {
      return parseFloat(number.toFixed(toFixedLength))
    }
  }

  if (!locale) {
    return format(parseFloat(myNumber))
  }
  if (locale === 'zh') {
    if (Math.abs(myNumber) >= 100000000 * offset) {
      return `${format(parseFloat(myNumber / 100000000))}亿`
    }
    if (Math.abs(myNumber) >= 10000 * offset) {
      return `${format(parseFloat(myNumber / 10000))}万`
    }
    if (formatKilo && Math.abs(myNumber) >= 1000 * offset) {
      return `${format(parseFloat(myNumber / 1000))}K`
    }

    return format(parseFloat(myNumber))
  } else {
    if (Math.abs(myNumber) >= 1000000000 * offset) {
      return `${format(parseFloat(myNumber / 1000000000))}B`
    }
    if (Math.abs(myNumber) >= 1000000 * offset) {
      return `${format(parseFloat(myNumber / 1000000))}M`
    }
    if (formatKilo && Math.abs(myNumber) >= 1000 * offset) {
      return `${format(parseFloat(myNumber / 1000))}K`
    }

    return format(parseFloat(myNumber))
  }
}
