/* eslint-disable */

/*
 * @Author: WLL
 * @Date: 2020-07-20 16:19:59
 * @LastEditors: WLL
 * @LastEditTime: 2020-12-23 19:03:55
 * @Description: indicator related methods
 */
import { dateHacker, getTimeList, timeFormatter, timestampFormatter } from './time'
import { frequencyMap } from './frequency'
import { numberFormatUnit } from './common'
import { merge } from 'lodash-es'
import { cloneDeep as clone } from 'lodash-es'

export function getFinalChartFormat(format) {
  if (!format) return null
  let res = format
  // if (format === "panel") {
  //   res = "timeline";
  // }
  // if (format === "group") {
  //   res = "cut";
  // }
  return res
}
// 图表选择分组列表
export const chartTypeGroupList = [
  {
    label: '折线图',
    value: 'line',
  },
  {
    label: '条形图',
    value: 'horizontal_bar',
  },
  {
    label: '柱状图',
    value: 'bar',
  },
  {
    label: '面积图',
    value: 'area',
  },
  {
    label: '饼图',
    value: 'pie',
  },
  {
    label: '散点图',
    value: 'scatter',
  },
  {
    label: '地图',
    value: 'map',
  },
  {
    label: '雷达图',
    value: 'radar',
  },
  {
    label: 'K线图',
    value: 'candlestick',
  },
  {
    label: '表格',
    value: 'table',
  },
]

// 图表选择类型列表
export const chartTypeList = [
  {
    label: '折线图',
    value: 'line',
    group: 'line',
  },
  {
    label: '堆叠折线图',
    value: 'stack_line',
    group: 'line',
  },
  {
    label: '条形图',
    value: 'horizontal_bar',
    group: 'horizontal_bar',
  },
  {
    label: '堆叠条形图',
    value: 'stack_horizontal_bar',
    group: 'horizontal_bar',
  },
  {
    label: '柱状图',
    value: 'bar',
    group: 'bar',
  },
  {
    label: '堆叠柱状图',
    value: 'stack_bar',
    group: 'bar',
  },
  {
    label: '面积图',
    value: 'area',
    group: 'area',
  },
  {
    label: '堆叠面积图',
    value: 'stack_area',
    group: 'area',
  },
  {
    label: '饼图',
    value: 'pie',
    group: 'pie',
  },
  {
    label: '环形图',
    value: 'donut_pie',
    group: 'pie',
  },
  {
    label: '散点图',
    value: 'scatter',
    group: 'scatter',
  },
  {
    label: '散点序列图',
    value: 'scatter_sequence',
    group: 'scatter',
  },
  {
    label: '地图',
    value: 'map',
    group: 'map',
  },
  {
    label: '散点地图',
    value: 'scatter_map',
    group: 'map',
  },
  {
    label: '雷达图',
    value: 'radar',
    group: 'radar',
  },
  {
    label: 'K线图',
    value: 'candlestick',
    group: 'candlestick',
  },
  {
    label: '表格',
    value: 'table',
    group: 'table',
  },
  {
    label: '时序表格',
    value: 'timetable',
    group: 'table',
  },
]

// timeline 子项 样式选择列表
export const styleList = [
  {
    label: '折线图',
    value: 'line',
  },
  {
    label: '条形图',
    value: 'horizontal_bar',
  },
  {
    label: '柱状图',
    value: 'bar',
  },
  {
    label: '面积图',
    value: 'area',
  },
  {
    label: '散点图',
    value: 'scatter',
  },
  {
    label: '堆叠折线图',
    value: 'stack_line',
  },
  {
    label: '堆叠条形图',
    value: 'stack_horizontal_bar',
  },
  {
    label: '堆叠柱状图',
    value: 'stack_bar',
  },
  {
    label: '堆叠面积图',
    value: 'stack_area',
  },
]

// 各指标类型默认图表样式字典
export const formatDefaultTypeMap = {
  timeline: 'line',
  panel: 'bar',
  timecut: 'horizontal_bar',
  cut: 'horizontal_bar',
  group: 'stack_bar',
  radar: 'radar',
  map: 'map',
  candlestick: 'candlestick',
  scatter_sequence: 'scatter_sequence',
  table: 'table',
  timetable: 'timetable',
}

export function getDefaultChartType(format, extra = {}) {
  let defaultType = formatDefaultTypeMap[format]
  if (format === 'map' && extra.isScatterMap) {
    defaultType = 'scatter_map'
  }
  return defaultType
}

export function getIsScatterMap(indicator) {
  let isScatterMap = false
  const indicatorFormat = indicator && indicator.indicator_format
  if (indicatorFormat === 'map') {
    if (indicator.data && Array.isArray(indicator.data.series)) {
      indicator.data.series.every(v => {
        if (v.type === 'scatter') {
          isScatterMap = true
          return false
        }
        return true
      })
    }
  }
  return isScatterMap
}

export function getIsPie(type) {
  return ['pie', 'donut_pie'].includes(type)
}

// 获取图表类型是否不可用
export function getChartTypeDisabled(chartType, format, config, extra = {}) {
  if (!format) {
    return true
  }

  if (format === 'timetable') {
    return chartType !== 'timetable'
  }
  if (chartType === 'table') {
    return false
  }

  const commonChartTypes = [
    'line',
    'stack_line',
    'horizontal_bar',
    'stack_horizontal_bar',
    'bar',
    'stack_bar',
    'area',
    'stack_area',
    'scatter',
  ]
  if (commonChartTypes.includes(chartType)) {
    return !['timeline', 'panel', 'timecut', 'cut', 'group'].includes(format)
  }

  if (['pie', 'donut_pie'].includes(chartType)) {
    if (format === 'cut') {
      return false
    }
    if (['timeline', 'panel', 'timecut'].includes(format) && config.showTimeline) {
      return false
    }
    return true
  }

  if (chartType === 'radar') {
    return format !== 'radar'
  }

  if (['map', 'scatter_map'].includes(chartType)) {
    if (format === 'map') {
      if (extra.isScatterMap) {
        return chartType !== 'scatter_map'
      }
      return chartType !== 'map'
    }
    return true
  }

  if (chartType === 'candlestick') {
    return format !== 'candlestick'
  }

  if (chartType === 'scatter_sequence') {
    return format !== 'scatter_sequence'
  }

  return true
}

// TODO 后续删除
// 获取默认指标的styles【兼容处理】
export function defaultIndicatorStyles(
  // eslint-disable-next-line no-unused-vars
  data = [],
  default_style = {},
  format,
  // eslint-disable-next-line no-unused-vars
  type
) {
  return getDefaultIndicatorStyles(format, default_style)
}

// 获取默认指标的styles
export function getDefaultIndicatorStyles(
  format,
  // eslint-disable-next-line no-unused-vars
  default_style = {},
  extra = {}
) {
  // // TODO 后续整理
  // let revert;
  // const { chart_type } = default_style;
  // switch (chart_type) {
  //   case "group_stacked_name":
  //   case "panel_bar_time":
  //     revert = false;
  //     break;
  //   case "group_stacked_tag":
  //   case "panel_bar_name":
  //     revert = true;
  //     break;
  //   default:
  //     revert = false;
  //     break;
  // }

  return {
    showTitle: true,
    titleAlign: 'left', // 标题水平对齐
    titleFontColor: '#3e3e3e', // 标题字体颜色
    titleFontSize: 15, // 标题字体大小

    showSplitArea: false,
    showSplitY: false,
    splitAreaColor: 'rgba(221,221,221,0.3)',
    splitYColor: 'rgba(221,221,221)',
    showWatermark: true,
    showTimelineSlider: true,
    frameBackgroundColor: 'rgba(255,255,255,0)',
    chartBackgroundColor: 'rgba(255,255,255,0)',

    showLegend: false,
    // legendPosition: "top",
    legendAlign: 'left', // 图例水平对齐
    legendVerticalAlign: 'top', // 图例垂直对齐
    legendArrangement: 'horizontal', // 图例排列方式
    legendFontColor: '#354052', // 图例字体颜色
    legendFontSize: 12, // 图例字体大小

    type: getDefaultChartType(format, extra), // 图表选择类型
    showDataZoom: ['timeline', 'panel', 'candlestick'].includes(format), // 缩放条显示
    showTimeline: ['timecut', 'timetable'].includes(format), // 时间轴显示

    // revert, // legend和y轴数据翻转

    labelFractionDigit: 2,
    showSeriesLabel: !['timeline', 'panel', 'candlestick'].includes(format),

    yAxisPosition: ['cut', 'timecut', 'group'].includes(format) ? 'right' : 'left',
    yAxis: {
      left: {
        showName: true,
        name: '',
        defaultMinMax: true,
        min: null,
        max: null,
        interval: null,
        namePos: 'end',
        log: false,
      },
      right: {
        showName: true,
        name: '',
        defaultMinMax: true,
        min: null,
        max: null,
        interval: null,
        namePos: 'end',
        log: false,
      },
    },
  }
}

// TODO 后续删除
// 获取默认子指标config【兼容处理】
export function defaultParameterConfig(indicator_format, extra = {}) {
  return getDefaultParameterConfig(indicator_format, extra)
}

// 获取默认子指标config
export function getDefaultParameterConfig(indicator_format = 'timeline', extra = {}) {
  return merge(
    {
      visible: true,

      lineType: 'solid', // solid dashed dotted
      lineWidth: 1,

      markerType: 'circle', // none circle rect emptyCircle, diamond,arrow,pin,roundRect
      markerWidth: 4,

      yAxisPosition: 'left',
      style: indicator_format === 'timeline' ? 'line' : 'bar',

      showLabel: false,
      labelFractionDigit: 2,

      otherOptions: {},
    },
    extra
  )
}

export const generatorUploadList = ['system_upload', 'user_upload']

export const generatorSystemList = ['system', 'calculate', ...generatorUploadList]

// 时序蜡烛图格式化
export function candlestickTimeFormatter(indicator) {
  let timeList = []
  const timeIndexMap = {}
  if (indicator.data && Array.isArray(indicator.data.axis)) {
    indicator.data.axis.forEach((v, i) => {
      if (v.name && typeof v.name === 'number') {
        timeList.push(v.name)
        timeIndexMap[String(dateHacker(v.name))] = i
      }
    })
  }
  if (timeList.length) {
    timeList = [...new Set(timeList)]
    timeList.sort((a, b) => a - b)
    if (indicator.frequency) {
      const minDate = timeList[0]
      const maxDate = timeList[timeList.length - 1]
      timeList = getTimeList(minDate, maxDate, indicator.frequency)
    }
    const axis = timeList.map(v => {
      const valKey = String(dateHacker(v))
      const newItem = {
        name: timeFormatter(v, indicator.frequency),
      }
      if (typeof timeIndexMap[valKey] === 'number') {
        const oldItem = indicator.data.axis[timeIndexMap[valKey]]
        if (oldItem) {
          return Object.assign({}, oldItem, newItem)
        }
      }
      return newItem
    })
    const series = indicator.data.series.map(seriesItem => {
      const oldValue = seriesItem.value
      return Object.assign({}, seriesItem, {
        value: timeList.map(v => {
          const valKey = String(dateHacker(v))
          if (typeof timeIndexMap[valKey] === 'number') {
            const oldItem = oldValue[timeIndexMap[valKey]]
            if (oldItem) {
              return oldItem
            }
          }
          return {}
        }),
      })
    })
    return Object.assign(clone(indicator.data), {
      axis,
      series,
    })
  }
  return indicator.data
}

// 处理indicator_values中的数据数据
const indicatorValuesProcessor = (
  value, // 原始值
  config, // indicator的配置
  { enabledDecimalFormat, defaultLabelFractionDigit } // 动态过滤的配置
) => {
  let finalValue = value
  // 处理小数点
  if (enabledDecimalFormat) {
    let labelFractionDigit = parseFloat(config && config.labelFractionDigit)
    if (isNaN(labelFractionDigit)) {
      labelFractionDigit = typeof defaultLabelFractionDigit === 'number' ? defaultLabelFractionDigit : 2
    }
    finalValue = numberFormatUnit(value, labelFractionDigit, false, null, true)
  }
  return finalValue
}

export function getTimeTableData(indicatorBase) {
  const indicator = JSON.parse(JSON.stringify(indicatorBase))
  const indicatorValues = indicator.indicator_values
  let resultData = []
  if (indicatorValues && Array.isArray(indicatorValues)) {
    resultData = indicatorValues.map(row => {
      const dateStr = timeFormatter(row.date, indicatorBase.frequency)
      const tableInfo = {
        date: dateStr,
        tableData: [],
        tableColumn: [],
      }
      if (Array.isArray(row.rows)) {
        row.rows.forEach((rowItem, rowIndex) => {
          if (rowIndex === 0) {
            rowItem.value.forEach((v, i) => {
              tableInfo.tableColumn.push({
                prop: `key_${i}`,
                label: v,
              })
            })
          } else {
            const newItem = {}
            rowItem.value.forEach((v, i) => {
              if (!tableInfo.tableColumn[i]) {
                tableInfo.tableColumn.push({
                  prop: `key_${i}`,
                  label: '',
                })
              }
              newItem[`key_${i}`] = v
            })
            tableInfo.tableData.push(newItem)
          }
        })
      }

      return tableInfo
    })
  }
  return resultData
}

// 根据指标生成表格数据
export function getIndicatorTableData(
  indicatorBase,
  // 是否根据原始频率格式化，用来处理timeline指标合并成非timeline的时间戳格式化
  originFrequencyFormatter = true,
  // 根据配置进行数据格式化
  enabledDecimalFormat = false,
  // 是否显示频率、单位、来源等额外信息
  showExtraInfo = false,
  // 是否把额外信息固定住
  isFixedExtraInfo = false
) {
  let columns = [] // 表头
  let tableData = [] // 普通表格内容
  const indicator = clone(indicatorBase)

  if (indicator) {
    // 外部默认
    const defaultLabelFractionDigit = 4
    const indicatorValuesProcessorConfig = {
      enabledDecimalFormat,
      defaultLabelFractionDigit,
    }
    const indicatorFormat = indicator.indicator_format
    let indicatorValues
    if (Array.isArray(indicator.indicator_values)) {
      indicatorValues = []
      indicator.indicator_values.forEach(v => {
        const visible = v.config ? v.config.visible : v.visible
        if (visible !== false) {
          indicatorValues.push(v)
        }
      })
    } else {
      indicatorValues = indicator.indicator_values
    }
    if (['timeline', 'panel', 'timecut'].includes(indicatorFormat)) {
      if (Array.isArray(indicatorValues)) {
        const dateMap = {}
        if (indicatorValues.length) {
          columns.push({
            prop: 'date',
            label: '日期',
          })
        }
        indicatorValues.forEach((v, i) => {
          const propKey = `value${i + 1}`
          columns.push({
            prop: propKey,
            label: (v.config && v.config.name) || v.name,
          })
          Array.isArray(v.values) &&
            v.values.forEach(valItem => {
              const dateKey = dateHacker(valItem.date)
              if (!dateMap[dateKey]) {
                dateMap[dateKey] = {}
              }
              dateMap[dateKey][propKey] = indicatorValuesProcessor(valItem.value, null, indicatorValuesProcessorConfig)
            })
        })
        const timeList = getDateTimeList(indicator)
        const frequency = indicator.aggregation_period || indicator.frequency
        timeList.reverse().forEach(v => {
          const dateKey = dateHacker(v)
          tableData.push(
            Object.assign(
              {
                date: frequency ? timeFormatter(v, frequency) : v,
              },
              dateMap[dateKey] || {}
            )
          )
        })
      }
    }
    if (['cut', 'group'].includes(indicatorFormat)) {
      if (Array.isArray(indicatorValues) && indicatorValues.length) {
        if (indicatorValues.length) {
          columns.push({
            prop: 'key',
            label: '数值',
          })
        }
        const columnMap = {}
        indicatorValues.forEach(v => {
          const dataItem = {
            key: timestampFormatter(v.name, originFrequencyFormatter && indicator.origin_frequency),
          }
          Array.isArray(v.values) &&
            v.values.forEach(valItem => {
              const columnKey = valItem.tag || indicator.header_name || indicator.show_name
              Object.assign(dataItem, {
                [columnKey]: indicatorValuesProcessor(valItem.value, null, indicatorValuesProcessorConfig),
              })
              columnMap[columnKey] = {
                prop: columnKey,
                label: columnKey,
              }
            })
          tableData.push(dataItem)
        })
        Object.values(columnMap).forEach(columnItem => {
          columns.push(columnItem)
        })
      } else if (indicator.data && Array.isArray(indicator.data.axis) && Array.isArray(indicator.data.series)) {
        const gridTableData = getGridTableData(
          indicator.data,
          originFrequencyFormatter && indicator.origin_frequency,
          null,
          indicatorValuesProcessorConfig
        )
        columns = gridTableData.columns
        tableData = gridTableData.data
      }
    }
    if (indicatorFormat === 'radar') {
      // 雷达图
      if (indicator.data && Array.isArray(indicator.data.axis) && Array.isArray(indicator.data.series)) {
        const gridTableData = getGridTableData(
          indicator.data,
          originFrequencyFormatter && indicator.origin_frequency,
          null,
          indicatorValuesProcessorConfig
        )
        columns = gridTableData.columns
        tableData = gridTableData.data
      }
    }
    if (indicatorFormat === 'candlestick') {
      // 蜡烛图
      if (indicator.data && Array.isArray(indicator.data.axis) && Array.isArray(indicator.data.series)) {
        if (indicator.frequency) {
          indicator.data = candlestickTimeFormatter(indicator)
        }
        const candlestickKeys = [
          {
            prop: 'open',
            label: '起始值',
          },
          {
            prop: 'close',
            label: '末尾值',
          },
          {
            prop: 'low',
            label: '最低',
          },
          {
            prop: 'high',
            label: '最高',
          },
        ]
        columns = [
          {
            prop: 'key',
            label: '数值',
          },
          ...candlestickKeys,
        ]
        const dataMap = {}
        indicator.data.axis.forEach((v, i) => {
          dataMap[i] = {
            key: timestampFormatter(v.name, originFrequencyFormatter && indicator.origin_frequency),
          }
        })
        indicator.data.series.forEach((v, i) => {
          if (v.type !== 'candlestick') {
            const propKey = `value${i + 1}`
            columns.push({
              prop: propKey,
              label: v.name,
            })
            Array.isArray(v.value) &&
              v.value.forEach((valItem, valIndex) => {
                if (!dataMap[valIndex]) {
                  dataMap[valIndex] = {}
                }
                dataMap[valIndex][propKey] = indicatorValuesProcessor(
                  valItem.value,
                  null,
                  indicatorValuesProcessorConfig
                )
              })
          } else {
            Array.isArray(v.value) &&
              v.value.forEach((valItem, valIndex) => {
                if (!dataMap[valIndex]) {
                  dataMap[valIndex] = {}
                }
                candlestickKeys.forEach(keyItem => {
                  dataMap[valIndex][keyItem.prop] = indicatorValuesProcessor(
                    valItem[keyItem.prop],
                    null,
                    indicatorValuesProcessorConfig
                  )
                })
              })
          }
        })
        tableData = Object.values(dataMap)
      }
    }
    if (indicatorFormat === 'map') {
      // 地图
      if (indicator.data && Array.isArray(indicator.data.series)) {
        columns.push({
          prop: 'key',
          label: '地区',
        })
        const typeList = indicator.data.series.map(v => v.type)
        if (typeList.includes('scatter')) {
          columns = [
            ...columns,
            {
              prop: 'longitude',
              label: '经度',
            },
            {
              prop: 'latitude',
              label: '纬度',
            },
          ]
        }
        const dataMap = {}
        indicator.data.series.forEach((v, i) => {
          const valueKey = `value${i}`
          columns.push({
            prop: valueKey,
            label: v.name,
          })
          if (Array.isArray(v.value)) {
            v.value.forEach(valItem => {
              if (!dataMap[valItem.name]) {
                dataMap[valItem.name] = {
                  key: valItem.name,
                }
              }
              dataMap[valItem.name][valueKey] = indicatorValuesProcessor(
                valItem.value,
                null,
                indicatorValuesProcessorConfig
              )
              if (v.type === 'scatter') {
                dataMap[valItem.name].longitude = valItem.coord && valItem.coord[0]
                dataMap[valItem.name].latitude = valItem.coord && valItem.coord[1]
              }
            })
          }
        })
        tableData = Object.values(dataMap)
      }
    }
    if (indicatorFormat === 'table') {
      if (indicatorValues && Array.isArray(indicatorValues.rows) && indicatorValues.rows.length) {
        indicatorValues.rows.forEach((rowItem, rowIndex) => {
          if (rowIndex === 0) {
            rowItem.value.forEach((v, i) => {
              columns.push({
                prop: `key_${i}`,
                label: v,
              })
            })
          } else {
            const newItem = {}
            rowItem.value.forEach((v, i) => {
              if (!columns[i]) {
                columns.push({
                  prop: `key_${i}`,
                  label: '',
                })
              }
              newItem[`key_${i}`] = v
            })
            tableData.push(newItem)
          }
        })
      }
    }
    if (indicatorFormat === 'scatter_sequence') {
      // 散点序列图
      if (indicator.data && Array.isArray(indicator.data.series)) {
        columns = [
          {
            prop: 'key',
            label: '序列',
          },
          {
            prop: 'axis',
            label: indicator.axis_name,
          },
          {
            prop: 'series',
            label: indicator.series_name,
          },
        ]
        indicator.data.series[0].value.forEach(v => {
          const dataItem = {
            key: indicator.frequency ? timeFormatter(v.sequence, indicator.frequency) : v.sequence,
            axis: v.coord[0],
            series: v.coord[1],
            sequence: v.sequence,
          }
          tableData.push(dataItem)
        })
        tableData.sort((a, b) => a.sequence - b.sequence)
      }
    }
    // 添加频率、单位、来源等额外信息
    if (showExtraInfo) {
      const indicatorValuesFinal = Array.isArray(indicatorValues) ? indicatorValues : []
      const extraList = [
        {
          label: '频率',
          getValue: keyIndex => {
            const valueItem = indicatorValuesFinal[keyIndex - 1]
            const frequency = (valueItem && valueItem.frequency) || indicator.frequency
            return frequency ? frequencyMap[frequency].slice(0, 1) : ''
          },
        },
        {
          label: '单位',
          getValue: keyIndex => {
            const valueItem = indicatorValuesFinal[keyIndex - 1]
            return (
              (valueItem && ((valueItem.config && valueItem.config.unit) || valueItem.unit)) || indicator.unit || ''
            )
          },
        },
        {
          label: '来源',
          getValue: keyIndex => {
            const valueItem = indicatorValuesFinal[keyIndex - 1]
            if (indicator.generator === 'calculate') {
              return indicator.owner_name || ''
            } else {
              return (valueItem && valueItem.data_source) || indicator.data_source || indicator.username || ''
            }
          },
        },
      ]

      for (let i = extraList.length - 1; i >= 0; i--) {
        let item = {
          isFixed: isFixedExtraInfo,
        }

        columns.forEach((columnItem, columnIndex) => {
          if (!columnIndex) {
            item[columnItem.prop] = extraList[i].label
          } else {
            item[columnItem.prop] = extraList[i].getValue(columnIndex)
          }
        })

        tableData.unshift(item)
      }
    }
  }
  return {
    columns,
    data: tableData,
  }
}

// 获取timeline、panel、timecut、candlestick的时间序列
export function getDateTimeList(indicator) {
  let timeList = []
  if (Array.isArray(indicator.indicator_values)) {
    indicator.indicator_values.forEach(v => {
      const visible = v.config ? v.config.visible : v.visible
      if (visible !== false && Array.isArray(v.values)) {
        v.values.forEach(valItem => {
          timeList.push(valItem.date)
        })
      }
    })
  }
  if (indicator.data && Array.isArray(indicator.data.axis)) {
    indicator.data.axis.forEach(v => {
      if (v.name && typeof v.name === 'number') {
        timeList.push(v.name)
      }
    })
  }
  if (timeList.length) {
    timeList = [...new Set(timeList)]
    timeList.sort((a, b) => a - b)
    const frequency = indicator.aggregation_period || indicator.frequency
    if (frequency) {
      const minDate = timeList[0]
      const maxDate = timeList[timeList.length - 1]
      timeList = getTimeList(minDate, maxDate, frequency)
    }
  }
  return timeList
}

// 获取grid类型的表格数据
export function getGridTableData(data, frequency, config, indicatorValuesProcessorConfig) {
  const columns = []
  let tableData = []
  columns.push({
    prop: 'key',
    label: '数值',
  })
  const dataMap = {}
  data.axis.forEach((v, i) => {
    dataMap[i] = {
      key: timestampFormatter(v.name, frequency),
    }
  })
  data.series.forEach((v, i) => {
    const propKey = `value${i + 1}`
    columns.push({
      prop: propKey,
      label: v.name,
    })
    Array.isArray(v.value) &&
      v.value.forEach((valItem, valIndex) => {
        if (!dataMap[valIndex]) {
          dataMap[valIndex] = {}
        }
        dataMap[valIndex][propKey] = indicatorValuesProcessor(
          typeof valItem === 'object' ? valItem.value : valItem,
          config,
          indicatorValuesProcessorConfig
        )
      })
  })
  tableData = Object.values(dataMap)
  return {
    columns,
    data: tableData,
  }
}

// 获取指标timeRange
export function getTimeRange(range, timeList) {
  let startIndex = 0
  let endIndex = null
  if (Array.isArray(range) && range.length === 2) {
    const newRange = range.map(v => dateHacker(v))
    timeList.every((v, i) => {
      const newVal = dateHacker(v)
      if (newVal >= newRange[0]) {
        startIndex = i
        return false
      }
      return true
    })
    timeList.every((v, i) => {
      const newVal = dateHacker(v)
      if (newRange[1] <= newVal) {
        endIndex = i
        return false
      }
      return true
    })
  }
  return [startIndex, endIndex]
}
