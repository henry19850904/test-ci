/* eslint-disable */
/*
 * @Author: WLL
 * @Date: 2020-09-03 17:42:34
 * @LastEditors: WLL
 * @LastEditTime: 2020-09-15 16:21:20
 * @Description: 数据处理相关
 */

/**
 * @description: 将指标数据转换成标准的编辑数据
 * @param {Object} indicator 指标数据
 * @return {Object} editData 标准编辑数据
 */

export function indicatorToEditData() {}

/**
 * @description: 将指标数据转换成标准的编辑数据
 * @param {Object} editData 标准编辑数据
 * @return {Object} indicator 指标数据
 */

export function editDataToIndicator() {}
