/* eslint-disable */

/*
 * @Author: WLL
 * @Date: 2020-03-05 15:10:08
 * @LastEditors: WLL
 * @LastEditTime: 2020-09-03 17:39:21
 * @Description: file content
 */
export * from './lib/common'
export * from './lib/frequency'
export * from './lib/time'
export * from './lib/indicator'

export function isNumeric(n) {
  return !Number.isNaN(parseFloat(n))
}
