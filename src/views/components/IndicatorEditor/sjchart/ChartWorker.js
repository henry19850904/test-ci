/* eslint-disable */
/**
 * Created by Xinhe on 2019/12/30.
 */
import getChartOptions from './lib/chart'
const registerWebworker = require('webworker-promise/lib/register')

registerWebworker(async options => {
  const result = await getChartOptions(options)
  return result
})
