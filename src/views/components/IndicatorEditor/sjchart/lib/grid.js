/* eslint-disable */
/**
 * Created by Xinhe on 2019/12/17.
 */
import { getColorGenerator, numberFormatUnit, strlen, getSamplingIndexs } from './utils'
import getTooltipConfig from './tooltip'
import { getDataZoomOptions } from './dataZoom'
import { merge } from 'lodash-es'

export default function (data, config, theme) {
  const {
    AXIS_COLOR,
    AXIS_LINE_COLOR,
    AXIS_LINE_TYPE,
    AXIS_TEXT_COLOR,
    AXIS_FONT_WEIGHT,
    AXIS_FONT_FAMILY,
    AXIS_LINE_HEIGHT,
    AXIS_FONT_SIZE,
    AXIS_TICK_COLOR,
    AXIS_SPLIT_LINE_COLOR,
    AXIS_NAME_TEXT_COLOR,

    DATAZOOM_FILTER_COLOR,

    LABEL_FONT_FAMILY,
    LABEL_FONT_SIZE,
    LABEL_LINE_HEIGHT,
    LABEL_FONT_WEIGHT,

    GRID_SPLIT_AREA_COLOR,

    LINE_SERIES_COLOR,
    BAR_SERIES_COLOR,

    CANDLESTICK_UP_COLOR,
    CANDLESTICK_DOWN_COLOR,
  } = theme
  let axisData = data.axis
  let seriesData = data.series

  const AXIS_TEXT_COLOR_FINAL = config.theme && config.theme.textColor ? config.theme.textColor : AXIS_TEXT_COLOR

  const DATAZOOM_FILTER_COLOR_FINAL =
    config.range && config.range.filterColor ? config.range.filterColor : DATAZOOM_FILTER_COLOR

  const colorGen = getColorGenerator(
    config.theme && config.theme.chartColors ? config.theme.chartColors : LINE_SERIES_COLOR || BAR_SERIES_COLOR
  )
  let colorIndex = 0
  const revertFinal = config.revert && !config.reverted
  if (revertFinal) {
    axisData = data.series.map(v => ({
      name: v.name,
    }))
    seriesData = data.axis.map(v => ({
      name: v.name,
      type: data.series.length ? data.series[0].type : 'bar',
      unit: data.series.length ? data.series[0].unit : '',
      stack: config.chartType.startsWith('stack') ? 'stack' : null,
      value: [],
    }))
    data.series.forEach(v => {
      Array.isArray(v.value) &&
        v.value.forEach((vItem, i) => {
          seriesData[i] && seriesData[i].value.push(vItem)
        })
    })
  }

  const typeAreaMapper = {}
  // 求出面积图
  seriesData.forEach(({ type, value }, position) => {
    let sum = -1
    if (type === 'area') {
      // 面积
      sum = value.reduce((a, b) => a + (b.value !== undefined && b.value !== null ? b.value : 0), 0)
    } else if (type === 'line') {
      // 堆叠面积
      sum = value.reduce((a, b) => a + (b.value !== undefined && b.value !== null ? b.value : 0), 0)
    }

    if (!Array.isArray(typeAreaMapper[type])) {
      typeAreaMapper[type] = []
    }
    typeAreaMapper[type].push({
      position,
      sum,
    })
  })
  Object.keys(typeAreaMapper).forEach(key => (typeAreaMapper[key] = typeAreaMapper[key].sort((a, b) => b.sum - a.sum)))

  let seriesZLevelList = []
  const series = merge(
    seriesData.map(({ name, type, value, unit, labelFractionDigit, ...rest }, index, arr) => {
      let data
      let defaultSeriesConfig = {
        visible: true,
        emphasis: {
          scale: false,
        },
        label: {
          show: config.showSeriesLabel || false,
          fontSize: LABEL_FONT_SIZE,
          lineHeight: LABEL_LINE_HEIGHT,
          fontFamily: LABEL_FONT_FAMILY,
          fontWeight: LABEL_FONT_WEIGHT,
        },
        [config.horizontal ? 'xAxisIndex' : 'yAxisIndex']: config.yAxisPosition === 'right' ? 1 : 0,
      }
      if (type === 'candlestick') {
        data = value.map(item => {
          const { open, close, high, low, prediction } = item
          item.value = undefined //remove value property
          return merge(
            {
              value: [open, close, low, high],
              itemStyle: {
                opacity: prediction ? 0.8 : 1,
              },
              unit,
            },
            item
          )
        })
        defaultSeriesConfig.itemStyle = {
          color: CANDLESTICK_UP_COLOR,
          color0: CANDLESTICK_DOWN_COLOR,
          borderColor: CANDLESTICK_UP_COLOR,
          borderColor0: CANDLESTICK_DOWN_COLOR,
        }
        defaultSeriesConfig.dimensions = ['base', '起始值', '末尾值', '最低', '最高']
        defaultSeriesConfig.barWidth = '30%'
      }

      if (type === 'line' || type === 'area' || type === 'scatter') {
        data = value.map((item, index, arr) => {
          let labelPosition = 'top'
          const { value, prediction } = item
          if (index - 1 >= 0 && index + 1 < arr.length) {
            if (arr[index - 1] > value && arr[index + 1] > value) {
              labelPosition = 'bottom'
            }
            if (arr[index - 1] < value && arr[index + 1] < value) {
              labelPosition = 'top'
            }
          }
          return merge(
            {
              label: {
                formatter: `${numberFormatUnit(
                  value,
                  typeof labelFractionDigit === 'number' ? labelFractionDigit : config.labelFractionDigit
                )}${unit === '%' ? '%' : ''}`,
                position: labelPosition,
              },
              itemStyle: {
                opacity: prediction ? 0.8 : 1,
              },
              unit,
            },
            item
          )
        })
        defaultSeriesConfig.symbol = 'circle'
        defaultSeriesConfig.symbolSize = 8
        defaultSeriesConfig.large = true
        defaultSeriesConfig.smooth = true
        defaultSeriesConfig.smoothMonotone = 'x'
        defaultSeriesConfig.connectNulls = true
        defaultSeriesConfig.itemStyle = {
          color: colorGen(colorIndex),
        }
        colorIndex++
      }
      if (type === 'bar') {
        data = value.map(item => {
          const { value, prediction } = item
          let labelPosition
          if (config.horizontal) {
            labelPosition = value >= 0 ? 'right' : 'left'
            if (rest.stack && arr.filter(({ stack }) => stack === rest.stack).length > 1) {
              labelPosition = `inside${labelPosition.charAt(0).toUpperCase()}${labelPosition.slice(1)}`
            }
          } else {
            labelPosition = value < 0 ? 'bottom' : 'top'
          }
          return merge(
            {
              label: {
                formatter: `${numberFormatUnit(
                  value,
                  typeof labelFractionDigit === 'number' ? labelFractionDigit : config.labelFractionDigit
                )}${unit === '%' ? '%' : ''}`,
                position: labelPosition,
              },
              itemStyle: {
                opacity: prediction ? 0.8 : 1,
              },
              unit,
            },
            item
          )
        })
        defaultSeriesConfig.large = true
        defaultSeriesConfig.barMaxWidth = '40%'
        defaultSeriesConfig.itemStyle = {
          color: colorGen(colorIndex),
        }
        colorIndex++
      }

      let z
      // 散点图>折线图>柱状图>面积图>堆叠柱状图>堆叠面积图
      if (type === 'scatter') {
        // 散点图
        z = 60000
      } else if (type === 'line' && !rest.stack) {
        // 折线图
        z = 50000
      } else if (type === 'bar' && !rest.stack) {
        // 柱状图
        z = 40000
      } else if (type === 'area' && !rest.stack) {
        z = 30000
        const sortPos = typeAreaMapper[type].findIndex(item => item.position === index) * 200
        z += sortPos
      } else if (type === 'bar') {
        z = 20000 + seriesData.length + 1 - index
      } else if (type === 'area') {
        // 堆叠面积
        z = 10000
        const sortPos = typeAreaMapper[type].findIndex(item => item.position === index) * 200
        z += sortPos
      } else {
        console.error('无法处理的类型：' + type)
        z = 0
      }

      // 如果不为堆叠柱状图就+上当前index， 堆叠柱状图是反过来加，否则上面的柱子会盖住下面的文字
      if (!(type === 'bar' && rest.stack)) {
        // 保证index唯一性
        z += index
      }
      seriesZLevelList.push({
        index,
        z,
      })

      if (type === 'area') {
        defaultSeriesConfig.symbol = 'circle'
        defaultSeriesConfig.symbolSize = 0.001
        defaultSeriesConfig.areaStyle = config.areaStyle ? config.areaStyle : {}
        type = 'line'
      }

      // 恢复散点图类型为line
      if (type === 'scatter') {
        type = 'line'
      }
      return {
        name,
        type,
        data,
        unit,
        ...defaultSeriesConfig,
        ...rest,
      }
    }),
    config.series,
    Array.isArray(config.series) &&
      config.series.map(item => {
        const newItem = {}
        if (typeof config.showSeriesLabel === 'boolean') {
          Object.assign(newItem, {
            label: {
              show: config.showSeriesLabel,
            },
          })
        }
        const indexKey = config.horizontal ? 'xAxisIndex' : 'yAxisIndex'
        if (!item[indexKey]) {
          Object.assign(newItem, {
            [indexKey]: config.yAxisPosition === 'right' ? 1 : 0,
          })
        }
        // 清除相反轴index
        const anotherKey = config.horizontal ? 'yAxisIndex' : 'xAxisIndex'
        if (item[anotherKey]) {
          Object.assign(newItem, {
            [anotherKey]: 0,
          })
        }
        return newItem
      })
  )

  seriesZLevelList = seriesZLevelList
    .sort((a, b) => a.z - b.z)
    .map((item, index) =>
      Object.assign({
        z: index + 1,
        index: item.index,
      })
    )
  series.forEach((item, index) => {
    if (!item.z) {
      const zObj = seriesZLevelList.find(item => item.index === index)
      item.z = zObj.z
    }
  })

  const xAxis = merge(
    {
      type: 'category',
      data: axisData.map(({ name, ...rest }) => {
        return {
          value: name,
          ...rest,
        }
      }),
      axisLine: {
        show: true,
        lineStyle: {
          color: AXIS_LINE_COLOR,
          width: 1,
          type: AXIS_LINE_TYPE,
        },
      },
      axisTick: {
        length: 8,
        padding: [17, 0, 0, 0],
        lineStyle: {
          color: AXIS_TICK_COLOR,
        },
      },
      boundaryGap: true,
      axisLabel: {
        show: true,
        color: AXIS_TEXT_COLOR_FINAL,
        fontWeight: AXIS_FONT_WEIGHT,
        fontFamily: AXIS_FONT_FAMILY,
        lineHeight: AXIS_LINE_HEIGHT,
        fontSize: AXIS_FONT_SIZE,
        showMinLabel: true,
        showMaxLabel: true,
      },
      splitArea: {
        interval: 2,
        show: false,
        areaStyle: {
          color: [GRID_SPLIT_AREA_COLOR, 'transparent'],
        },
      },
    },
    config.xAxis
  )

  //统计y轴,最多2个,获取合并后的单位
  const yAxisMap = {
    0: { show: false, unit: undefined },
    1: { show: false, unit: undefined },
  }
  series.forEach(serieItem => {
    const unit = serieItem.unit
    const key = config.horizontal ? serieItem.xAxisIndex : serieItem.yAxisIndex
    yAxisMap[key].show = true
    if (yAxisMap[key].unit === undefined) {
      yAxisMap[key].unit = unit
    } else if (yAxisMap[key].unit === unit) {
      yAxisMap[key].unit = unit
    } else {
      yAxisMap[key].unit = null
    }
  })
  let yAxis = merge(
    [0, 1].map(yAxisIndex => {
      const { show, unit } = yAxisMap[yAxisIndex]
      return {
        type: 'value',
        defaultMinMax: true,
        position: yAxisIndex === 0 ? (config.horizontal ? 'top' : 'left') : config.horizontal ? 'bottom' : 'right',
        show,
        unit,
        axisLine: {
          show: true,
          lineStyle: {
            color: config.theme && config.theme.lineColor ? config.theme.lineColor : AXIS_COLOR,
            width: 1,
          },
        },
        scale: true,
        axisTick: {
          show: false,
        },
        splitLine: {
          show: false,
          lineStyle: {
            color: AXIS_SPLIT_LINE_COLOR,
          },
        },
        axisLabel: {
          show: true,
          color: AXIS_TEXT_COLOR_FINAL,
          fontWeight: AXIS_FONT_WEIGHT,
          fontFamily: AXIS_FONT_FAMILY,
          lineHeight: AXIS_LINE_HEIGHT,
          fontSize: AXIS_FONT_SIZE,
        },
        nameLocation: 'end',
        nameTextStyle: {
          color: config.theme && config.theme.textColor ? config.theme.textColor : AXIS_NAME_TEXT_COLOR,
          fontWeight: AXIS_FONT_WEIGHT,
          fontFamily: AXIS_FONT_FAMILY,
          lineHeight: AXIS_LINE_HEIGHT,
          fontSize: AXIS_FONT_SIZE,
        },
      }
    }),
    config.yAxis
  )

  yAxis.forEach(axis => {
    if (axis.defaultMinMax) {
      axis.interval = null
    }
    if (!config.horizontal) {
      axis.nameRotate = axis.nameLocation === 'middle' ? (axis.position === 'right' ? -90 : 90) : undefined
    } else {
      axis.nameRotate = axis.nameLocation !== 'middle' ? -90 : undefined
      axis.axisLabel.rotate = axis.position === 'top' ? 90 : -90
    }
    axis.nameGap = axis.nameLocation === 'middle' ? 40 : 15
    axis.type = axis.log ? 'log' : 'value'
    axis.min = axis.type === 'log' ? undefined : axis.min
    axis.max = axis.type === 'log' ? undefined : axis.max
    axis.name = axis.showName ? axis.name || axis.unit || undefined : undefined
  })

  const tooltip = merge(getTooltipConfig(config, theme), { trigger: 'axis' }, config.tooltip)
  //grid
  const getGridOffsetByYAxisIndex = yAxisIndex => {
    return yAxis[yAxisIndex].show ? Math.max(30, (strlen(yAxis[yAxisIndex].name || '') / 2) * 6) : 30
  }

  let grid
  if (!config.horizontal) {
    grid = {
      left: getGridOffsetByYAxisIndex(0),
      right: getGridOffsetByYAxisIndex(1),
      top: yAxis[0].name || yAxis[1].name ? 40 : 30,
    }
  } else {
    grid = {
      bottom: getGridOffsetByYAxisIndex(0),
      top: getGridOffsetByYAxisIndex(1),
      right: yAxis[0].name || yAxis[1].name ? 40 : 30,
    }
  }
  if (config.sampling && config.sampling.open) {
    // 数据采样
    const samplingIndexs = getSamplingIndexs(xAxis.data.length, config.sampling.size)
    if (config.range.show) {
      config.range.labels = config.range.labels.filter((v, index) => samplingIndexs.includes(index))
      const maxIndex = config.range.labels.length - 1
      config.range.range = [0, maxIndex]
      config.range.value = [
        Array.isArray(config.range.value) && config.range.value[0] <= maxIndex ? config.range.value[0] : 0,
        Array.isArray(config.range.value) && config.range.value[1] <= maxIndex ? config.range.value[1] : maxIndex,
      ]
    }
    xAxis.data = xAxis.data.filter((v, index) => samplingIndexs.includes(index))
    series.forEach(item => {
      item.data = item.data.filter((v, index) => samplingIndexs.includes(index))
    })
  }
  return merge(
    {
      tooltip,
      xAxis: config.horizontal ? yAxis : xAxis,
      yAxis: config.horizontal ? xAxis : yAxis,
      series: series.filter(({ visible }) => visible),
      grid,
    },
    getDataZoomOptions(config, grid, AXIS_TEXT_COLOR_FINAL, DATAZOOM_FILTER_COLOR_FINAL)
  )
}
