/* eslint-disable */
/*
 * @Author: WLL
 * @Date: 2020-09-21 11:46:35
 * @LastEditors: WLL
 * @LastEditTime: 2020-10-19 10:53:11
 * @Description: file content
 */
/**
 * Created by Xinhe on 2019/12/17.
 */
import { getColorGenerator } from './utils'
import getTooltipConfig from './tooltip'
// import { merge } from "lodash-es";

export default function (data, config, theme) {
  const {
    PIE_SERIES_COLOR,
    PIE_LABEL_FONT_SIZE,
    PIE_LABEL_LINE_HEIGHT,
    PIE_LABEL_FONT_FAMILY,
    PIE_LABEL_WEIGHT,
  } = theme

  let seriesData = data.series
  const colorGen = getColorGenerator(
    config.theme && config.theme.chartColors ? config.theme.chartColors : PIE_SERIES_COLOR
  )
  if (config.sampling && config.sampling.open) {
    seriesData = seriesData.slice(0, config.sampling.size)
  }
  const series = {
    type: 'pie',
    label: {
      fontSize: PIE_LABEL_FONT_SIZE,
      lineHeight: PIE_LABEL_LINE_HEIGHT,
      fontFamily: PIE_LABEL_FONT_FAMILY,
      fontWeight: PIE_LABEL_WEIGHT,
    },
    radius: config.chartType === 'donut_pie' ? ['50%', '75%'] : [0, '75%'],
    data: seriesData.map(({ name, value }, index) => {
      return {
        name,
        value,
        itemStyle: {
          color: colorGen(index),
        },
      }
    }),
  }

  const tooltip = getTooltipConfig(config, theme)
  return {
    tooltip,
    grid: {
      borderWidth: 0,
    },
    series,
  }
}
