/* eslint-disable */

/*
 * @Author: WLL
 * @Date: 2020-05-15 17:54:39
 * @LastEditors: WLL
 * @LastEditTime: 2020-05-15 17:54:47
 * @Description: get tooptip config
 */
export default function (config, theme) {
  const {
    TOOLTIP_BORDER_COLOR,
    TOOLTIP_BACKGROUND,
    TOOLTIP_BOX_SHADOW,
    TOOLTIP_TEXT_COLOR,
    TOOLTIP_FONT_WEIGHT,
    TOOLTIP_FONT_FAMILY,
    TOOLTIP_LINE_HEIGHT,
    TOOLTIP_FONT_SIZE,
  } = theme

  return {
    confine: true,
    backgroundColor: TOOLTIP_BACKGROUND,
    borderColor: TOOLTIP_BORDER_COLOR,
    borderWidth: 1,
    extraCssText: TOOLTIP_BOX_SHADOW,
    textStyle: {
      color: TOOLTIP_TEXT_COLOR,
      lineHeight: TOOLTIP_LINE_HEIGHT,
      fontSize: TOOLTIP_FONT_SIZE,
      fontFamily: TOOLTIP_FONT_FAMILY,
      fontWeight: TOOLTIP_FONT_WEIGHT,
    },
    padding: [10, 20],
  }
}
