/* eslint-disable */
/*
 * @Author: WLL
 * @Date: 2020-05-15 17:22:46
 * @LastEditors: WLL
 * @LastEditTime: 2021-01-05 10:34:39
 * @Description: dataZoom工具类
 */

/**
 * @description: get dataZoom related options
 * @param {Object} config
 * @param {Object} grid echarts grid options
 * @param {Object} textColor
 * @return: echarts options
 */
export const DATAZOOM_GRID_BOTTOM = 45
export const DATAZOOM_LEFT = 80
export const DATAZOOM_RIGHT = 80

export const getDataZoomOptions = (config, grid, textColor, fillerColor) => {
  const options = {
    dataZoom: null,
  }
  const rangeValue =
    Array.isArray(config.range.value) && config.range.value.length > 1 ? config.range.value : config.range.range
  if (config.range.show && Array.isArray(rangeValue) && rangeValue.length > 1) {
    const isNative = config.range.type === 'native'
    options.dataZoom = [
      {
        type: 'slider',
        show: isNative,
        startValue: rangeValue[0],
        endValue: rangeValue[1],
        fillerColor,
        textStyle: {
          color: textColor,
        },
        orient: 'horizontal',
        [config.horizontal ? 'yAxisIndex' : 'xAxisIndex']: 0,
        left: DATAZOOM_LEFT,
        right: DATAZOOM_RIGHT,
      },
    ]
    if (isNative) {
      options.grid = {
        bottom: grid && grid.bottom ? Math.max(grid.bottom, DATAZOOM_GRID_BOTTOM) : DATAZOOM_GRID_BOTTOM,
      }
    }
  }
  return options
}
