/* eslint-disable */

/**
 * Created by Xinhe on 2019/12/18.
 */
import chroma from 'chroma-js'
import { merge } from 'lodash-es'

import { getDefaultIndicatorStyles } from '../../../utils/lib/indicator'

export { numberFormatUnit } from '../../../utils/lib/common'

import { isNumber as isNumber2 } from 'lodash-es'
export const isNumber = isNumber2

export function strlen(str) {
  var len = 0
  for (let i = 0; i < str.length; i++) {
    const c = str.charCodeAt(i)
    // 单字节加1
    if ((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f)) {
      len++
    } else {
      len += 2
    }
  }
  return len
}

export const INIT_COLOR_ERROR_MSG = '初始色盘错误！'
export function getColorGenerator(initColor) {
  if (!(Array.isArray(initColor) && initColor.length >= 10)) {
    throw new Error(INIT_COLOR_ERROR_MSG)
  }
  const colorArr = [...initColor]
  const offset = 10
  return index => {
    while (colorArr.length - 1 < index) {
      const i = colorArr.length
      const scale = chroma.scale([colorArr[i - offset], colorArr[i - offset + 1]])
      colorArr.push(scale(0.5).hex())
    }
    return colorArr[index]
  }
}

export function valueFormatter(value = null) {
  if (value === null) return '-'
  if (Math.abs(value) >= 1000) {
    return value.toLocaleString(undefined, { maximumFractionDigits: 0 })
  }
  if (Math.abs(value) < 1) {
    return value.toLocaleString(undefined, {
      minimumSignificantDigits: 1,
      maximumSignificantDigits: 2,
    })
  }
  return value.toLocaleString(undefined, { maximumFractionDigits: 2 })
}

function quantityExponent(val) {
  return Math.floor(Math.log(val) / Math.LN10)
}

function nice(val, round) {
  var exponent = quantityExponent(val)
  var exp10 = Math.pow(10, exponent)
  var f = val / exp10 // 1 <= f < 10
  var nf
  if (round) {
    if (f < 1.5) {
      nf = 1
    } else if (f < 2.5) {
      nf = 2
    } else if (f < 4) {
      nf = 3
    } else if (f < 7) {
      nf = 5
    } else {
      nf = 10
    }
  } else {
    if (f < 1) {
      nf = 1
    } else if (f < 2) {
      nf = 2
    } else if (f < 3) {
      nf = 3
    } else if (f < 5) {
      nf = 5
    } else {
      nf = 10
    }
  }
  val = nf * exp10

  // Fix 3 * 0.1 === 0.30000000000000004 issue (see IEEE 754).
  // 20 is the uppper bound of toFixed.
  return exponent >= -20 ? +val.toFixed(exponent < 0 ? -exponent : 0) : val
}

function getPrecisionSafe(val) {
  var str = val.toString()

  // Consider scientific notation: '3.4e-12' '3.4e+12'
  var eIndex = str.indexOf('e')
  if (eIndex > 0) {
    var precision = +str.slice(eIndex + 1)
    return precision < 0 ? -precision : 0
  } else {
    var dotIndex = str.indexOf('.')
    return dotIndex < 0 ? 0 : str.length - 1 - dotIndex
  }
}

function getIntervalPrecision(interval) {
  // Tow more digital for tick.
  return getPrecisionSafe(interval) + 2
}

function roundNumber(x, precision, returnStr) {
  if (precision == null) {
    precision = 10
  }
  // Avoid range error
  precision = Math.min(Math.max(0, precision), 20)
  x = (+x).toFixed(precision)
  return returnStr ? x : +x
}

const calc = (max, min) => {
  var span = max - min
  let splitNumber = 5
  var interval = nice(span / splitNumber, true)
  // Tow more digital for tick.
  var precision = getIntervalPrecision(interval)
  // Niced extent inside original extent
  return {
    min: roundNumber(Math.floor(min / interval) * interval, precision),
    max: roundNumber(Math.ceil(max / interval) * interval, precision),
  }
}

const getRoundNumber = (num, type) => {
  if (Math.abs(num) >= 1 || Math.abs(num) === 0) {
    return Math[type](num)
  }

  let n = num
  let i = 1
  while (Math.abs(n) < 10) {
    n = n * 10
    i *= 10
  }
  return Math[type](n) / i
}

export const getDefaultMinMax = (value, unit, stack = false) => {
  let min = getRoundNumber(value.min - (value.max - value.min) * 0.2, 'floor')
  min = min < 0 ? (value.min < 0 ? min : 0) : min

  let max = getRoundNumber(value.max + (value.max - value.min) * 0.2, 'ceil')

  if (min === max) {
    if (min < 0) {
      max = 0
    } else {
      min = 0
    }
  }
  if (unit === '%') {
    if (value.max <= 100 && max > 100) {
      max = 100
    }
  }

  if (stack) {
    if (min > 0) {
      min = 0
    }
  }
  return calc(max, min)
}

export const getSamplingIndexs = (dataNum, samplingNum) => {
  const indexArr = []
  const interval = Math.ceil(dataNum / samplingNum)
  for (let i = 0; i < dataNum; i += interval) {
    indexArr.push(i)
  }
  if (indexArr[indexArr.length - 1] !== dataNum - 1) {
    indexArr.push(dataNum - 1)
  }
  return indexArr
}

export const getRangeConfig = (axis, timeRange) => {
  const rangeConfig = {
    labels: [],
    range: [],
    value: null,
  }
  if (Array.isArray(axis) && axis.length) {
    rangeConfig.labels = axis.map(({ name }) => name)
    rangeConfig.range = [0, axis.length - 1]
    rangeConfig.value = [0, axis.length - 1]
    if (Array.isArray(timeRange) && timeRange.length) {
      if (timeRange[0] < axis.length) {
        rangeConfig.value[0] = timeRange[0]
      }
      if (timeRange[1] !== null && timeRange[1] < axis.length) {
        rangeConfig.value[1] = timeRange[1]
      }
    }
  }
  return rangeConfig
}

// 根据 指标type 映射 图表type
export function getChartType(type) {
  if (type === 'scatter') {
    return 'scatter'
  }

  if (['bar', 'stack_bar', 'horizontal_bar', 'stack_horizontal_bar'].includes(type)) {
    return 'bar'
  }

  if (['area', 'stack_area'].includes(type)) {
    return 'area'
  }

  return 'line'
}

// 根据 styles 获取是否显示 缩放条
export function getRangeShow(styles) {
  return styles && styles.showDataZoom && !['pie', 'donut_pie', 'map', 'scatter_map', 'radar'].includes(styles.type)
}

export function getStylesByIndicator(indicator) {
  let finalStyles = indicator.styles
  if (indicator.indicator_format) {
    finalStyles = merge(
      getDefaultIndicatorStyles(indicator.indicator_format, indicator.default_style),
      indicator.styles || {}
    )
  }
  return finalStyles
}

export function getStyleConfigByIndicator(indicator, format) {
  const finalStyles = getStylesByIndicator(indicator)
  return getStyleConfig(finalStyles, format)
}

export function getStyleConfig(finalStyles, format) {
  let style_config = {}
  if (finalStyles) {
    merge(style_config, {
      title: {
        show: finalStyles.showTitle,
        textAlign: finalStyles.titleAlign,
        color: finalStyles.titleFontColor,
        // TODO 后续和legend统一
        fontSize: `${finalStyles.titleFontSize}px`,
        lineHeight: `${finalStyles.titleFontSize}px`,
      },
      legend: {
        show: finalStyles.showLegend,
        align: finalStyles.legendAlign, // 图例水平对齐
        verticalAlign: finalStyles.legendVerticalAlign, // 图例垂直对齐
        arrangement: finalStyles.legendArrangement, // 图例排列方式
        textColor: finalStyles.legendFontColor,
        textFontSize: `${finalStyles.legendFontSize}px`,
      },
      horizontal: ['horizontal_bar', 'stack_horizontal_bar'].includes(finalStyles.type),
      chartType: finalStyles.type,
    })

    if (finalStyles.frameBackgroundColor && finalStyles.chartBackgroundColor) {
      merge(style_config, {
        chart: {
          backgroundColor: finalStyles.frameBackgroundColor,
        },
        grid: {
          backgroundColor: finalStyles.chartBackgroundColor,
        },
        backgroundColor: finalStyles.frameBackgroundColor,
      })
    }
    if (finalStyles.rangeColor || finalStyles.textColor) {
      merge(style_config, {
        range: {
          color: finalStyles.rangeColor,
          textColor: finalStyles.textColor,
        },
      })
    }
    if (finalStyles.sliderColor || finalStyles.textColor) {
      merge(style_config, {
        slider: {
          color: finalStyles.sliderColor,
          textColor: finalStyles.textColor,
        },
      })
    }
    if (finalStyles.textColor || finalStyles.lineColor || finalStyles.chartColors) {
      merge(style_config, {
        grid: {
          borderColor: finalStyles.lineColor,
        },
        theme: {
          textColor: finalStyles.textColor,
          lineColor: finalStyles.lineColor,
          chartColors: finalStyles.chartColors,
        },
      })
    }
    // 转化xyAxis
    if (format === 'grid' || format === 'scatter_sequence') {
      merge(style_config, {
        xAxis: {
          splitArea: {
            show: finalStyles.showSplitArea,
            areaStyle: {
              color: [finalStyles.splitAreaColor, 'transparent'],
            },
          },
        },
        yAxis: [finalStyles.yAxis.left, finalStyles.yAxis.right].map(({ namePos, ...rest }) => {
          return {
            nameLocation: namePos,
            splitLine: {
              show: finalStyles.showSplitY,
              lineStyle: {
                color: finalStyles.splitYColor,
              },
            },
            ...rest,
          }
        }),
      })
    }

    if (typeof finalStyles.labelFractionDigit === 'number') {
      merge(style_config, {
        labelFractionDigit: finalStyles.labelFractionDigit,
      })
    }

    if (typeof finalStyles.showSeriesLabel === 'boolean') {
      Object.assign(style_config, {
        showSeriesLabel: finalStyles.showSeriesLabel,
      })
    }

    if (finalStyles.yAxisPosition) {
      merge(style_config, {
        yAxisPosition: finalStyles.yAxisPosition,
      })
    }

    if (typeof finalStyles.revert === 'boolean') {
      merge(style_config, {
        revert: finalStyles.revert,
      })
    }
  }
  return style_config
}
