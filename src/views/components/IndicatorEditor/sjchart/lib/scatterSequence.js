/* eslint-disable */

/*
 * @Author: WLL
 * @Date: 2020-12-09 11:16:37
 * @LastEditors: WLL
 * @LastEditTime: 2020-12-21 15:13:15
 * @Description: 散点序列
 */
import { getColorGenerator, strlen } from './utils'
import getTooltipConfig from './tooltip'
import { merge } from 'lodash-es'

export default function (data, config, theme) {
  const {
    AXIS_COLOR,
    AXIS_LINE_COLOR,
    AXIS_LINE_TYPE,
    AXIS_TEXT_COLOR,
    AXIS_FONT_WEIGHT,
    AXIS_FONT_FAMILY,
    AXIS_LINE_HEIGHT,
    AXIS_FONT_SIZE,
    AXIS_TICK_COLOR,
    AXIS_SPLIT_LINE_COLOR,
    AXIS_NAME_TEXT_COLOR,

    GRID_SPLIT_AREA_COLOR,

    SCATTER_SEQUENCE_START_COLOR,
    SCATTER_SEQUENCE_END_COLOR,

    LINE_SERIES_COLOR,
  } = theme

  const AXIS_TEXT_COLOR_FINAL = config.theme && config.theme.textColor ? config.theme.textColor : AXIS_TEXT_COLOR

  const colorGen = getColorGenerator(
    config.theme && config.theme.chartColors ? config.theme.chartColors : LINE_SERIES_COLOR
  )

  let colorIndex = 0

  const series = merge(
    data.series.map(v => {
      const seriesItem = {
        type: 'line',
        name: data.config && data.config.name, // TODO 临时单条散点序列处理
        smooth: true,
        symbol: 'circle',
        symbolSize: 6,
        showAllSymbol: true,
        itemStyle: {
          color: colorGen(colorIndex),
        },
        yAxisIndex: config.yAxisPosition === 'right' ? 1 : 0,
        data: v.value
          .sort((a, b) => a.sequence - b.sequence)
          .map((val, i) => {
            const itemStyle = {}
            if (i === 0) {
              itemStyle.color = SCATTER_SEQUENCE_START_COLOR
            } else if (i === v.value.length - 1) {
              itemStyle.color = SCATTER_SEQUENCE_END_COLOR
            }
            const dataItem = {
              value: [...val.coord, val.sequence],
              itemStyle,
            }
            return dataItem
          }),
      }
      colorIndex += 1
      return seriesItem
    }),
    config.series
  )

  const xAxis = merge(
    {
      type: 'value',
      name: data.config && data.config.axis_name,
      nameLocation: config.yAxisPosition === 'right' ? 'start' : 'end',
      axisLine: {
        show: true,
        onZero: false,
        lineStyle: {
          color: AXIS_LINE_COLOR,
          width: 1,
          type: AXIS_LINE_TYPE,
        },
      },
      axisTick: {
        length: 8,
        padding: [17, 0, 0, 0],
        lineStyle: {
          color: AXIS_TICK_COLOR,
        },
      },
      splitNumber: 3,
      scale: true,
      boundaryGap: true,
      axisLabel: {
        show: true,
        color: AXIS_TEXT_COLOR_FINAL,
        fontWeight: AXIS_FONT_WEIGHT,
        fontFamily: AXIS_FONT_FAMILY,
        lineHeight: AXIS_LINE_HEIGHT,
        fontSize: AXIS_FONT_SIZE,
        showMinLabel: true,
        showMaxLabel: true,
      },
      splitLine: {
        show: false,
      },
      splitArea: {
        interval: 2,
        show: false,
        areaStyle: {
          color: [GRID_SPLIT_AREA_COLOR, 'transparent'],
        },
      },
    },
    config.xAxis
  )

  const yAxisMap = {
    0: { show: false },
    1: { show: false },
  }

  series.forEach(serieItem => {
    const key = serieItem.yAxisIndex
    yAxisMap[key].show = true
  })

  const yAxis = merge(
    [0, 1].map(yAxisIndex => {
      const { show } = yAxisMap[yAxisIndex]
      return {
        type: 'value',
        unit: data.config && data.config.series_name,
        scale: true,
        defaultMinMax: true,
        position: yAxisIndex === 0 ? 'left' : 'right',
        show,
        axisLine: {
          show: true,
          lineStyle: {
            color: config.theme && config.theme.lineColor ? config.theme.lineColor : AXIS_COLOR,
            width: 1,
          },
        },
        axisTick: {
          show: false,
        },
        splitLine: {
          show: false,
          lineStyle: {
            color: AXIS_SPLIT_LINE_COLOR,
          },
        },
        axisLabel: {
          show: true,
          color: AXIS_TEXT_COLOR_FINAL,
          fontWeight: AXIS_FONT_WEIGHT,
          fontFamily: AXIS_FONT_FAMILY,
          lineHeight: AXIS_LINE_HEIGHT,
          fontSize: AXIS_FONT_SIZE,
        },
        nameLocation: 'end',
        nameTextStyle: {
          color: config.theme && config.theme.textColor ? config.theme.textColor : AXIS_NAME_TEXT_COLOR,
          fontWeight: AXIS_FONT_WEIGHT,
          fontFamily: AXIS_FONT_FAMILY,
          lineHeight: AXIS_LINE_HEIGHT,
          fontSize: AXIS_FONT_SIZE,
        },
      }
    }),
    config.yAxis
  )

  yAxis.forEach(axis => {
    if (axis.defaultMinMax) {
      axis.interval = null
    }

    axis.nameRotate = axis.nameLocation === 'middle' ? (axis.position === 'right' ? -90 : 90) : undefined

    axis.nameGap = axis.nameLocation === 'middle' ? 38 : 15
    axis.type = axis.log ? 'log' : 'value'
    axis.min = axis.log ? undefined : axis.min
    axis.max = axis.log ? undefined : axis.max
    axis.name = axis.showName ? axis.name || axis.unit : undefined
  })

  const getGridOffsetByYAxisIndex = yAxisIndex => {
    return yAxis[yAxisIndex].show ? Math.max(30, (strlen(yAxis[yAxisIndex].name || '') / 2) * 6) : 30
  }

  const grid = {
    left: getGridOffsetByYAxisIndex(0),
    right: getGridOffsetByYAxisIndex(1),
    top: yAxis[0].name || yAxis[1].name ? 40 : 30,
  }

  const gridKey = config.yAxisPosition === 'right' ? 'left' : 'right'
  grid[gridKey] = Math.max(grid[gridKey], strlen(xAxis.name || '') * 6 + 20)

  return {
    tooltip: getTooltipConfig(config, theme),
    grid,
    xAxis,
    yAxis,
    series,
  }
}
