/* eslint-disable */
/**
 * Created by Xinhe on 2019/12/17.
 */
import { getColorGenerator } from './utils'
import getTooltipConfig from './tooltip'
import { merge } from 'lodash-es'

function formatValue(val) {
  if (typeof val === 'number') {
    return val
  }
  return val || '-'
}

export default function (data, config, theme) {
  const {
    AXIS_COLOR,
    RADAR_SERIES_COLOR,
    AXIS_TEXT_COLOR,
    AXIS_FONT_WEIGHT,
    AXIS_FONT_FAMILY,
    AXIS_LINE_HEIGHT,
    AXIS_FONT_SIZE,
  } = theme
  const colorGen = getColorGenerator(
    config.theme && config.theme.chartColors ? config.theme.chartColors : RADAR_SERIES_COLOR
  )

  let axisData = data.axis
  let seriesData = data.series
  //默认逆时针
  if (config.clockwise) {
    const mirrorArray = arr => [arr[0], ...arr.slice(1).reverse()]
    axisData = mirrorArray(axisData)
    seriesData = seriesData.map(item => {
      return {
        ...item,
        value: mirrorArray(item.value),
      }
    })
  }

  // const legend = getLegendConfig(config, theme);
  // legend.icon = `path://${radarLegend}`;
  const radar = {
    name: {
      show: true,
      color: AXIS_TEXT_COLOR,
      fontWeight: AXIS_FONT_WEIGHT,
      fontFamily: AXIS_FONT_FAMILY,
      lineHeight: AXIS_LINE_HEIGHT,
      fontSize: AXIS_FONT_SIZE,
    },
    splitNumber: config.split_number,
    axisLine: {
      show: true,
      lineStyle: {
        color: AXIS_COLOR,
        width: 1,
      },
    },
    splitArea: {
      show: false,
    },
    indicator: config.revert
      ? seriesData.map(({ name }) => ({ name }))
      : axisData.map(({ name, max, min }) => {
          return {
            name,
            max,
            min,
          }
        }),
  }
  const series = {
    type: 'radar',
    symbol: 'circle',
    symbolSize: 7,

    data: config.revert
      ? axisData.map(({ name }, index) => ({
          name,
          value: seriesData.map(({ value }) =>
            formatValue(typeof value[index] === 'object' ? value[index].value : value[index])
          ),
          itemStyle: {
            color: colorGen(index),
          },
          lineStyle: {
            width: 2,
          },
        }))
      : seriesData.map(({ name, value }, index) => {
          return {
            name,
            value: value.map(v => formatValue(typeof v === 'object' ? v.value : v)),
            itemStyle: {
              color: colorGen(index),
            },
            lineStyle: {
              width: 2,
            },
          }
        }),
  }

  const mergeData = []
  Array.isArray(config.series) &&
    config.series.forEach((item, index) => {
      if (index < series.data.length) {
        mergeData.push({
          itemStyle: item.itemStyle || {},
        })
      }
    })

  const tooltip = getTooltipConfig(config, theme)
  return {
    tooltip,
    // legend,
    radar,
    series: merge(series, { data: mergeData }),
  }
}
