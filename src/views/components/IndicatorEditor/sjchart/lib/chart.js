/* eslint-disable */
/**
 * Created by Xinhe on 2019/12/17.
 */
import indicatorMiddleware from './middleware/middleware'
import getRadarOptions from './radar'
import getGridOptions from './grid'
import getMapOptions from './map'
import getPieOptions from './pie'
import getScatterSequenceOptions from './scatterSequence'
import defaultTheme from './theme'
import { timeFormatter } from '../../utils/lib/time'
import { candlestickTimeFormatter, getIsPie } from '../../utils/lib/indicator'
import { getChartType, getRangeConfig, getRangeShow, getStylesByIndicator, getStyleConfig } from './utils'
import { merge, cloneDeep as clone } from 'lodash-es'

//viewOptions
//timecut

export default function ({ data: baseData, config, theme = defaultTheme, viewOption }) {
  const chartData = clone(baseData)
  const { BACKGROUND_COLOR, GRID_COLOR, GRID_BORDER_WIDTH, GRID_BORDER_COLOR } = theme
  let { format, data, user_config, default_config } = chartData
  let samplingSize = 500
  if (!format) {
    const samplingConfig = {
      open: false,
      size: samplingSize,
    }
    //旧指标转化
    ;({ format, data, user_config, default_config } = indicatorMiddleware({
      indicator: chartData,
      viewOption,
      samplingConfig: merge(
        samplingConfig,
        default_config && default_config.sampling,
        user_config && user_config.sampling,
        config && config.sampling
      ),
    }))
  } else if (chartData.indicator_format === 'candlestick' && chartData.frequency) {
    // 蜡烛图时间格式化
    data = candlestickTimeFormatter(chartData)
    chartData.data = data
  } else if (chartData.styles) {
    // TODO 需要优化
    // 同步数据series类型
    if (
      chartData.styles.type &&
      Array.isArray(data.series) &&
      format === 'grid' &&
      chartData.styles.type !== 'candlestick'
    ) {
      data.series = data.series.map(v => {
        const newItem = merge({}, v, {
          type: getChartType(chartData.styles.type),
          connectNulls: true,
          lineStyle: {
            width: chartData.styles.type === 'scatter' ? 0 : 1,
          },
          stack: chartData.styles.type.startsWith('stack') ? 'stack' : null,
          // 面积图 强制不显示圆点
          symbol:
            ['area', 'stack_area'].includes(chartData.styles.type) && !chartData.styles.showSeriesLabel
              ? 'none'
              : 'circle',
          symbolSize: 4,
          showAllSymbol: chartData.styles.type === 'scatter' ? true : 'auto',
        })
        return newItem
      })
    }
  }

  // 清除默认主题
  if (((config && config.clearDefaultColor) || (user_config && user_config.clearDefaultColor)) && default_config) {
    default_config = clone(default_config)
    if (default_config && Array.isArray(default_config.series)) {
      default_config.series.forEach(item => {
        if (item && item.itemStyle && item.itemStyle.color) {
          delete item.itemStyle.color
        }
      })
    }
  }

  const finalStyles = getStylesByIndicator(chartData)

  // 转化styles
  const style_config = getStyleConfig(finalStyles, format)

  // 转化其他配置
  let other_config = {
    range: {
      ...getRangeConfig(data.axis, chartData.timeRange),
      show: getRangeShow(finalStyles),
    },
  }

  const getFinalConfig = formatConfig => {
    return merge(
      {
        chart: {
          backgroundColor: BACKGROUND_COLOR,
        },
        title: {
          show: true,
          textAlign: 'left',
        },
        legend: {
          show: true,
          allowColorPick: true,
          position: 'top',
        },
        range: {
          show: false,
          type: 'line',
        },
        slider: {
          show: false,
        },
        // 数据采样
        sampling: {
          open: false,
          size: samplingSize,
        },
        revert: false,
        // 用于表示middleware中是否已处理翻转
        reverted: false,
        labelFractionDigit: 2,
        yAxisPosition: default_config && default_config.horizontal ? 'right' : 'left',
      },
      other_config,
      formatConfig,
      default_config,
      user_config,
      style_config,
      config
    )
  }
  if (chartData.origin_frequency && Array.isArray(data.axis)) {
    data.axis.forEach(v => {
      if (v && typeof v.name === 'number') {
        v.name = timeFormatter(v.name, chartData.origin_frequency)
      }
    })
  }
  let options, finalConfig
  if (format === 'radar') {
    finalConfig = getFinalConfig()
    options = getRadarOptions(data, finalConfig, theme)
  }
  if (format === 'grid') {
    finalConfig = getFinalConfig({})
    // timecut
    if (finalConfig.slider.show) {
      data = finalConfig.slider.dataMap[finalConfig.slider.labels[finalConfig.slider.selectedIndex]]
      finalConfig.series = merge(
        [],
        finalConfig.series,
        data.series.map(() => {
          return {
            yAxisIndex: 0,
            label: {
              show: true,
            },
          }
        })
      )
    }
    if (getIsPie(finalConfig.chartType)) {
      const pieData = {
        series: data.series[0].value.map(({ name, value }) => {
          return {
            name,
            value,
          }
        }),
      }
      options = getPieOptions(pieData, finalConfig, theme)
    } else if (finalConfig.chartType === 'radar') {
      options = getRadarOptions(data, finalConfig, theme)
    } else {
      options = getGridOptions(data, finalConfig, theme)
    }
  }
  if (format === 'map') {
    finalConfig = getFinalConfig({
      unit: chartData.unit,
    })
    options = getMapOptions(data, finalConfig, theme)
  }
  if (format === 'pie') {
    if (viewOption === 'grid') {
      finalConfig = getFinalConfig({
        horizontal: true,
        showSeriesLabel: true,
        legend: {
          show: false,
        },
        yAxis: [
          {
            axisLabel: {
              rotate: -90,
            },
          },
        ],
        xAxis: {
          axisLabel: {
            interval: 0,
          },
        },
      })
      const gridData = {
        axis: data.series.map(({ name }) => {
          return {
            name,
          }
        }),
        series: [
          {
            type: 'bar',
            value: data.series.map(({ name, value }) => {
              return {
                name,
                value,
              }
            }),
          },
        ],
      }
      options = getGridOptions(gridData, finalConfig, theme)
    } else {
      finalConfig = getFinalConfig({})
      options = getPieOptions(data, finalConfig, theme)
    }
  }
  if (format === 'scatter_sequence') {
    finalConfig = getFinalConfig({})
    data = merge(
      {
        config: {
          name: chartData.show_name, // TODO 临时单条散点序列处理
          axis_name: chartData.axis_name,
          series_name: chartData.series_name,
        },
      },
      data
    )
    options = getScatterSequenceOptions(data, finalConfig, theme)
  }

  const defaultOptions = {
    animation: false,
    grid: {
      show: true,
      borderWidth: format !== 'map' && format !== 'radar' ? GRID_BORDER_WIDTH : 0,
      borderColor: GRID_BORDER_COLOR,
      backgroundColor: GRID_COLOR,
      containLabel: true,
      left: 5,
      right: 0,
      top: 0,
      bottom: 0,
    },
    backgroundColor: finalConfig.backgroundColor || BACKGROUND_COLOR,
  }

  return {
    format,
    options: merge(defaultOptions, options, finalConfig.grid && { grid: finalConfig.grid }),
    config: finalConfig,
    viewOption,
  }
}
