/* eslint-disable */
/**
 * Created by Xinhe on 2019/12/17.
 */
import getTooltipConfig from './tooltip'
import { merge } from 'lodash-es'
import { getColorGenerator, getSamplingIndexs } from './utils'

export default function (data, config, theme) {
  const {
    MAP_AREA_COLOR,
    MAP_AREA_BORDER_COLOR,
    MAP_AREA_EMPHASIS_BORDER_WIDTH,
    MAP_LABEL_TEXT_COLOR,
    MAP_LABEL_FONT_SIZE,
    MAP_LABEL_LINE_HEIGHT,
    MAP_LABEL_WEIGHT,
    MAP_LABEL_FONT_FAMILY,

    SCATTER_SERIES_COLOR,
  } = theme
  const mapType = data.map
  const seriesData = data.series
  const tooltip = getTooltipConfig(config, theme)
  const geo = {
    map: mapType,
    itemStyle: {
      borderColor: MAP_AREA_BORDER_COLOR,
      areaColor: MAP_AREA_COLOR,
    },
    emphasis: {
      itemStyle: {
        borderColor: MAP_AREA_BORDER_COLOR,
        areaColor: MAP_AREA_COLOR,
        borderWidth: MAP_AREA_EMPHASIS_BORDER_WIDTH,
      },
      label: {
        show: true,
        color: MAP_LABEL_TEXT_COLOR,
        lineHeight: MAP_LABEL_LINE_HEIGHT,
        fontSize: MAP_LABEL_FONT_SIZE,
        fontWeight: MAP_LABEL_WEIGHT,
        fontFamily: MAP_LABEL_FONT_FAMILY,
      },
    },
  }
  const scatterColorGen = getColorGenerator(
    config.theme && config.theme.chartColors ? config.theme.chartColors : SCATTER_SERIES_COLOR
  )
  let scatterIndex = 0

  const mapValueDict = {}
  seriesData.forEach(({ type, value }) => {
    if (type === 'map') {
      value.forEach(v => {
        const mapKey = v.name
        if (typeof v.value === 'number') {
          if (Object.prototype.hasOwnProperty.call(mapValueDict, mapKey)) {
            mapValueDict[mapKey] += v.value
          } else {
            mapValueDict[mapKey] = v.value
          }
        }
      })
    }
  })
  const mapValueList = Object.values(mapValueDict).sort((a, b) => a - b)

  let hasMap = false
  const visualMap = merge(
    seriesData.map(({ type, value }, index) => {
      let item = {
        calculable: true,
        seriesIndex: index,
        itemHeight: 50,
        itemWidth: 15,
        left: index * 40,
      }
      if (type === 'map') {
        item = {
          ...item,
          show: !hasMap,
          inRange: {
            color: ['#F9E4D3', '#CC4D48'],
          },
        }
        if (mapValueList.length) {
          item.min = mapValueList[0]
          item.max = mapValueList[mapValueList.length - 1]
        }
        hasMap = true
      }
      if (type === 'scatter') {
        item = {
          ...item,
          inRange: {
            symbolSize: [10, 50],
          },
        }
        const valueList = value.map(v => v.value).filter(v => typeof v === 'number')
        if (valueList.length) {
          valueList.sort((a, b) => a - b)
          item.min = valueList[0]
          item.max = valueList[valueList.length - 1]
        }
      }
      return item
    }),
    config.visualMap
  )

  const series = merge(
    seriesData.map(({ type, name, unit, value }) => {
      let item
      const unitFinal = unit || config.unit
      if (type === 'map') {
        item = {
          type,
          name,
          mapType,
          data: value.map(v =>
            Object.assign(
              {
                unit: unitFinal,
              },
              v
            )
          ),
          itemStyle: {
            borderColor: MAP_AREA_BORDER_COLOR,
            areaColor: MAP_AREA_COLOR,
          },
          emphasis: {
            itemStyle: {
              borderColor: MAP_AREA_BORDER_COLOR,
              areaColor: MAP_AREA_COLOR,
              borderWidth: MAP_AREA_EMPHASIS_BORDER_WIDTH,
            },
            label: {
              show: true,
              color: MAP_LABEL_TEXT_COLOR,
              lineHeight: MAP_LABEL_LINE_HEIGHT,
              fontSize: MAP_LABEL_FONT_SIZE,
              fontWeight: MAP_LABEL_WEIGHT,
              fontFamily: MAP_LABEL_FONT_FAMILY,
            },
          },
        }
      }
      if (type === 'scatter') {
        item = {
          type,
          name,
          coordinateSystem: 'geo',
          data: value.map(({ name, value, coord }) => {
            return {
              name,
              unit: unitFinal,
              value: [...coord, value],
            }
          }),
          itemStyle: {
            color: scatterColorGen(scatterIndex),
          },
        }
        scatterIndex++
      }
      return item
    }),
    config.series
  )

  visualMap.forEach((item, index) => {
    if (seriesData[index].type === 'scatter') {
      if (!item.inRange.color) {
        item.inRange.color = series[index].itemStyle.color
      }
    }
  })
  if (config.sampling && config.sampling.open) {
    // 数据采样
    series.forEach(item => {
      if (Array.isArray(item.data)) {
        const samplingIndexs = getSamplingIndexs(item.data.length, config.sampling.size)
        item.data = item.data.filter((v, index) => samplingIndexs.includes(index))
      }
    })
  }
  return {
    tooltip,
    visualMap,
    geo,
    series,
    mapType,
  }
}
