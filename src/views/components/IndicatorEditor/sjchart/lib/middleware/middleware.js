/* eslint-disable */
/**
 * Created by Xinhe on 2019/12/31.
 */
//backup
//将现有指标转成新的格式
/**
 * indicator_format:
 panel ---> timeline
 group ---> cut
 timecut
 scatter

 * default_style: 默认样式

 **/
import Moment from 'moment'
import { merge } from 'lodash-es'
import { getTimeList, timeFormatter, dateHacker } from '../../../utils/lib/time'
import { getDefaultIndicatorStyles, getDefaultParameterConfig } from '../../../utils/lib/indicator'
import { getSamplingIndexs, getRangeConfig, getChartType, getRangeShow } from '../utils'
import { getMergedFrequency } from '../../../utils/lib/frequency'

const getValueMap = (values, unit) => {
  const valueMap = {}
  values.forEach(({ date, value, prediction }) => {
    // hack 夏令时匹配
    valueMap[String(dateHacker(date))] = {
      value,
      prediction,
      unit,
    }
  })
  return valueMap
}

export default function ({ indicator, viewOption, samplingConfig }) {
  const { default_style, indicator_format, indicator_values, show_name, styles } = indicator
  let frequency = indicator.frequency
  const format = indicator_format
  const data = (indicator_values || []).filter(item => {
    const visible = item.config ? item.config.visible : item.visible
    return visible !== false
  })
  const defaultStyles = default_style
  const name = show_name
  const finalStyles = (() => {
    return merge(getDefaultIndicatorStyles(format, defaultStyles), styles || {})
  })()

  const horizontal = ['horizontal_bar', 'stack_horizontal_bar'].includes(finalStyles.type)
  const frequencyList = data.map(item => item.frequency)
  const mergeFrequency = getMergedFrequency(frequencyList)
  if (mergeFrequency) {
    indicator.frequency = mergeFrequency
    frequency = mergeFrequency
  }

  const shownValues = (() => {
    return data
      .map(item => ({
        ...item,
        config: merge(
          getDefaultParameterConfig(indicator_format, {
            showLabel: finalStyles.showSeriesLabel,
            labelFractionDigit: finalStyles.labelFractionDigit,
            style: finalStyles.type,
          }),
          item.config
        ),
      }))
      .sort((a, b) => a.sortIndex - b.sortIndex)
      .map(({ config, values, name, originUnit, unit, ...rest }) => {
        const finalUnit = config.unit || originUnit || unit || indicator.unit

        let symbol = config.markerType
        let symbolSize = config.markerWidth

        // hack: do not show label when symbol is 'none'
        if (config.showLabel && config.markerType === 'none') {
          symbol = 'circle'
          symbolSize = 0.001
        }

        // 获取每项图表类型
        const itemType = indicator_format === 'timeline' ? config.style : finalStyles.type

        const yAxisPosition = indicator_format === 'timeline' ? config.yAxisPosition : finalStyles.yAxisPosition

        const showLabel = indicator_format === 'timeline' ? config.showLabel : finalStyles.showSeriesLabel

        const newItem = {
          name: config.name || name || indicator.show_name,
          color: config.color,
          type: getChartType(itemType),
          options: {
            connectNulls: true,
            lineStyle: {
              type: config.lineType,
              width: itemType === 'scatter' ? 0 : config.lineWidth,
            },
            stack: itemType.startsWith('stack') ? 'stack' : null,
            // 面积图 强制不显示圆点
            symbol: ['area', 'stack_area'].includes(itemType) && !showLabel ? 'none' : symbol,
            z: config.z,
            symbolSize,
            showAllSymbol: itemType === 'scatter' ? true : 'auto',
            [horizontal ? 'xAxisIndex' : 'yAxisIndex']: yAxisPosition === 'right' ? 1 : 0,
            label: {
              show: showLabel,
            },
            ...config.otherOptions,
          },
          unit: finalUnit,
          labelFractionDigit: config.labelFractionDigit,
          values,
        }

        return {
          ...newItem,
          ...rest,
        }
      })
  })()

  const finalData = (() => {
    return shownValues.filter(({ values }) => values && values.length > 0)
  })()

  const getTimeData = () => {
    const minDate = (() => {
      if (!finalData || finalData.length === 0) return null
      return Math.min(...finalData.map(({ values }) => Math.min(...values.map(({ date }) => date))))
    })()

    const maxDate = (() => {
      if (!finalData || finalData.length === 0) return null
      return Math.max(...finalData.map(({ values }) => Math.max(...values.map(({ date }) => date))))
    })()

    const timeData = (() => {
      let data = []
      if (frequency) {
        data = getTimeList(minDate, maxDate, frequency)
      } else {
        finalData.forEach(({ values }) => {
          values.forEach(({ date }) => data.push(String(date)))
        })
        data = [...new Set(data)]
        data.sort((a, b) => parseFloat(a) - parseFloat(b))
      }
      const maxNum = data.length
      if (samplingConfig && samplingConfig.open && maxNum > samplingConfig.size) {
        // 数据采样
        const samplingIndexs = getSamplingIndexs(maxNum, samplingConfig.size)
        data = data.filter((v, index) => samplingIndexs.includes(index))
        if (indicator.timeRange) {
          indicator.timeRange = [
            Math.floor((indicator.timeRange[0] / maxNum) * data.length),
            indicator.timeRange[1] === null ? null : Math.floor((indicator.timeRange[1] / maxNum) * data.length),
          ]
        }
      }
      return data
    })()

    return timeData
  }

  const formatTime = (time, isTooltip = false) => {
    if (frequency) {
      return timeFormatter(Moment(Number(time)), frequency, isTooltip)
    }
    return time
  }

  const getCutData = originData => {
    const cutFinalData = (() => {
      if (!originData) return null
      let data = [...originData]

      data = data.reverse()

      return data.map(item => {
        return {
          ...item,
          values: item.values.map(value => {
            return {
              ...value,
              tag: value.tag || indicator.header_name || name,
            }
          }),
        }
      })
    })()

    const stacks = (() => {
      let stacks = []
      if (cutFinalData && cutFinalData[0]) {
        if (finalStyles.revert) {
          stacks = cutFinalData.map(({ name }) => {
            return name
          })
        } else {
          stacks = cutFinalData[0].values.map(({ tag }) => {
            return tag
          })
        }
      }
      return stacks
    })()

    const chartData = (() => {
      const getDataOption = (value, unit, name) => {
        return {
          value: value.value,
          unit,
          name,
        }
      }
      return stacks.map((tag, index) => {
        return {
          type: getChartType(finalStyles.type),
          connectNulls: true,
          lineStyle: {
            width: finalStyles.type === 'scatter' ? 0 : 1,
          },
          stack: finalStyles.type.startsWith('stack') ? 'stack' : null,
          // 面积图 强制不显示圆点
          symbol: ['area', 'stack_area'].includes(finalStyles.type) && !finalStyles.showSeriesLabel ? 'none' : 'circle',
          label: tag,
          data: finalStyles.revert
            ? cutFinalData[index].values.map(value => {
                return getDataOption(value, cutFinalData[index].unit)
              })
            : cutFinalData.map(({ values, unit, name }) => {
                const value = values.find(item => item.tag === tag)
                return getDataOption(value, unit, name)
              }),
        }
      })
    })()

    const xAxisData = (() => {
      if (!cutFinalData) return null
      return {
        data: finalStyles.revert
          ? cutFinalData[0] && cutFinalData[0].values.map(({ tag }) => tag || name)
          : cutFinalData.map(({ name }) => name),
        options: {
          axisLabel: {
            interval: 0,
          },
        },
      }
    })()

    const series = chartData.map(({ label, type, data, options, ...rest }) => {
      const seriesOption = {
        name: label,
        type,
        value: data,
        ...rest,
      }
      return merge(seriesOption, options)
    })
    const axis = xAxisData.data.map(data => {
      return { name: data }
    })

    return {
      series,
      axis,
    }
  }

  // 获取时间轴数据
  const getSliderData = timeData => {
    const sliderMap = {}
    const sliderLabels = []
    timeData.forEach(timeItem => {
      const newData = []
      finalData.forEach(dataItem => {
        const newItem = {
          value: null,
          data: Object.assign({}, dataItem, {
            values: [],
          }),
        }

        // 可能出现字符串和数字类型比较的情况
        const dateStrs = dataItem.values.map(valItem => valItem.date + '')
        const timeIndex = dateStrs.indexOf(timeItem + '')
        if (timeIndex !== -1) {
          newItem.value = dataItem.values[timeIndex].value
          newItem.data.values.push(Object.assign({}, dataItem.values[timeIndex]))
          newData.push(newItem)
        }
      })
      if (newData.length) {
        const timeLabel = formatTime(timeItem)
        newData.sort((a, b) => b.value - a.value)
        sliderMap[timeLabel] = getCutData(newData.map(newDataItem => newDataItem.data))
        sliderLabels.push(timeLabel)
      }
    })
    let selectedTimeIndex = sliderLabels.length - 1
    if (
      Object.prototype.hasOwnProperty.call(indicator, 'selectedTimeIndex') &&
      typeof indicator.selectedTimeIndex === 'number' &&
      indicator.selectedTimeIndex <= selectedTimeIndex
    ) {
      selectedTimeIndex = indicator.selectedTimeIndex
    }
    return {
      labels: sliderLabels,
      range: [0, sliderLabels.length - 1],
      dataMap: sliderMap,
      selectedIndex: selectedTimeIndex,
    }
  }

  const getUserConfig = indicatorStyles => {
    const userConfig = {
      title: {
        show: indicatorStyles.showTitle,
        textAlign: indicatorStyles.titleAlign,
      },
      legend: {
        show: indicatorStyles.showLegend,
        position: indicatorStyles.legendPosition,
      },
    }
    if (indicatorStyles.frameBackgroundColor && indicatorStyles.chartBackgroundColor) {
      Object.assign(userConfig, {
        chart: {
          backgroundColor: indicatorStyles.frameBackgroundColor,
        },
        grid: {
          backgroundColor: indicatorStyles.chartBackgroundColor,
        },
        backgroundColor: indicatorStyles.frameBackgroundColor,
      })
    }
    return merge({}, indicator.user_config, userConfig)
  }

  if (['timeline', 'panel', 'timecut'].includes(format) && !finalStyles.showTimeline) {
    const timeData = getTimeData()

    const xAxisData = (() => {
      if (!finalData || finalData.length === 0 || !frequency) return null
      let data
      if (finalStyles.revert) {
        data = finalData.map(({ name }) => name)
      } else {
        data = timeData
      }
      // console.log(data)
      return {
        data,
      }
    })()

    const yAxisData = (() => {
      if (!xAxisData) return []
      return [finalStyles.yAxis.left, finalStyles.yAxis.right].map(({ namePos, ...rest }) => {
        return {
          nameLocation: namePos,
          // name: showName? name : undefined,
          splitLine: {
            show: finalStyles.showSplitY,
            lineStyle: {
              color: finalStyles.splitYColor,
            },
          },
          ...rest,
        }
      })
    })()

    const chartData = (() => {
      if (!finalData || finalData.length === 0 || !xAxisData || !timeData) return null
      const xData = xAxisData.data
      let res
      if (finalStyles.revert) {
        const finalDataMap = finalData.reduce((prev, { name, values, unit }) => {
          const valueMap = getValueMap(values, unit)
          return {
            ...prev,
            [name]: valueMap,
          }
        }, {})
        res = timeData.map(time => {
          return {
            type: getChartType(finalStyles.type),
            label: formatTime(time),
            data: xData.map(category => {
              const valueMap = finalDataMap[category]
              const { value, prediction, unit } = valueMap[String(dateHacker(time))] || {}
              return {
                value: value,
                prediction,
                unit,
              }
            }),
          }
        })
      } else {
        res = finalData.map(({ values, name, color, unit, labelFractionDigit, type, options = {} }) => {
          const valueMap = getValueMap(values, unit)
          let data
          data = xData
            .map(date => {
              const { value, prediction, unit } = valueMap[String(Math.round(date / 86400000))] || {}
              return {
                date,
                unit,
                value,
                prediction,
              }
            })
            .map(({ value, prediction, unit }) => {
              return {
                value: value,
                unit,
                prediction,
              }
            })

          return {
            label: name,
            type,
            color,
            data,
            unit,
            labelFractionDigit,
            options: {
              ...options,
            },
          }
        })
      }
      return res
    })()

    const xAxisDataFormatter = (value, index = null, isTooltip) => {
      if (finalStyles.revert) {
        return value
      } else {
        let time = formatTime(value, isTooltip)
        if (index !== null) {
          // 已第一根线为准
          if (chartData && chartData.length > 0 && chartData[0].data[index] && chartData[0].data[index].prediction) {
            time += 'E'
          }
        }
        return time
      }
    }

    // indicator_values.
    const series =
      chartData &&
      chartData.map(({ label, type, data, options, color, ...rest }) => {
        const baseOption = {
          name: label,
          type,
          value: data,
        }
        if (color) {
          Object.assign(baseOption, {
            itemStyle: {
              color,
            },
          })
        }
        const seriesOption = {
          ...baseOption,
          ...rest,
        }
        return merge(seriesOption, options)
      })

    const axis =
      xAxisData &&
      xAxisData.data &&
      xAxisData.data.map((data, index) => {
        return {
          name: xAxisDataFormatter(data, index),
          tooltipLabel: xAxisDataFormatter(data, index, true),
        }
      })
    return {
      format: 'grid',
      default_config: {
        reverted: finalStyles.revert,
        yAxis: yAxisData,
        xAxis: {
          splitArea: {
            show: finalStyles.showSplitArea,
            areaStyle: {
              color: [finalStyles.splitAreaColor, 'transparent'],
            },
          },
        },
        legend: {
          showValue: true,
        },
        range: {
          ...getRangeConfig(axis, indicator.timeRange),
          show: getRangeShow(finalStyles),
        },
        slider: {
          show: false,
        },
      },
      user_config: getUserConfig(finalStyles),
      data: {
        series: series || [],
        axis: axis || [],
      },
    }
  }

  if (
    ['cut', 'group', 'timecut'].includes(format) ||
    (['timeline', 'panel', 'timecut'].includes(format) && finalStyles.showTimeline)
  ) {
    const resultData = {
      format: 'grid',
      default_config: {
        reverted: finalStyles.revert,
        // showSeriesLabel: true,
        // yAxis: [
        //   {
        //     axisLabel: {
        //       rotate: -90
        //     }
        //   }
        // ],
        // xAxis: {
        //   axisLabel: {
        //     interval: 0
        //   }
        // }
      },
      user_config: getUserConfig(finalStyles),
    }

    if (['cut', 'group'].includes(format)) {
      const cutData = getCutData(finalData)
      Object.assign(resultData, {
        data: cutData,
      })
      Object.assign(resultData.default_config, {
        range: {
          ...getRangeConfig(cutData.axis, indicator.timeRange),
          show: getRangeShow(finalStyles),
        },
      })
    } else {
      // 显示时间轴
      const timeData = getTimeData()
      const sliderData = getSliderData(timeData)
      const cutData = sliderData.dataMap[sliderData.labels[sliderData.selectedIndex]]
      Object.assign(resultData, {
        data: cutData,
      })
      Object.assign(resultData.default_config, {
        range: {
          ...getRangeConfig(cutData.axis, indicator.timeRange),
          show: getRangeShow(finalStyles),
        },
        slider: {
          show: true,
          ...sliderData,
        },
      })
    }
    return resultData
  }
}
