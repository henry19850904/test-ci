const articleLabelKeys = [
  {
    title: '标题',
    dataIndex: 'titlelink',
    key: 'titlelink',
    ellipsis: true,
    width: 240,
    slots: { customRender: 'titlelink' },
    scopedSlots: { customRender: 'titlelink' },
  },
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    key: 'createdTime',
    width: 120,
  },
  {
    title: '状态',
    dataIndex: 'status',
    key: 'status',
    width: 60,
  },
  {
    title: '操作',
    dataIndex: 'action',
    key: 'action',
    width: 60,
    slots: { customRender: 'action' },
    scopedSlots: { customRender: 'action' },
  },
]

const approvalLabelKeys = [
  {
    title: '标题',
    dataIndex: 'title',
    key: 'title',
    ellipsis: true,
    width: 500,
  },
  {
    title: '项目名称',
    dataIndex: 'projectName',
    key: 'projectName',
    width: 180,
  },
  {
    title: '作者',
    dataIndex: 'authorName',
    key: 'authorName',
    width: 180,
  },
  {
    title: '提交时间',
    dataIndex: 'submitTime',
    key: 'submitTime',
    width: 180,
  },
  {
    title: '操作',
    dataIndex: 'action',
    key: 'action',
    width: 80,
    slots: { customRender: 'action' },
    scopedSlots: { customRender: 'action' },
  },
]

export default {
  articleLabelKeys,
  approvalLabelKeys,
}
