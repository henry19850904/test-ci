// 发布位置字典项
const publishPlaceDics = [
  {
    value: 'place1',
    name: '军工',
  },
  {
    value: 'place2',
    name: '保险',
  },
]
// 发布频率字典项
const publishRateDics = [
  {
    value: 'day',
    name: '每日',
  },
  {
    value: 'week',
    name: '每周',
  },
  {
    value: 'mounth',
    name: '每月',
  },
]
// 发布频率周字典项
const publishDayDics = [
  {
    value: '1',
    name: '周一',
  },
  {
    value: '2',
    name: '周二',
  },
  {
    value: '3',
    name: '周三',
  },
  {
    value: '4',
    name: '周四',
  },
  {
    value: '5',
    name: '周五',
  },
  {
    value: '6',
    name: '周六',
  },
  {
    value: '7',
    name: '周日',
  },
]
// 数据对象
const dataObjectDics = [
  {
    value: 'data1',
    name: '分析师',
  },
  {
    value: 'data2',
    name: '机构',
  },
]
// 审批人列表
const approverDics = [
  {
    value: 'userId1',
    name: '审批人1',
  },
  {
    value: 'userId2',
    name: '审批人2',
  },
]

export default {
  publishPlaceDics,
  publishRateDics,
  publishDayDics,
  dataObjectDics,
  approverDics,
}
