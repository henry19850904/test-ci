import { BlankLayout } from '@/layouts'
import { Menu } from '@/models/menu'
import { RouteRecordRaw } from 'vue-router'

// // 前端路由表
// const constantRouterComponents = {
//   // 基础页面 layout 必须引入
//   BlankLayout,
//   PageView,
// }

// 前端未找到页面路由（固定不用改）
const notFoundRouter = {
  path: '*',
  redirect: '/404',
  hidden: true,
}

// // 根级菜单
// const rootRouter = {
//   key: '',
//   name: 'index',
//   path: '',
//   component: 'BlankLayout',
//   redirect: '/home',
//   meta: {
//     title: '首页',
//   },
//   children: [],
// }

/**
 * 动态生成菜单
 * @param token
 * @returns {Promise<Router>}
 */
export const generatorDynamicRouter = (menus: Menu[]): RouteRecordRaw[] => {
  const routers = generator(menus)

  routers.push(notFoundRouter)

  return routers
}

/**
 * 动态生成菜单生成 vue-router 层级路由表
 *
 * @param routerMap
 * @param parent
 * @returns {*}
 */
export const generator = (menus: Menu[], parent?: RouteRecordRaw): RouteRecordRaw[] => {
  const routerMap = menus.filter(m => !m.parentId)

  return routerMap.map(item => {
    const currentRouter: RouteRecordRaw = {
      // 如果路由设置了 path，则作为默认 path，否则 路由地址 动态拼接生成
      path: (item.path as string) || `${(parent && parent.path) || ''}/${item.key}`,
      // 路由名称，建议唯一
      name: item.name || item.key || '',

      component: BlankLayout,
      // // 该路由对应页面的 组件 :方案2 (动态加载)
      // component: constantRouterComponents[item.component || item.key] || (() => import(`@/views/${item.component}`)),

      // meta: 页面标题, 菜单图标, 页面权限(供指令权限用，可去掉)
      meta: {
        title: item.title,
        icon: item.icon,
        // hiddenHeaderContent: hiddenHeaderContent,
        // target: target,
        permission: item.name,
      },
    }

    // 为了防止出现后端返回结果不规范，处理有可能出现拼接出两个 反斜杠
    if (!currentRouter.path.startsWith('http')) {
      currentRouter.path = currentRouter.path.replace('//', '/')
    }
    // 重定向
    item.redirect && (currentRouter.redirect = item.redirect)

    const subMenus = menus.filter(m => m.parentId === item.Id)

    // 是否有子菜单，并递归处理
    if (subMenus.length > 0) {
      // Recursion
      currentRouter.children = generator(subMenus, currentRouter)
    }
    return currentRouter
  })
}
