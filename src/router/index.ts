import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import { constantRouterMap, asyncRouterMap } from '@/config/routerConfig'

// const routes = constantRouterMap as Array<RouteRecordRaw>;

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: [...constantRouterMap, ...asyncRouterMap] as Array<RouteRecordRaw>,
})

export default router
