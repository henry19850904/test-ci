import { InjectionKey } from 'vue'
import { createStore, useStore as baseUseStore, Store, createLogger } from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import AllStates, { RootState } from './rootState'
import user from './modules/user'
import home from './modules/home'
import messages from './modules/messages'

const _DEV_ = process.env.NODE_ENV === 'development'

import permission from './modules/async-router'

export const key: InjectionKey<Store<RootState>> = Symbol('vuex-store')

const plugins = _DEV_ ? [createLogger()] : [createPersistedState({ storage: window.sessionStorage })]

export default createStore<RootState>({
  state: {
    sideCollapsed: false,
    theme: 'dark',
    isMobile: false,
  },
  modules: {
    user,
    home,
    messages,
    permission,
  },
  mutations: {},
  actions: {},
  getters: {},
  plugins,
})

// define your own `useStore` composition function
export function useStore<T = AllStates>(): Store<T> {
  return baseUseStore<T>(key)
}
