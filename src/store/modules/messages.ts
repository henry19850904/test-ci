import { Module } from 'vuex'
import { RootState } from '@/store/rootState'
import { messageApi } from '@/api'
import Message from '@/models/message'
import ApiResult from '@/models/apiResult'
// import { notification } from 'ant-design-vue'

export const enum MessagesTypes {
  GET_MESSAGES = 'GET_MESSAGES',
  LISTEN_MESSAGES = 'LISTEN_MESSAGES',
  GET_UNREAD_COUNT = 'GET_UNREAD_COUNT',
  SET_UNREAD_COUNT = 'SET_UNREAD_COUNT',
  UPDATE_READ_STATUS = 'UPDATE_READ_STATUS',
  ADD_MESSAGES = 'ADD_MESSAGES',
}

export interface MessagesState {
  messages: Message[]
  total: number
  unreadCount: number
}

const messageModule: Module<MessagesState, RootState> = {
  namespaced: true,
  state: {
    messages: [],
    unreadCount: 0,
    total: 0,
  },
  mutations: {
    [MessagesTypes.GET_MESSAGES](state, { messages, total }) {
      state.messages = messages
      state.total = total
    },
    [MessagesTypes.SET_UNREAD_COUNT](state, count) {
      state.unreadCount = count
    },
    [MessagesTypes.UPDATE_READ_STATUS](state, messageId) {
      const message = state.messages.find(msg => msg.messageId === messageId)
      if (message) {
        message.isRead = 1
      }
    },
    [MessagesTypes.ADD_MESSAGES](state, message) {
      if (Array.isArray(message)) {
        state.messages.push(...message)
        console.log('state.messages.push', state.messages)
        state.total += message.length
      } else {
        state.messages.unshift(message)
        state.total++
      }
    },
  },
  actions: {
    [MessagesTypes.LISTEN_MESSAGES]() {
      // const socket = messageApi.listenMessages()
      // socket.addEventListener('open', () => {
      //   console.log('socket opened')
      // })
      // socket.addEventListener('message', e => {
      //   console.log('socket message', e.data)
      // })
      // setInterval(() => {
      //   commit(MessagesTypes.ADD_MESSAGES, {
      //     id: 1,
      //     isRead: 0,
      //     messageType: 2,
      //     noticeCode: 200,
      //     noticeType: 2,
      //     sendContent:
      //       Date.now() +
      //       '较又内线会话候系红线北支。六你复四员干合文平装公角性单片织。感标次声被机第正特用山农族由确实听新。手么极算话影当转流队见统。那火备除资约江化过与联率万响造面拉。件千片用图属情调称眼里什。机目适许积证适以类政到支直影入般价。',
      //     sendTime: '1980-07-04 09:30:26',
      //   })
      //   notification.open({
      //     message: '您有一条新的通知',
      //     duration: 10,
      //     description:
      //       Date.now() +
      //       '较又内线会话候系红线北支。六你复四员干合文平装公角性单片织。感标次声被机第正特用山农族由确实听新。手么极算话影当转流队见统。那火备除资约江化过与',
      //   })
      // }, 10000)
    },
    async [MessagesTypes.GET_MESSAGES]({ commit }) {
      const res = await messageApi.getPagingList()
      if (res.code === 200) {
        commit(MessagesTypes.GET_MESSAGES, { messages: res.data.list, total: res.data.total })
      }
    },
    async [MessagesTypes.GET_UNREAD_COUNT]({ commit }) {
      const res = (await messageApi.getUnreadMessagesCount()) as ApiResult
      if (res.code === 200) {
        commit(MessagesTypes.SET_UNREAD_COUNT, res.data || 0)
      }
    },
    // async [MessagesTypes.UPDATE_READ_STATUS]() {

    // },
  },
}

export default messageModule
