/* eslint-disable */
/**
 * 向后端请求用户的菜单，动态生成路由
 */
import { Module } from 'vuex'
import { RootState } from '../rootState'
import { constantRouterMap } from '@/config/routerConfig'
import { generatorDynamicRouter } from '@/router/generator-routers'

const permission: Module<{ routers: typeof constantRouterMap; addRouters: typeof constantRouterMap }, RootState> = {
  state: {
    routers: constantRouterMap,
    addRouters: [],
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRouterMap.concat(routers)
    },
  },
  actions: {
    async GenerateRoutes({ commit }, data) {
      const { token } = data
      const routers = await generatorDynamicRouter(token)

      commit('SET_ROUTERS', routers)
    },
  },
}

export default permission
