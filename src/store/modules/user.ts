/* eslint-disable */
import { Module } from 'vuex'
import { RootState, UserState } from '../rootState'
import storage from 'store'
import { login, smsLogin, logout, getCurrentUser, saveUserTheme } from '@/api/login'
import { ACCESS_TOKEN } from '@/store/mutationTypes'
import User from '@/models/user'
import { SUCCESS_CODE } from '@/config/http'
import { notification } from 'ant-design-vue'

const user: Module<UserState, RootState> = {
  // namespaced: true,
  state: {} as UserState,
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
      state.roles = undefined
    },
    SET_CURRENT_USER: (state, data: { user: User; urls: string[] }) => {
      state.user = data.user
      state.roles = data.urls
    },
    SET_THEME: (state, payload) => {
      state.user.theme = payload
    },
  },

  actions: {
    // 邮箱登录
    async login({ commit }, userInfo) {
      const { data: res } = await login(userInfo)

      if (res.code === SUCCESS_CODE) {
        storage.set(ACCESS_TOKEN, res.data.token)
        commit('SET_TOKEN', res.data.token)
      } else {
        return Promise.reject(res)
      }
    },

    // 手机验证码登录
    async smsLogin({ commit }, userInfo) {
      const { data: res } = await smsLogin(userInfo)

      if (res.code === SUCCESS_CODE) {
        storage.set(ACCESS_TOKEN, res.data.token)
        commit('SET_TOKEN', res.data.token)
      } else {
        return Promise.reject(res)
      }
    },

    // 获取用户信息
    async getUserInfo({ commit }) {
      const { data: res } = await getCurrentUser()
      console.log(res)
      if (res.code === 200) {
        commit('SET_CURRENT_USER', res.data)
      } else {
        notification.error({
          message: 'Exception',
          description: res.msg,
        })
      }
    },

    // 获取用户信息
    async setUserTheme({ commit }, theme) {
      const { data: res } = await saveUserTheme({ theme })

      if (res.code === 200) {
        commit('SET_THEME', theme)
      } else {
        notification.error({
          message: 'Exception',
          description: res.msg,
        })
      }
    },

    // 登出
    async logout({ commit, state }) {
      const { data: res } = await logout()
      if (res.code === SUCCESS_CODE) {
        commit('SET_TOKEN', '')
        storage.remove(ACCESS_TOKEN)
      } else {
        notification.error({
          message: 'Exception',
          description: res.msg,
        })
      }
    },
  },
}

export default user
