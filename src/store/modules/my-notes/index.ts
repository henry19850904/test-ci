import { Module } from 'vuex'
import { RootState } from '@/store/rootState'
// import essaysApi from '@/api/essays'
import Essays from '@/models/essays'

export const enum MyNotesTypes {
  GET_ESSAYS = 'GET_ESSAYS',
}

interface MyNotesState {
  essays: Essays[]
}

const homeModule: Module<MyNotesState, RootState> = {
  namespaced: true,
  state: {
    essays: [],
  },
  mutations: {
    [MyNotesTypes.GET_ESSAYS](state, { essays }) {
      state.essays = essays
    },
  },
  // actions: {
  //   async [MyNotesTypes.GET_ESSAYS]({commit}) {

  //   }
  // },
}

export default homeModule
