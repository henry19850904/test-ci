import { Module } from 'vuex'
import { RootState } from '@/store/rootState'
import HomeConfigApi from '@/api/home'
import { SUCCESS_CODE } from '@/config/http'
import {
  SubscribeNews,
  WritingStatistics,
  WritingTasks,
  MyNotes,
  // HotWords,
  MessageCenter,
} from '@/views/home/components'
import { cloneDeep } from 'lodash-es'
export const GET_HOME_CONFIG = 'GET_HOME_CONFIG'
export const SET_HOME_CONFIG = 'SET_HOME_CONFIG'
export const RESET_HOME_CONFIG = 'RESET_HOME_CONFIG'
export const CANCEL_HOME_CONFIG = 'CANCEL_HOME_CONFIG'
export const COMMIT_HOME_CONFIG = 'COMMIT_HOME_CONFIG'
export const CHANGE_HOME_CONFIG = 'CHANGE_HOME_CONFIG'
export const TOGGLE_CONFIG_ITEM = 'TOGGLE_CONFIG_ITEM'

export type HomeConfig = Array<Config>

export interface Config {
  id: number
  span: number
  removed?: boolean
  children: ChildConfig[]
}

export interface ChildConfig {
  title: string
  removed?: boolean
}

export interface changeConfig {
  addedIndex: number
  removedIndex: number
  id?: number
}

export interface HomeState {
  homeConfig: Array<Config>
  homeConfigFinal: Array<Config>
  defaultConfig: Array<Config>
}

export type toggleParams = {
  colIndex: number
  rowIndex: number
  isRemove: boolean
}

const defaultConfig = [
  {
    id: 1,
    span: 3,
    children: [
      {
        title: '订阅资讯',
        component: SubscribeNews.name,
        height: 'calc(100vh - 70px)',
        icon: 'iconlogmanagement',
        border: true,
      },
    ],
  },
  {
    id: 2,
    span: 8,
    children: [
      {
        title: '写作统计',
        component: WritingStatistics.name,
        height: '115px',
        icon: 'iconhandle',
        border: false,
      },
      {
        title: '写作任务',
        component: WritingTasks.name,
        height: '253px',
        icon: 'iconfunction',
        border: false,
      },
      {
        title: '我的笔记',
        component: MyNotes.name,
        height: 'calc(100vh - 458px)',
        icon: 'iconmynote',
        border: true,
      },
    ],
  },
  {
    id: 3,
    span: 3,
    children: [
      // {
      //   title: '资讯热词',
      //   component: HotWords.name,
      //   height: '278px',
      //   icon: 'iconfunction',
      //   border: true,
      // },
      {
        title: '消息中心',
        component: MessageCenter.name,
        // height: 'calc(100vh - 358px + 278px + 10px)',
        height: 'calc(100vh - 70px)',
        icon: 'iconsave',
        border: true,
      },
    ],
  },
]

const swap = (arr: Array<ChildConfig | Config>, addedIndex: number, removedIndex: number) => {
  const el = arr[removedIndex]
  arr.splice(removedIndex, 1)
  arr.splice(addedIndex, 0, el)
}

const homeModule: Module<HomeState, RootState> = {
  namespaced: true,
  state: {
    defaultConfig,
    homeConfig: cloneDeep(defaultConfig),
    homeConfigFinal: cloneDeep(defaultConfig),
  },
  mutations: {
    [SET_HOME_CONFIG](state, payload) {
      state.homeConfig = payload
    },
    [COMMIT_HOME_CONFIG](state) {
      if (state.homeConfig.some(conf => conf.children.some(child => child.removed))) return
      state.homeConfigFinal = cloneDeep(state.homeConfig)
    },
    [RESET_HOME_CONFIG](state) {
      state.homeConfig = cloneDeep(defaultConfig)
    },
    [CANCEL_HOME_CONFIG](state) {
      state.homeConfig = cloneDeep(state.homeConfigFinal)
    },
    // 删除/恢复列表项
    [TOGGLE_CONFIG_ITEM](state, { colIndex, rowIndex, isRemove }: toggleParams) {
      const col = state.homeConfig[colIndex]
      const children = col.children
      const isColEmptyBefore = children.every(child => child.removed)
      children[rowIndex].removed = isRemove
      const isColEmptyAfter = children.every(child => child.removed)
      // 添加
      if (!isRemove) {
        if (isColEmptyBefore) {
          // state.homeConfig.splice(colIndex, 1)
          const index = state.homeConfig.slice().findIndex(config => config.children.every(child => child.removed))

          if (index >= 0) {
            swap(state.homeConfig, index, colIndex)
          }
        }
        const child = children[rowIndex]
        children.splice(rowIndex, 1)
        children.push(child)
      } else if (isColEmptyAfter) {
        // 删除整列
        state.homeConfig.splice(colIndex, 1)
        state.homeConfig.push(col)
      }
    },
  },
  actions: {
    // 获取首页配置
    async [GET_HOME_CONFIG]({ commit }) {
      const res = await HomeConfigApi.get()
      if (res.code === SUCCESS_CODE) {
        console.log(JSON.parse(res.data.assembly))
        commit(SET_HOME_CONFIG, JSON.parse(res.data.assembly))
        commit(COMMIT_HOME_CONFIG)
      }
    },
    // 提交首页配置
    async [COMMIT_HOME_CONFIG]({ commit, state }) {
      const res = await HomeConfigApi.add({
        assembly: JSON.stringify(state.defaultConfig),
      })
      if (res.code === SUCCESS_CODE) {
        commit(COMMIT_HOME_CONFIG)
      }
    },
    // 修改顺序
    [CHANGE_HOME_CONFIG]({ state, commit }, { addedIndex, removedIndex, id }: changeConfig) {
      const config = state.homeConfig
      if (id) {
        const conf = config?.find((i: Config) => i.id === id)
        if (!conf) return
        swap(conf.children, addedIndex, removedIndex)
      } else {
        swap(config as Array<Config>, addedIndex, removedIndex)
      }
      commit(SET_HOME_CONFIG, config)
    },
  },
}

export default homeModule
