import { mapState } from 'vuex'
import { RootState } from './rootState'

const deviceMixin = {
  computed: {
    ...mapState<RootState>({
      isMobile: (state: { isMobile: boolean }) => state.isMobile,
    }),
  },
}

export { deviceMixin }
