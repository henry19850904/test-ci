import User from '../models/user'
import { HomeState } from './modules/home'
import { MessagesState } from './modules/messages'
/**
 * 状态树根节点
 */
export interface RootState {
  sideCollapsed: boolean
  isMobile: boolean
  theme: string
}

export interface UserState {
  token: string
  user: User
  roles?: string[]
}

export default interface AllStates {
  user: UserState
  home: HomeState
  messages: MessagesState
}
