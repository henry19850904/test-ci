/* eslint-disable */
import { App } from 'vue'

const on = (function () {
  if (document.addEventListener !== undefined) {
    return function (element: Element, event: any, handler: any) {
      if (element && event && handler) {
        element.addEventListener(event, handler, false)
      }
    }
  } else {
    return function (element: any, event: any, handler: any) {
      if (element && event && handler) {
        element.attachEvent('on' + event, handler)
      }
    }
  }
})()

const throttleFunc = (function () {
  let result: number | null = null
  let isFirst = true
  return function (time: number, func: any) {
    if (!result) {
      result = window.setTimeout(
        function () {
          func()
          result = null
          isFirst = false
        },
        isFirst ? 0 : time
      )
    }
  }
})()

export default {
  install: (app: App<Element>): void => {
    app.directive('infinite-scroll', {
      mounted(el: Element, binding) {
        const customBottomDistance = binding.arg || 50
        on(el, 'scroll', function () {
          const bottomDistance = el.scrollHeight - el.clientHeight - el.scrollTop
          if (typeof binding.value === 'function' && bottomDistance <= customBottomDistance) {
            throttleFunc(1000, binding.value) // 降低触发频率
          }
        })
      },
      beforeUnmount(el: Element, binding) {
        el.removeEventListener('scroll', binding.value. false) // 移除绑定
      },
    })
  },
}
