/* eslint-disable */
export default function loading(field: string) {
  return function decorator(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    console.log(target, propertyKey, descriptor, field)
    const fn = descriptor.value
    descriptor.value = async function (...args: any[]) {
      target[field] = true
      try {
        return await fn.apply(this, args)
      } finally {
        target[field] = false
      }
    }
    return descriptor
  }
}
