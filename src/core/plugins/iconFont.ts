import { App } from 'vue'
import { createFromIconfontCN } from '@ant-design/icons-vue'

const IconFont = createFromIconfontCN({
  extraCommonProps: {
    // 给所有的 svg 图标 <Icon /> 组件设置额外的属性
    style: 'font-size: 20px; user-select:none;',
  },
  scriptUrl: ['//at.alicdn.com/t/font_2515580_251szfsww7c.js'], // 在 iconfont.cn 上生成
})

export default {
  install: (app: App<Element>): void => {
    app.component('IconFont', IconFont)
  },
}
