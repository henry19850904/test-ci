import { App } from 'vue'
import DataEmpty from '@/components/DataEmpty.vue'
import { MessageApi } from 'ant-design-vue/lib/message/index'

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $message: MessageApi
    // eslint-disable-next-line
    $notification: any
  }
}

// base library
import {
  Alert,
  Anchor,
  AutoComplete,
  Avatar,
  Badge,
  Breadcrumb,
  Button,
  Card,
  Checkbox,
  Col,
  Collapse,
  ConfigProvider,
  DatePicker,
  Descriptions,
  Divider,
  Drawer,
  Dropdown,
  Form,
  Icon,
  Image,
  Input,
  InputNumber,
  Layout,
  List,
  Menu,
  message,
  Modal,
  notification,
  PageHeader,
  Popconfirm,
  Popover,
  Progress,
  Radio,
  Result,
  Row,
  Select,
  Skeleton,
  Space,
  Spin,
  Statistic,
  Steps,
  Switch,
  Table,
  Tabs,
  Tag,
  TimePicker,
  Tooltip,
  Typography,
  Tree,
  Upload,
  Empty,
} from 'ant-design-vue'
// import Viser from 'viser-vue'

// // ext library
// import VueClipboard from "vue-clipboard2";
// import VueCropper from 'vue-cropper'
// import Dialog from '@/components/Dialog'
// import MultiTab from '@/components/MultiTab'
// import PageLoading from '@/components/PageLoading'
// import PermissionHelper from '@/core/permission/permission'
// import './directives/action'

// Vue.use(Viser)
// Vue.use(Dialog) // this.$dialog func
// Vue.use(MultiTab)
// Vue.use(PageLoading)
// Vue.use(PermissionHelper)
// Vue.use(VueCropper)

// VueClipboard.config.autoSetContainer = true;

export default {
  install: (app: App<Element>): void => {
    const components = [
      Alert,
      Anchor,
      AutoComplete,
      Avatar,
      Badge,
      Breadcrumb,
      Button,
      Card,
      Checkbox,
      Col,
      Collapse,
      ConfigProvider,
      DatePicker,
      Descriptions,
      Divider,
      Drawer,
      Dropdown,
      Form,
      Icon,
      Image,
      Input,
      InputNumber,
      Layout,
      List,
      Menu,
      Modal,
      PageHeader,
      Popconfirm,
      Popover,
      Progress,
      Radio,
      Result,
      Row,
      Select,
      Skeleton,
      Space,
      Spin,
      Statistic,
      Steps,
      Switch,
      Table,
      Tabs,
      Tag,
      TimePicker,
      Tooltip,
      Typography,
      Tree,
      Upload,
      Empty,
    ]
    app.component('data-empty', DataEmpty)
    app.config.globalProperties.$message = message
    app.config.globalProperties.$notification = notification
    app.provide('$message', message)
    app.provide('$notification', notification)

    components.forEach((com): App<Element> => app.use(com))
  },
}
