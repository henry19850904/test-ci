import { ref, computed, onUnmounted, watch, Ref, ComputedRef } from 'vue'
import dayjs from 'dayjs'
// import { watchEffect } from 'vue'
// import usePersistFn from '../usePersistFn';

export type TDate = Date | number | string | undefined

export type Options = {
  targetDate?: TDate
  interval?: number
  onEnd?: () => void
}

export interface FormattedRes {
  days: number
  hours: number
  minutes: number
  seconds: number
  milliseconds: number
}

const calcLeft = (t?: TDate) => {
  if (!t) {
    return 0
  }
  // https://stackoverflow.com/questions/4310953/invalid-date-in-safari
  const left = dayjs(t).valueOf() - new Date().getTime()
  if (left < 0) {
    return 0
  }
  return left
}

const parseMs = (milliseconds: number): FormattedRes => {
  return {
    days: Math.floor(milliseconds / 86400000),
    hours: Math.floor(milliseconds / 3600000) % 24,
    minutes: Math.floor(milliseconds / 60000) % 60,
    seconds: Math.floor(milliseconds / 1000) % 60,
    milliseconds: Math.floor(milliseconds) % 1000,
  }
}

const useCountdown: (
  options?: Options
) => { timeLeft: Ref<number>; target: Ref<TDate>; formattedRes: ComputedRef<FormattedRes> } = options => {
  const { targetDate, interval = 1000, onEnd } = options || {}

  const target = ref<TDate>(targetDate)
  const timeLeft = ref(calcLeft(targetDate))
  const intervalRef = ref(interval)

  const onEndPersistFn = () => {
    if (onEnd) {
      onEnd()
    }
  }

  let timer: NodeJS.Timeout

  watch([target, intervalRef], () => {
    if (!target.value) {
      // for stop
      timeLeft.value = 0
      return
    }

    // 立即执行一次
    timeLeft.value = calcLeft(target.value)

    timer = setInterval(() => {
      const targetLeft = calcLeft(target.value)
      timeLeft.value = targetLeft
      if (targetLeft === 0) {
        clearInterval(timer)
        onEndPersistFn()
      }
    }, intervalRef.value)
  })

  onUnmounted(() => timer && clearInterval(timer))

  const formattedRes = computed(() => {
    return parseMs(timeLeft.value)
  })

  return { timeLeft, target, formattedRes }
}

export default useCountdown
