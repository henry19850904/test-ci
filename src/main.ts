import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store, { key } from './store'
// with polyfills
import 'core-js/stable'
import 'regenerator-runtime/runtime'
// 初始化store
// import bootstrap from "./core/bootstrap";
import extComponents from './core/lazyUse' // use lazy load components
import IconFont from '@/core/plugins/iconFont'
import 'normalize.css'
// import Ant from 'ant-design-vue/es';
// import 'ant-design-vue/dist/antd.css';
import './permission' // 待修改
import './styles/global.less'
import infiniteScroll from '@/core/directives/infiniteScroll'
import '@/utils/setScrollStyle'

const app = createApp(App)

app.use(extComponents).use(IconFont).use(store, key).use(router).use(infiniteScroll).mount('#app')
