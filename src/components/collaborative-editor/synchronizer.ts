import { Delta } from 'quill'
import EditorEvents from './editorEvents'
import ReconnectingWebSocket, { UrlProvider } from 'reconnecting-websocket'
import ShareDB, { Doc, Error } from 'sharedb/lib/client'
import Editor from './Editor'
// import { EventHandlerPayload } from './editorEvents'
import Composition from './composition'

class Synchronizer {
  editor: Editor
  debug: boolean
  composition: Composition
  doc: Doc | null
  heartbeat: any
  socket: any

  constructor(editor: Editor, composition: Composition) {
    this.editor = editor
    this.debug = false
    this.composition = composition
  }

  submitDeltaToUpstream(delta: Delta) {
    this.doc?.submitOp(delta, { source: 'user' })
  }

  syncThroughWebsocket(endpoint: UrlProvider, collection: string, docId: string) {
    this.close()

    this.socket = new ReconnectingWebSocket(endpoint)

    let connection = new ShareDB.Connection(this.socket)

    this.syncShareDBDocument(connection.get(collection, docId))

    // Send heartbeat message to keep websocket connection alive

    let self = this

    this.socket.addEventListener('open', () => {
      self.heartbeat = setInterval(() => {
        self.socket.send('{"a":"hs"}')
      }, 5000)
    })

    this.socket.addEventListener('close', () => {
      clearInterval(self.heartbeat)
    })

    return this.socket
  }

  syncShareDBDocument(shareDBDocument: Doc) {
    this.doc = shareDBDocument

    let self = this

    shareDBDocument.subscribe(function (err) {
      if (err) {
        self.log(err)
        throw err
      }

      if (self.doc?.type === null) {
        throw new Error('doc does not exist.')
      }

      self.editor.dispatchEvent(EditorEvents.beforeSync, shareDBDocument)

      self.composition.setEditorContent(self.doc?.data)

      shareDBDocument.on('op', function (delta, source) {
        if (source === 'user') return
        const ops = ((delta as unknown) as Delta).ops as any
        if (!(ops || ops.length === 0)) return

        self.composition.submitToEditor((delta as unknown) as Delta)
      })

      shareDBDocument.on('del', function () {
        // The doc has been deleted.
        // Local session should be terminated.
        self.close()
        self.editor.dispatchEvent(EditorEvents.documentDeleted, shareDBDocument)
      })

      shareDBDocument.on('error', function (err) {
        self.editor.dispatchEvent(EditorEvents.synchronizationError, err)
      })

      // Initialize history recording
      self.editor.quill.getModule('history').init(self.editor)

      self.editor.dispatchEvent(EditorEvents.documentLoaded, shareDBDocument)
    })
  }

  close() {
    if (this.doc) {
      this.doc.destroy()
      this.doc = null
    }

    if (this.socket) {
      this.socket.close()
      this.socket = null
    }
  }

  log(msg: string | Error) {
    if (!this.debug) {
      return
    }

    console.log(msg)
  }
}

export default Synchronizer
