import Quill, { QuillOptionsStatic } from 'quill'
import { EventHandlerPayload, EventHandler } from './editorEvents'
import { UrlProvider } from 'reconnecting-websocket'
import Variable from './blots/variable'
import Chart from './blots/chart'
import { merge } from 'lodash-es'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.bubble.css'
import 'quill/dist/quill.snow.css'
import Authorship, { AuthorshipOptions } from './authorship'
import ImageHandlers from './image-handlers'
import Composition from './composition'
import shareDB from 'sharedb/lib/client'
import richText from 'rich-text'
import QuillImageDropAndPaste from 'quill-image-drop-and-paste'
import ImagePlaceholder from './blots/image-placeholder'
import Synchronizer from './synchronizer'
import History from './modules/history'
import MiksClipboard from './modules/miks-clipboard'

shareDB.types.register(richText.type)

Quill.register('modules/imageDropAndPaste', QuillImageDropAndPaste)
Quill.register(ImagePlaceholder)
Quill.register(Variable)
Quill.register(Chart)
Quill.register('modules/history', History)
Quill.register('modules/clipboard', MiksClipboard)

export type EditorOptions = {
  authorship: AuthorshipOptions
  image: any
}

type EventHandlers = {
  [key: string]: {
    [key: string]: EventHandler
  }
}

class CollaborativeEditor {
  options: Partial<EditorOptions>
  eventHandlers: EventHandlers
  imageHandlers: ImageHandlers
  quill: Quill
  composition: Composition
  synchronizer: Synchronizer
  authorship: Authorship

  constructor(container: string | Element, editorOptions: Partial<EditorOptions>, quillOptions: QuillOptionsStatic) {
    this.options = editorOptions
    this.eventHandlers = {}

    this.imageHandlers = new ImageHandlers(this)

    const options = this.mergeQuillOptions(quillOptions)
    this.quill = new Quill(container, options)

    this.composition = new Composition(this)
    this.synchronizer = new Synchronizer(this, this.composition)
    this.composition.setSynchronizer(this.synchronizer)

    if (editorOptions.authorship) {
      this.authorship = new Authorship(this, this.composition, (editorOptions.authorship || {}) as AuthorshipOptions)
    }

    // Add image upload toolbar button handler
    this.quill.getModule('toolbar').addHandler('image', this.imageHandlers.imageUploadButtonHandler)

    // Initialize clipboard module
    this.quill.getModule('clipboard').setEditor(this)
  }

  mergeQuillOptions(options: QuillOptionsStatic) {
    const self = this

    return merge(options, {
      modules: {
        imageDropAndPaste: {
          handler: self.imageHandlers.imageDropAndPasteHandler,
        },
      },
    })
  }

  syncThroughWebsocket(endpoint: UrlProvider, collection: string, docId: string) {
    this.composition.clear()
    return this.synchronizer.syncThroughWebsocket(endpoint, collection, docId)
  }

  getEditorContents() {
    return this.composition.getEditorContents()
  }

  on(event: string, handler: EventHandler) {
    const handlerId = Math.ceil(Math.random() * 10000)

    if (!this.eventHandlers[event]) {
      this.eventHandlers[event] = {}
    }

    this.eventHandlers[event][handlerId] = handler

    return handlerId
  }

  off(event: string, handlerId: number) {
    if (this.eventHandlers[event] && this.eventHandlers[event][handlerId]) {
      delete this.eventHandlers[event][handlerId]
    }
  }

  dispatchEvent(event: string, payload: EventHandlerPayload) {
    if (this.eventHandlers[event]) {
      Object.keys(this.eventHandlers[event]).forEach(handlerId => {
        const handler = this.eventHandlers[event][handlerId]
        handler(payload)
      })
    }
  }

  close() {
    this.eventHandlers = {}
    this.synchronizer.close()
  }
}

export default CollaborativeEditor
