import { VariableType } from '@/components/collaborative-editor/blots/variable'
import Quill from 'quill'
import Editor from '../Editor'

const Delta = Quill.import('delta')
export interface UseVariableResult {
  insertVariable: (text: string, id: string, type: VariableType, exp: string) => void
  removeVariable: (id: string) => void
}

export default function useVariable(editor: Editor): UseVariableResult {
  function insertVariable(text: string, id: string, type: VariableType, exp: string) {
    const { quill } = editor as Editor
    const getEndIndex = () => quill.getLength() - 1
    quill.insertEmbed(getEndIndex(), 'variable', { id, text, type, exp })
    quill.setSelection(getEndIndex(), 0)
  }

  function removeVariable(id: string) {
    if (!id) return
    const { quill } = editor as Editor
    const el = quill.root.querySelector(`[var-id=${id}]`)
    if (el) {
      const variableBlot = Quill.find(el)
      const index = quill.getIndex(variableBlot)
      quill.updateContents(new Delta().retain(index).delete(1))
    }
  }

  return {
    insertVariable,
    removeVariable,
  }
}
