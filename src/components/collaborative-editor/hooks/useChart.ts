// import { ChartType } from '@/components/collaborative-editor/blots/chart'
import Quill from 'quill'
import Editor from '../Editor'

const Delta = Quill.import('delta')
export interface UseChartResult {
  insertChart: () => void
  removeChart: (id: string) => void
}

export default function useChart(editor: Editor): UseChartResult {
  function insertChart() {
    const { quill } = editor
    const getEndIndex = () => quill.getLength() - 1
    quill.insertEmbed(getEndIndex(), 'chart', {})
    quill.setSelection(getEndIndex(), 0)
  }

  function removeChart(id: string) {
    if (!id) return
    const { quill } = editor
    const el = quill.root.querySelector(`[var-id=${id}]`)
    if (el) {
      const variableBlot = Quill.find(el)
      const index = quill.getIndex(variableBlot)
      quill.updateContents(new Delta().retain(index).delete(1))
    }
  }

  return {
    insertChart,
    removeChart,
  }
}
