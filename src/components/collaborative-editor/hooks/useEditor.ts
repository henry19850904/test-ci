import { onBeforeUnmount } from 'vue'
import { QuillOptionsStatic } from 'quill'
import Editor, { EditorOptions } from '../Editor'
import EditorEvents from '../editorEvents'
import { merge } from 'lodash-es'

type BindVariableClickHandler = (e: MouseEvent) => any

type UseEditorOptions = {
  editorOptions?: Partial<EditorOptions>
  quillOptions?: QuillOptionsStatic
  collaborativeOptions?: { docId: string }
  onVariableClick: BindVariableClickHandler
}

function sync(editor: Editor, docId: string) {
  let websocketEndpoint = `ws://localhost:3000/${docId}`
  if (!docId) return
  editor.syncThroughWebsocket(websocketEndpoint, 'examples', docId)
}

function bindEditorEvents(editor: Editor) {
  editor.on(EditorEvents.imageSkipped, () => {
    console.log('image skipped')
  })

  editor.on(EditorEvents.documentLoaded, () => {
    console.log('document loaded')
  })

  editor.on(EditorEvents.synchronizationError, err => {
    console.log('connection error')
    console.log(err)
  })
}

function bindVariableEvent(editor: Editor, handler: BindVariableClickHandler) {
  const EditorEl = editor.quill.root

  EditorEl.addEventListener('click', variableClickHandler)

  onBeforeUnmount(() => {
    EditorEl.removeEventListener('click', variableClickHandler)
  })

  function variableClickHandler(e: MouseEvent) {
    if ((e.target as HTMLElement).classList.contains('editor-variable')) {
      handler(e)
    }
  }
}

export default function useEditor(container: string, options: UseEditorOptions): Editor {
  // let authors = [
  //   {
  //     id: 1,
  //     name: '张三',
  //   },
  //   {
  //     id: 2,
  //     name: '李四',
  //   },
  //   {
  //     id: 3,
  //     name: '王五',
  //   },
  //   {
  //     id: 4,
  //     name: '周六',
  //   },
  //   {
  //     id: 5,
  //     name: '陈七',
  //   },
  // ]

  // let authorIndex = Math.ceil(Math.random() * 1000) % authors.length
  const editorOptions = merge(
    {
      // authorship: {
      //   author: authors[authorIndex],
      //   authorColor: '#ed5634',
      //   colors: ['#f7b452', '#ef6c91', '#8e6ed5', '#6abc91', '#5ac5c3', '#7297e3', '#9bc86e', '#ebd562', '#d499b9'],
      //   handlers: {
      //     getAuthorInfoById: (authorId: string) => {
      //       return new Promise((resolve, reject) => {
      //         let author = authors.find(a => a.id + '' === authorId)

      //         console.log('user info retrieved from server: ' + authorId)

      //         if (author) {
      //           resolve(author)
      //         } else {
      //           reject('user not found')
      //         }
      //       })
      //     },
      //   },
      // },
      image: {
        handlers: {
          imageDataURIUpload: (dataURI: string) => {
            return new Promise(resolve => {
              resolve(dataURI)
            })
          },
          imageSrcUpload: (src: string) => {
            return new Promise(resolve => {
              resolve(src)
            })
          },
          imageUploadError: (err: string | Error) => {
            console.log('image upload error: ' + err)
          },
        },
      },
    },
    options.editorOptions || {}
  )

  const quillOptions = merge(
    {
      modules: {
        toolbar: '#toolbar',
      },
      theme: 'snow',
    },
    options.quillOptions || {}
  )

  const editor = new Editor(container, editorOptions, quillOptions)
  bindEditorEvents(editor)
  bindVariableEvent(editor, options.onVariableClick)
  if (options.collaborativeOptions) {
    sync(editor, options.collaborativeOptions.docId)
  }
  return editor
}
