import { Delta } from 'quill'
// interface EditorEvents {
//   editorTextChanged: 'editor-text-changed',
//   userTextChanged: 'user-text-changed',
//   upstreamTextChanged: 'upstream-text-changed',
//   documentLoaded: 'document-loaded',
//   documentDeleted: 'document-deleted',
//   beforeSync: 'before-sync',
//   beforeSubmitToUpstream: 'before-submit-to-upstream',
//   synchronizationError: 'synchronization-error',
//   imageSkipped: 'image-skipped',
//   undo: 'undo',
//   redo: 'redo',
//   textChange: 'text-change',
//   selectionChange: 'selection-change',
//   editorChange: 'editor-change',
// }
enum EditorEvents {
  editorTextChanged = 'editor-text-changed',
  userTextChanged = 'user-text-changed',
  upstreamTextChanged = 'upstream-text-changed',
  documentLoaded = 'document-loaded',
  documentDeleted = 'document-deleted',
  beforeSync = 'before-sync',
  beforeSubmitToUpstream = 'before-submit-to-upstream',
  synchronizationError = 'synchronization-error',
  imageSkipped = 'image-skipped',
  undo = 'undo',
  redo = 'redo',
  textChange = 'text-change',
  selectionChange = 'selection-change',
  editorChange = 'editor-change',
}

export type EventHandlerPayload = any

export type EventHandler = (payload: EventHandlerPayload) => void

export default EditorEvents
