import Quill from 'quill'
const Parchment = Quill.import('parchment')
import * as echarts from 'echarts'

type Chart = {
  id: string
  text: string
  type: ChartType
}

export const enum ChartType {
  DataChart,
  CalculateChart,
}

let chartId = 0

class ChartBlot extends Parchment.Embed {
  static blotName = 'chart'
  static tagName = 'div'
  static className = 'editor-chart'

  static create(chartOptions: Chart) {
    const node = super.create()
    node.setAttribute('id', 'editorChart_' + chartId++)

    const chart = echarts.init(node, undefined, {
      width: 200,
      height: 200,
    })
    chart.setOption({
      xAxis: {
        type: 'category',
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      },
      yAxis: {
        type: 'value',
      },
      series: [
        {
          data: [150, 230, 224, 218, 135, 147, 260],
          type: 'line',
        },
      ],
    })
    return node
  }

  // formatAt() {

  // }
}

export default ChartBlot
