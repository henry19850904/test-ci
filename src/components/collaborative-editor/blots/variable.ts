import Quill from 'quill'
// const Embed = Quill.import('blots/embed')
const Parchment = Quill.import('parchment')
import './variable.less'

type Variable = {
  id: string
  text: string
  type: VariableType
  exp: string
}

export const enum VariableType {
  DataVariable,
  CalculateVariable,
}

class VariableBlot extends Parchment.Embed {
  static blotName = 'variable'
  static tagName = 'span'
  static className = 'editor-variable'

  static create(variable: Variable) {
    const node = super.create()
    const exp = ' ' + variable.exp.toString().trim() + ' '
    node.setAttribute('var-id', variable.id)
    node.setAttribute('var-type', variable.type)
    node.setAttribute('var-exp', exp)
    node.setAttribute('contenteditable', false)
    if (variable.type === VariableType.DataVariable) {
      node.classList.add('shallow')
    }
    node.innerText = variable.text
    return node
  }
}

export default VariableBlot
