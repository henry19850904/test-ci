const defaultOptions = [
  {
    class: 'button',
    type: 'header',
    value: 1,
  },
  {
    class: 'button',
    type: 'bold',
  },
  {
    class: 'button',
    type: 'italic',
  },
  {
    class: 'button',
    type: 'underline',
  },
  {
    class: 'button',
    type: 'strike',
  },
  {
    class: 'button',
    type: 'blockquote',
  },
  {
    class: 'button',
    type: 'list',
    value: 'ordered',
  },
  {
    class: 'button',
    type: 'list',
    value: 'bullet',
  },
  {
    class: 'button',
    type: 'align',
    value: 'right',
  },
  {
    class: 'button',
    type: 'align',
    value: 'center',
  },
  {
    class: 'button',
    type: 'align',
    value: 'justify',
  },
]

export default defaultOptions
