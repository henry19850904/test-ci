// eslint-disable
declare module 'SmoothDnd' {
  import type { DefineComponent } from 'vue'
  export const Container: DefineComponent<unknown, unknown, any>
  export const Draggable: DefineComponent<unknown, unknown, any>
}
