import BaseApi from './base'
import { WritingWorkbench } from '@/models/writingWorkbench'
import ApiResult, { PagingParam } from '@/models/apiResult'
import request from '@/utils/request'
import { ApprovalItem } from '@/models/writingWorkbench'
class ApproveListApi extends BaseApi<WritingWorkbench> {
  /**
   *  审批列表
   */
  constructor() {
    super('/approveList')
  }
  getApproveList(params: PagingParam) {
    return this.getPagingList(params)
  }
  // eslint-disable-next-line
  async getApprovalItem() {
    const res = await request({
      url: '/approvalItem',
      method: 'GET',
    })
    return res.data as ApiResult<ApprovalItem>
  }
}
const approveListApi = new ApproveListApi()

export default approveListApi
