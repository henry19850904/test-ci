import BaseApi from './base'
import Subscriptions from '@/models/subscriptions'

class SubscriptionsApi extends BaseApi<Subscriptions> {
  /**
   *  资讯订阅api
   */
  constructor() {
    super('/information/subscriptions')
  }
}

export default new SubscriptionsApi()
