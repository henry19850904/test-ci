/* eslint-disable */
// TODO:暂时忽略检查，有空改下
import request from '@/utils/request'
import { ListResult } from '@/models/apiResult'
import BaseApi from './base'
import { HotNews, Business } from '@/models/hotNews'

class HotNewsApi extends BaseApi<HotNews> {
  /**
   *  热门资讯Api
   */
  constructor() {
    super('/news')
  }
  // eslint-disable-next-line
  async getBusiness(params?: any) {
    const res = await request({
      url: '/business',
      method: 'GET',
      params,
    })
    return res.data as ListResult<Business>
  }
  // eslint-disable-next-line
  async getTerritory(params?: any) {
    const res = await request({
      url: '/territory',
      method: 'GET',
      params,
    })
    return res.data as ListResult<Business>
  }
  // eslint-disable-next-line
  async getCommonFiltrate(params?: any) {
    const res = await request({
      url: '/commonFiltrate',
      method: 'GET',
      params,
    })
    return res.data as ListResult<Business>
  }
  // eslint-disable-next-line
  async getNoteList(params?: any) {
    const res = await request({
      url: '/noteList',
      method: 'GET',
      params,
    })
    return res.data as ListResult<Business>
  }
}
const hotNewsApi = new HotNewsApi()

export default hotNewsApi
