import request from '@/utils/request'
import ApiResult from '@/models/apiResult'
import BaseApi from './base'
import { PersonalInfomation } from '@/models/personal'

class PersonalApi extends BaseApi<PersonalInfomation> {
  /**
   *  个人信息api
   */
  constructor() {
    super('/personal')
  }
  async getPersonalInfo(params?: string) {
    const res = await request({
      url: '/infomation',
      method: 'GET',
      params,
    })
    return res.data as ApiResult<PersonalInfomation>
  }
  // getMyTop() {
  //   return this.get('mytop')
  // }
  // getRecords() {
  //   return this.get('records')
  // }
  // getMyNotes() {
  //   return this.get('mynotes')
  // }
  // getMyCollection() {
  //   return this.get('mycollection')
  // }
}

export default new PersonalApi()
