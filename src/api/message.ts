import BaseApi from './base'
import Message from '@/models/message'
import ReconnectingWebSocket from 'reconnecting-websocket'

class MessageApi extends BaseApi<Message> {
  /**
   * 消息中心
   */
  constructor() {
    super('/admin/index/messageCenters')
  }
  getUnreadMessagesCount() {
    return this.request('/admin/index/messageUnReadNums', {
      method: 'GET',
    })
  }
  listenMessages() {
    const socket = new ReconnectingWebSocket('ws://172.16.1.216:6001/selfToSpecific')
    return socket
  }
}
export default new MessageApi()
