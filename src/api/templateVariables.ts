import BaseApi from './base'
import { CommonVariables } from '@/models/templateVariables'

class TemplateVariablesApi extends BaseApi<CommonVariables> {
  /**
   *  模板变量api
   */
  constructor() {
    super('/templateVariables')
  }
  // 变量列表
  async getTempVarsList(params: { id: string | number }) {
    return this.getAll(params.id)
  }
  // 新建变量
  // async addTemplateVariables(params: CommonVariables) {
  // return this.add(params)
  // }
}

export default new TemplateVariablesApi()
