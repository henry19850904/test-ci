import BaseApi from './base'
import WritingStats from '@/models/writingStats'

class WritingStatsApi extends BaseApi<WritingStats> {
  /**
   *  写作统计
   */
  constructor() {
    super('/write/report/index/write/vital')
    // super('/tasks')
  }
}

export default new WritingStatsApi()
