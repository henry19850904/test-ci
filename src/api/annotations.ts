import BaseApi from './base'
import Annotations from '@/models/annotations'

class AnnotationsApi extends BaseApi<Annotations> {
  /**
   *  批注api
   */
  constructor() {
    super('/write/user/notes/annotations')
  }
}

export default new AnnotationsApi()
