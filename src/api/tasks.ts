import BaseApi from './base'
import Tasks from '@/models/tasks'

class TasksApi extends BaseApi<Tasks> {
  /**
   *  写作任务
   */
  constructor() {
    super('/write/report/index/write/task')
    // super('/tasks')
  }
}

export default new TasksApi()
