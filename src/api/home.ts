import BaseApi from './base'
import Home from '@/models/home'
class HomeApi extends BaseApi<Home> {
  constructor() {
    super('/write/system/custom/home')
  }
}

export default new HomeApi()
