/* eslint-disable */
import request from '@/utils/request'

const userApi = {
  Login: '/admin/sys/accountLogin',
  SmsLogin: '/admin/sys/phoneCodeLogin',
  Logout: '/admin/sys/logout',
  SendSms: '/admin/sys/verificationCodes',
  GetCurrentInfo: '/admin/sys/userPermissions',
  SaveUserTheme: '/admin/theme',
}

/**
 * login func
 * parameter: {
 *     username: '',
 *     password: '',
 *     remember_me: true,
 *     captcha: '12345'
 * }
 * @param parameter
 * @returns {*}
 */
export function login(parameter: any) {
  return request({
    url: userApi.Login,
    method: 'post',
    data: parameter,
  })
}

export function smsLogin(parameter: any) {
  return request({
    url: userApi.SmsLogin,
    method: 'post',
    data: parameter,
  })
}

export function getCurrentUser() {
  return request({
    url: userApi.GetCurrentInfo,
  })
}

export function saveUserTheme(data: any) {
  return request({
    url: userApi.SaveUserTheme,
    method: 'post',
    data,
  })
}

export function sendSms(params: any) {
  return request({
    url: userApi.SendSms,
    method: 'get',
    params,
  })
}

export function logout() {
  return request({
    url: userApi.Logout,
    method: 'post',
  })
}
