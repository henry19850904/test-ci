import request from '@/utils/request'
import { ListResult } from '@/models/apiResult'
import BaseApi from './base'
import { ProjectArticle } from '@/models/smartWriting'
import { Menu } from '@/models/menu'

class SmartWritingApi extends BaseApi<ProjectArticle> {
  /**
   *  个人信息api
   */
  constructor() {
    super('/smartwriting')
  }
  // 项目列表
  async getProjects(params?: string) {
    const res = await request({
      url: '/projects',
      method: 'GET',
      params,
    })
    return res.data as ListResult<Menu>
  }
  // 文章列表
  async getArticleList(params?: string) {
    const res = await request({
      url: '/articleList',
      method: 'GET',
      params,
    })
    return res.data as ListResult<ProjectArticle>
  }
  // 删除文章
  async deleteArticle(params: { id: string | number }) {
    return this.delete(params.id)
  }
}

export default new SmartWritingApi()
