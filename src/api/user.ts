import BaseApi from './base'
import User from '@/models/user'

class UserApi extends BaseApi<User> {
  /**
   *  资讯api
   */
  constructor() {
    super('/users')
  }
  async getCurrentUser() {
    return this.request<User>('current')
  }
}

export default new UserApi()
