import BaseApi from './base'
import { Role } from '@/models/user'

class RoleApi extends BaseApi<Role> {
  /**
   *  资讯api
   */
  constructor() {
    super('/roles')
  }
}

export default new RoleApi()
