import BaseApi from './base'
import { WritingWorkbench, ApprovalRecord } from '@/models/writingWorkbench'
import { PagingParam } from '@/models/apiResult'

class WritingWorkbenchApi extends BaseApi<WritingWorkbench> {
  /**
   *  创作列表
   */
  constructor() {
    super('/writingWorkbench')
  }
  getMyWritingList(params: PagingParam) {
    return this.getPagingList(params)
  }
  getApprovalList() {
    return this.getAll()
  }
}
class ApproveRecordApi extends BaseApi<ApprovalRecord> {
  /**
   *  审批记录
   */
  constructor() {
    super('/approveRecord')
  }
}
const writingWorkbenchApi = new WritingWorkbenchApi()
const approveRecordApi = new ApproveRecordApi()

export { writingWorkbenchApi, approveRecordApi }
