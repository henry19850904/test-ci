import { shallowReactive, ref, watchEffect, Ref, toRaw } from 'vue'
import { useApi, BaseApi } from '@/api'
import { GetPagingListResult } from '@/api/base'
import { KeyValue, ListResultExt } from '@/models/apiResult'

interface UsePagingParams {
  pageSize: number
  pageNum: number
  keyWord?: string
}

// eslint-disable-next-line
export default function usePaging<T = KeyValue>(
  Api: BaseApi<T>,
  { pageNum, pageSize, keyWord }: UsePagingParams = { pageNum: 1, pageSize: 6, keyWord: '' },
  loading?: Ref<boolean>,
  loadedAll: Ref<boolean> = ref(false)
) {
  const _pageNum = ref<number>(pageNum)
  const result = shallowReactive<T[]>([])
  // eslint-disable-next-line
  const delta = ref<any>([])
  const total = ref(0)

  const loadMore = () => {
    const apiResult = useApi<GetPagingListResult<T>>(
      Api.getPagingList({
        pageSize: pageSize,
        pageNum: _pageNum.value,
        keyWord,
      }),
      loading
    )

    watchEffect(() => {
      if (!apiResult.response?.value) return
      const { list = [], total: _total } = apiResult.response.value as ListResultExt<T>
      total.value = _total
      if (result.length < _total) {
        loadedAll.value = false
        _pageNum.value++
        result.push(...(delta.value = toRaw(list) || []))
      } else {
        loadedAll.value = true
      }
    })

    return apiResult
  }

  return {
    loadMore,
    loadedAll,
    total,
    result,
    delta,
    pageNum: _pageNum,
  }
}
