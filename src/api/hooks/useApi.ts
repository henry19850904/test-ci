import { ref, Ref } from 'vue'
import { SUCCESS_CODE } from '@/config/http'
import ApiResult, { KeyValue, ListResult, ListResultExt } from '@/models/apiResult'

/**
 * 接口请求hook
 * @param M Api类方法的返回值类型，如getAll为GetAllResult
 * @param T 返回数据的类型
 * @param apiPromise Api类方法返回的Promise
 * @param loading 外部loading，响应式变量
 * @param lazy 是否不立即请求
 * @returns
 */

interface UseApiResult<T> {
  loading: Ref<boolean>
  response: Ref<T | ListResultExt<T> | undefined>
  error: Ref
  request: () => void
}

function useApi<M extends ApiResult<T> | ListResult<T>, T = KeyValue>(
  apiPromise: Promise<M>,
  loading: Ref<boolean> = ref(false),
  lazy = false
): UseApiResult<T> {
  const response = ref<T | ListResultExt<T>>()
  const error = ref()
  const request = async () => {
    loading.value = true
    const res = await apiPromise
    if (res.code === SUCCESS_CODE) {
      response.value = res.data
    } else {
      error.value = res
    }
    loading.value = false
  }
  if (!lazy) request()

  return {
    loading,
    response,
    error,
    request,
  }
}

export default useApi
