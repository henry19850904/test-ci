import BaseApi from './base'
import Essays from '@/models/essays'

class EssaysApi extends BaseApi<Essays> {
  /**
   *  随笔api
   */
  constructor() {
    super('/write/user/notes/essays')
  }
}

export default new EssaysApi()
