import request from '@/utils/request'
import ApiResult, { KeyValue, ListResult, PagingParam } from '@/models/apiResult'
import { AxiosRequestConfig } from 'axios'

export type GetAllResult<T> = ApiResult<T[]>
export type GetResult<T> = ApiResult<T>
export type GetPagingListResult<T> = ListResult<T>
export type AddResult<T> = ApiResult<T>
export type UpdateResult = ApiResult
export type DeleteResult = ApiResult

export default class<T> {
  constructor(public url: string) {}

  /**
   * 根据条件查询数据列表
   * @param params 查询参数
   * @returns 数组
   */
  // eslint-disable-next-line
  async getAll(params?: any): Promise<GetAllResult<T>> {
    const res = await request({
      url: `${this.url}`,
      method: 'GET',
      params,
    })

    return res.data
  }

  /**
   * 根据条件查询单条数据
   * @param id 主键
   * @returns 单条数据
   */
  async get(id?: number | string): Promise<GetResult<T>> {
    const res = await request({
      url: `${this.url}${id ? '/' + id : ''}`,
      method: 'GET',
    })

    return res.data
  }

  /**
   * 根据条件查询分页数据
   * @param params 查询参数
   * @returns 数组
   */
  async getPagingList(params?: PagingParam & KeyValue): Promise<GetPagingListResult<T>> {
    const res = await request({
      url: `${this.url}`,
      method: 'GET',
      params,
    })

    return res.data
  }

  /**
   * 添加数据
   * @param data 实体模型
   * @returns 操作结果，一般返回主键
   */
  async add<RType = KeyValue>(data: KeyValue): Promise<ApiResult<RType>> {
    const res = await request({
      url: this.url,
      method: 'POST',
      data,
    })

    return res.data
  }

  /**
   * 修改数据
   * @param data 实体模型
   * @param id 主键
   * @returns 操作结果
   */
  async update(data: KeyValue, id?: number | string): Promise<UpdateResult> {
    const res = await request({
      url: `${this.url}${id ? '/' + id : ''}`,
      method: 'PUT',
      data,
    })

    return res.data
  }

  /**
   * 根据条件删除一条或多条数据
   * @param id 主键
   * @returns 操作结果
   */
  // eslint-disable-next-line
  async delete(id?: number | string, params?: any): Promise<DeleteResult> {
    const res = await request({
      url: `${this.url}${id ? '/' + id : ''}`,
      method: 'DELETE',
      data: params,
    })

    return res.data
  }

  /**
   * 自定义请求，子类继承后使用此方法自定义操作
   * @param path 路径
   * @param config axios请求配置
   * @returns 自定义返回值类型
   */
  async request<RType>(path = '', config?: AxiosRequestConfig): Promise<RType> {
    const res = await request({
      // url: `${this.url}/${path}`,
      url: path,
      ...config,
    })

    return res.data
  }
}
