import BaseApi from './base'
import { Materials } from '@/models/materials'

class MaterialsApi extends BaseApi<Materials> {
  /**
   * 素材
   */
  constructor() {
    super('/materials')
  }
}

export default new MaterialsApi()
