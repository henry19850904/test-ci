import BaseApi from './base'
import { WritingWorkbench } from '@/models/writingWorkbench'
import { PagingParam } from '@/models/apiResult'

class PublishedListApi extends BaseApi<WritingWorkbench> {
  /**
   *  审批列表
   */
  constructor() {
    super('/publishedList')
  }
  getPublishedList(params: PagingParam) {
    return this.getPagingList(params)
  }
}
const publishedListApi = new PublishedListApi()

export default publishedListApi
