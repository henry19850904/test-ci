import BaseApi from './base'
import { HotNews } from '@/models/hotNews'

class NewsApi extends BaseApi<HotNews> {
  /**
   *  资讯api
   */
  constructor() {
    super('/news')
  }
  // TODO: 调整
  getNews() {
    return this.getAll()
  }
  getSubscribeNews() {
    return this.get('subscribe')
  }
}

export default new NewsApi()
