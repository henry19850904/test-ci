export {} // 这个必须有，将文件转化为模块

declare global {
  const __ENV__: 'local' | 'development' | 'production'

  interface Window {
    themeVars: { key: string; fileName: string; theme?: string; modifyVars?: { [key: string]: string } }[]
  }
}
