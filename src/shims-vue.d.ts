/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

// declare module "@ant-design-vue/pro-layout" {
//   const content: any;
//   export = content;
// }

// declare module "@/assets/icons/bx-analyse.svg?inline" {
//   const content: string;
//   export default content;
// }

// declare module "@vue/runtime-core" {
//   interface ComponentCustomProperties {
//     $store: Store;
//   }
// }

declare module 'sj_spread' {
  export default function importSpreadJSLib(): Promise<void>
}

declare namespace GC {
  export namespace Spread {
    export namespace Sheets {
      export class Workbook {
        constructor(dom: any, config: any) {}
      }
    }
  }
}

declare module 'wangeditor/dist/editor/history' {
  export class History {
    records: any
  }
}

declare module '@/components/smooth-dnd' {
  import type { DefineComponent } from 'vue'
  export const Container: DefineComponent<{}, {}, any>
  export const Draggable: DefineComponent<{}, {}, any>
}

declare module 'rich-text' {
  const content: any
  export default content
}

declare module 'quill-image-drop-and-paste' {
  const content: any
  export default content
}
declare module '*.less' {
  const content: any
  export default content
}

declare module '*.css' {
  const content: any
  export default content
}

declare module 'webpack-theme-color-replacer/client'
