/**
 * 智能写作
 */

/**
 * 项目文章列表
 */
export class ProjectArticle {
  /**
   * @param id: 文章id
   */
  public id: string
  /**
   * @param key: 文章key值
   */
  public key?: string
  /**
   * @param title: 文章标题
   */
  public title: string
  /**
   * @param createdTime: 文章创建时间
   */
  public createdTime: string
  /**
   * @param status: 文章审批状态
   */
  public status: string
  /**
   * @param content: 文章内容
   */
  public content: string
  /**
   * @param projectId: 所属项目id
   */
  public projectId: string
  /**
   * @param projectName: 所属项目名称
   */
  public projectName: string
  /**
   * @param submitTime: 文章提交时间
   */
  public submitTime: string
  /**
   * @param authorId: 文章作者id
   */
  public authorId: string
  /**
   * @param authorName: 文章作者名称
   */
  public authorName: string
}

/**
 * 项目设置信息
 */
export class ProjectInfo {
  /**
   * @param projectId: 项目id
   */
  public projectId: string

  /**
   * @param projectName: 项目名称
   */
  public projectName: string

  /**
   * @param publishPlaceId: 项目发布位置id
   */
  public publishPlaceId: string

  /**
   * @param publishPlace: 项目发布位置
   */
  public publishPlace: string

  /**
   * @param publishTime: 项目发布时间
   */
  public publishTime: string

  /**
   * @param publishRateId: 项目发布频率id
   */
  public publishRateId: string

  /**
   * @param publishRate: 项目发布频率
   */
  public publishRate: string

  /**
   * @param publishDay: 项目发布具体周几
   */
  public publishDay: string

  /**
   * @param publishDate: 项目发布具体某一天
   */
  public publishDate: string

  /**
   * @param dataObjectId: 项目数据对象id
   */
  public dataObjectId: string

  /**
   * @param dataObject: 项目数据对象
   */
  public dataObject: string

  /**
   * @param approverId: 项目审批人id
   */
  public approverId: string

  /**
   * @param approverName: 项目审批人名称
   */
  public approverName: string
}
