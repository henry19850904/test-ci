/**
 * ppt 相关类
 */

/**
 * 元素位置信息
 */
export class BoxConfig {
  /**
   *  @param x 横坐标位置
   */
  public x: number
  /**
   *  @param y 纵坐标位置
   */
  public y: number
  /**
   *  @param width 元素宽度
   */
  public width: number
  /**
   *  @param height 元素高度
   */
  public height: number
}
/**
 * 文本样式类
 */
export class TextStyle {
  /**
   *  @param ‘font-size’ 字体大小：'1.625em'
   */
  public 'font-size': string
  /**
   *  @param color 字体颜色：'#b74857'
   */
  public color: string
}
/**
 * ppt文本类
 */
export class Textboxes {
  /**
   *  @param id 文本id
   */
  public id: string
  /**
   *  @param key 文本键值
   */
  public key: string
  /**
   *  @param text 文本内容
   */
  public text: string
  /**
   *  @param placeholder 文本html/占位符
   */
  public placeholder: string
  /**
   *  @param config 文本位置
   */
  public config: BoxConfig
  /**
   *  @param textStyle 文本位置
   */
  public textStyle: TextStyle
}
/**
 * ppt指标类
 */
export class Indicators {
  /**
   *  @param indicatorId 指标id
   */
  public indicatorId: null | string
  /**
   *  @param indicator 指标
   */
  public indicator: null | string
  /**
   *  @param config 指标位置
   */
  public config: BoxConfig
}
/**
 * ppt图片类
 */
export class Images {
  /**
   *  @param id 图片id
   */
  public id: null | string
  /**
   *  @param src 图片路径
   */
  public src: null | string
  /**
   *  @param config 图片位置
   */
  public config: BoxConfig
}
/**
 * ppt单页
 */
export class PageTypes {
  /**
   *  @param key 页面键值
   */
  public key: string
  /**
   *  @param name 页面名称
   */
  public name: string
  /**
   *  @param background 页面背景图
   */
  public background: string | undefined
  /**
   *  @param textboxes 页面文本
   */
  public textboxes: Textboxes[]
  /**
   *  @param indicators 页面指标
   */
  public indicators: Indicators[]
  /**
   *  @param images 页面图片
   */
  public images: Images[]
}
/**
 * 增加pageId 的pages类
 */
export class PagesType extends PageTypes {
  /**
   *  @param pageId 页面page id
   */
  public pageId: string
  /**
   *  @param template 页面所属template key
   */
  public template: string | undefined
}

/**
 * ppt模板/页面类
 */
export class TemplatePages {
  /**
   *  @param name 模板名称
   */
  public name: string
  /**
   *  @param key 模板键值
   */
  public key: string
  /**
   *  @param aspectRatio 模板页面比例 4 / 3
   */
  public aspectRatio?: number
  /**
   *  @param pageTypes 模板页面
   */
  public pageTypes: Partial<PageTypes>[]
}
