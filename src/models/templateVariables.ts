/**
 * 模板的变量
 */

/**
 * 数据变量
 */
export class CommonVariables {
  /**
   * @param variableId: 变量id
   */
  public variableId: string | number
  /**
   * @param variableName: 变量名称
   */
  public variableName: string
  /**
   * @param variableDesc: 变量描述
   */
  public variableDesc: string
  /**
   * @param dataValue: 变量示例
   */
  public dataValue: string
  /**
   * @param dataType: 变量值的类型
   */
  public dataType: string
  /**
   * @param variableType: 变量的类型
   */
  public variableType: string
}

/**
 * 新增-运算变量
 */
export class AddOperationalVars extends CommonVariables {
  /**
   * @param dataValueShowStr: 变量示例-详情值
   */
  public dataValueShowStr: string
}
