export enum UseType {
  Normal = 0,
  Analyst = 1,
  Expert = 2,
}
/**
 * 用户实体
 */
export default class User {
  public id: number
  /**
   * 用户管理系统id
   */
  public accId: number
  public userName: string
  public lastName: string
  public userAccount: string
  public userEmail: string
  public userHead: string
  public userPhone: string
  public userType: UseType
  public theme: string
  public analystId: number
  public expertId: number
  /**
   * 行业id
   */
  public industryId: number
  public insName: string
  /**
   * 机构id
   */
  public instId: number
  /**
   * 职业证书
   */
  public practice: string
  public roleIdList: string[]
  public userTeams: string[]
  public roles: string[]
}

/**
 * 权限
 */
export class Role {
  public roleId: number
  public roleName: string
}
