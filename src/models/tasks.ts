/**
 * 写作任务
 */
export default class Tasks {
  public id: number
  /**
   * 研报标题
   */
  public reportTitle: string
  /**
   * 任务描述
   */
  public task: string
  /**
   * 任务分配者
   */
  public taskDistributionUserName: string
  /**
   * 任务工期
   */
  // public period: number
}
