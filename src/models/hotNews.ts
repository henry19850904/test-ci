/**
 * 资讯
 */
export class HotNews {
  /**
   * 时间
   */
  time: string
  href: string
  title: string
  hot: number
  like: number
  avatar: string
  content: string
}

export class Business {
  id: number | string
  /**
   * 搜索关键字
   */
  value: string
}
