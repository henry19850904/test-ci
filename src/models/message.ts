/**
 * 消息模型
 */
export const enum ReadStatus {
  Unread,
  Read,
}

export const enum MessageType {
  Notice = 1,
  PrivateMessage,
  Comments,
}

export const enum noticeType {
  WritingWorkbench = 1,
  SmartWriting,
}
export default class Message {
  /**
   * 消息状态 （0:未读，1:已读）
   */
  public isRead: ReadStatus
  /**
   * 消息主键
   */
  public messageId: number
  /**
   * 消息类型（1: 消息通知 2:私信 3:评论）
   */
  public messageType: MessageType
  /**
   * 消息通知编码
   */
  noticeCode: number
  /**
   * 消息通知类型（1: 写作工作台审批流程消息 2:智能写作审批流程消息）
   */
  noticeType: noticeType
  /**
   * 消息主体
   */
  sendContent: string
  /**
   * 消息发送时间
   */
  sendTime: string
}
