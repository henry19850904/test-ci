/**
 * 素材
 */
export enum MaterialType {
  Image = 1,
}
export class Materials {
  id: number
  /**
   * 素材类型
   */
  type: MaterialType
  /**
   * 图片地址
   */
  src: string
  title: string
  content: string
  collected: boolean
  createdAt: string
  author: string
}
