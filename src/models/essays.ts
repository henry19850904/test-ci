/**
 * 随笔
 */
export default class Essays {
  /**
   * 随笔主键
   */
  public id?: number

  /**
   * 随笔内容
   */
  public noteContent: string

  /**
   * 随笔类型 1批注2随笔
   */
  public noteType?: number

  /**
   * 用户编码
   */
  public userId?: number

  /**
   * 创建时间
   */
  public createTime?: string

  /**
   * 修改人
   */
  public updateBy?: number

  /**
   * 修改时间
   */
  public updateTime?: string
}
