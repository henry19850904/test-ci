/**
 * 批注
 */
export default class Annotations {
  /**
   * 批注id
   */
  public id: number
  /**
   * 批注时间
   */
  public commentTime: string
  /**
   * 原文id
   */
  public informationId: number
  /**
   * 批注内容
   */
  public noteContent: string
  /**
   * 原文内容
   */
  public originalContent: string
}
