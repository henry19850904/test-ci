/**
 * 写作统计
 */
export default class WritingStats {
  /**
   * 正在创作研报数
   */
  nowReportCount: number
  /**
   * 阅读量
   */
  readTotalCount: number
  /**
   * 共创作研报数
   */
  reportTotalCount: number
  /**
   * 用户id
   */
  userId: number
  /**
   * 截至时间
   */
  vitalTime: string
  /**
   * 创作时长总计
   */
  writeTotalTime: number
}
