/**
 * 个人中心
 */

/**
 * 个人信息
 */
export class PersonalInfomation {
  /**
   * @param userId: 用户id
   */
  public userId: string
  /**
   * @param userName: 用户名
   */
  public userName: string
  /**
   * @param organize: 所属组织
   */
  public organize: string
  /**
   * @param tel: 电话
   */
  public tel: string
  /**
   * @param email: 邮箱
   */
  public email: string
  /**
   * @param customerType: 用户类型
   */
  public customerType: string
  /**
   * @param avatar: 用户头像
   */
  public avatar: string
}
