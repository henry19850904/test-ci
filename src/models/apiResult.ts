export type KeyValue = {
  // eslint-disable-next-line
  [key: string]: any
}

/**
 * api操作结果
 */
export default interface ApiResult<T = KeyValue> {
  code: number
  msg: string
  data: T
}

export enum SortOrder {
  asc,
  desc,
}

/**
 * 分页参数
 */
export type PagingParam = {
  pageNum: number | undefined
  pageSize: number | undefined
  sortby?: { [key: string]: string }
}

/**
 * 分页结果列表
 */
export type ListResultExt<T> = { total: number; list: T[] }
export type ListResult<T> = ApiResult<ListResultExt<T>>
