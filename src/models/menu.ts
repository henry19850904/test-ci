/**
 * 左侧导航菜单
 */
export class MenuPath {
  /**
   * @param id: 路由name值
   */
  public name: string
  /**
   * @param path: 路由path值
   */
  public path: string
}

/**
 * 多级菜单信息
 */
export class Menu {
  /**
   * @param key: 菜单key值
   */
  public key: string
  /**
   * @param title: 菜单名称
   */
  public title: string
  /**
   * @param icon: 菜单icon
   */
  public icon: string
  /**
   * @param path: 菜单路由path
   */
  public path: Partial<MenuPath> | string
  /**
   * @param hasBtn: 菜单是否有按钮（智能写作子菜单有）
   */
  public hasBtn: boolean
  /**
   * @param status: 菜单按钮的状态值
   */
  public status: boolean
  /**
   * @param children: 菜单的子菜单
   */
  public children: Menu[]

  /**
   * @param name: 菜单名称
   */
  public name: string
  /**
   * @param redirect: 重定向
   */
  public redirect: string
  /**
   * @param component: 组件
   */
  public component: string

  /**
   * @param parentId: 父id
   */
  public parentId: number

  public Id: number
}
