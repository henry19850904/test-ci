export class WritingWorkbench {
  /**
   * id
   */
  id: number
  /**
   * 标题
   */
  title: string
  author: string
  createTime?: string
  updateTime?: string
  type?: string
}

export class ApprovalItem {
  /**
   * id
   */
  id: number
  /**
   * 报告标题
   */
  title: string
  /**
   * 报告摘要
   */
  summary: string
  /**
   * 报告关键词
   */
  keyword: string
  /**
   * 报告作者
   */
  author: string
  /**
   * 所属分类
   */
  sort: string
  /**
   * 所属栏目
   */
  programa: string
  /**
   * 发布渠道
   */
  channel: [
    {
      title: string // 渠道名称
      id: number
      type: string
      img: string
    }
  ]
  /**
   * 下一任审批人
   */
  nextPeople: [
    {
      id: number
      name: string
    }
  ]
}

/**
 * 审批记录
 */
export class ApprovalRecord {
  /**
   * 时间
   */
  time: string
  /**
   * 审批节点
   */
  order: number
  /**
   * 审批状态
   */
  status: number
  /**
   * 审批人员
   */
  person: string
  /**
   * 审批意见
   */
  opinion: string
}
