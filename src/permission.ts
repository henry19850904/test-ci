import router from './router'
import store from './store'
import storage from 'store'
import NProgress from 'nprogress'
import '@/components/nprogress/index.less'
import { setDocumentTitle, domTitle } from '@/utils/domUtil'
import { ACCESS_TOKEN } from '@/store/mutationTypes'
import intersection from 'lodash/intersection'

NProgress.configure({ showSpinner: false })
const allowList = ['login', 'register', 'registerResult']
const loginRoutePath = '/user/login'
const defaultRoutePath = '/home'

router.beforeEach(async (to, from, next) => {
  NProgress.start()
  to.meta && typeof to.meta.title !== 'undefined' && setDocumentTitle(`${to.meta.title} - ${domTitle}`)
  // 是否已有token
  if (storage.get(ACCESS_TOKEN)) {
    if (to.path === loginRoutePath) {
      next({ path: defaultRoutePath })
      NProgress.done()
    } else {
      // 检查登录用户的角色，使用ts+module不知如何书写
      let user = Reflect.get(store.state, 'user')
      // 登录时后都查询用户权限
      if (!user || !user.roles) {
        await store.dispatch('getUserInfo')
        user = Reflect.get(store.state, 'user')
      }

      if (user.roles.includes(to.path) || intersection(user.roles, to.meta?.pemission as string[]).length > 0) {
        next()
      } else {
        // storage.remove(ACCESS_TOKEN)
        next({ path: loginRoutePath, query: { redirect: to.fullPath } })
        NProgress.done()
      }

      // .catch(() => {
      //   notification.error({
      //     message: '错误',
      //     description: '请求用户信息失败，请重试',
      //   })
      //   // 失败时，获取用户信息失败时，调用登出，来清空历史保留信息
      //   store.dispatch('logout').then(() => {
      //     next({ path: loginRoutePath, query: { redirect: to.fullPath } })
      //   })
      // })
    }
  } else {
    if (allowList.includes(to.name as string)) {
      // 在免登录名单，直接进入
      next()
    } else {
      next({ path: loginRoutePath, query: { redirect: to.fullPath } })
      NProgress.done()
      // if current page is login will not trigger afterEach hook, so manually handle it
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})
