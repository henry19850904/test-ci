// eslint-disable-next-line
import BlankLayout from '@/layouts/BlankLayout.vue'
import UserLayout from '@/layouts/UserLayout.vue'
import { RouteRecordRaw } from 'vue-router'
// import { bxAnaalyse } from "@/core/icons";

export const asyncRouterMap: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'index',
    component: BlankLayout,
    redirect: '/home',
    children: [
      {
        path: '/home',
        name: 'Home',
        component: () => import('@/views/home/index.vue'),
        meta: { title: '首页' },
      },
      {
        path: '/home/config',
        name: 'HomeConfig',
        component: () => import('@/views/home/HomeConfig.vue'),
        meta: { pemission: ['/home'] },
      },
      {
        path: '/hot-news',
        name: 'HotNews',
        component: () => import('@/views/hot-news/index.vue'),
      },
      // {
      //   path: '/writing-workbench',
      //   name: 'WritingWorkbench',
      //   component: () => import('@/views/writing-workbench/index.vue'),
      // },
      {
        path: '/writing-workbench/creation-report',
        name: 'CreationReport',
        component: () => import('@/views/creation-report/index.vue'),
        meta: { pemission: ['/writing-workbench'] },
      },
      {
        path: '/smart-writing',
        name: 'SmartWriting',
        component: () => import('@/views/smart-writing/index.vue'),
        redirect: '/no-projects',
        children: [
          {
            path: '/no-projects',
            name: 'NoProjects',
            component: () => import('@/views/smart-writing/NoProjects.vue'),
            meta: { pemission: ['/smart-writing'] },
          },
          {
            path: '/writing-prolist/:id',
            name: 'WritingProList',
            component: () => import('@/views/smart-writing/ProList.vue'),
            meta: { pemission: ['/smart-writing'] },
          },
          {
            path: '/writing-approval',
            name: 'WritingApproval',
            component: () => import('@/views/smart-writing/MyApproval.vue'),
            meta: { pemission: ['/smart-writing'] },
          },
        ],
      },
      {
        path: '/smart-writing/edit-template',
        name: 'WritingEditTemplate',
        component: () => import('@/views/smart-writing/EditTemplate.vue'),
        meta: { pemission: ['/smart-writing'] },
      },
      {
        path: '/personal-center',
        name: 'PersonalCenter',
        component: () => import('@/views/personal-center/index.vue'),
        redirect: '/personal-infomation',
        children: [
          {
            path: '/personal-infomation',
            name: 'PersonalInfomation',
            component: () => import('@/views/personal-center/PersonalInfomation.vue'),
            meta: { pemission: ['/personal-center'] },
          },
          {
            path: '/personal-mytop',
            name: 'PersonalMyTop',
            component: () => import('@/views/personal-center/MyTop.vue'),
            meta: { pemission: ['/personal-center'] },
          },
          {
            path: '/personal-mycollection',
            name: 'PersonalMyCollection',
            component: () => import('@/views/personal-center/MyTop.vue'),
            meta: { pemission: ['/personal-center'] },
          },
          {
            path: '/personal-myrecords',
            name: 'PersonalMyRecords',
            component: () => import('@/views/personal-center/MyTop.vue'),
            meta: { pemission: ['/personal-center'] },
          },
          {
            path: '/personal-mynotes',
            name: 'PersonalMyNotes',
            component: () => import('@/views/personal-center/MyNotes.vue'),
            meta: { pemission: ['/personal-center'] },
          },
        ],
      },
      {
        path: '/writing-workbench',
        name: 'writing-workbench',
        redirect: {
          name: 'my-writing',
        },
        component: () => import('@/views/writing-workbench/index.vue'),
        children: [
          {
            path: 'my-writing',
            name: 'my-writing',
            component: () => import('@/views/writing-workbench/MyWriting.vue'),
            meta: { pemission: ['/writing-workbench'] },
          },
          {
            path: 'approval-ing',
            name: 'approval-ing',
            component: () => import('@/views/writing-workbench/ApprovalIng.vue'),
            meta: { pemission: ['/writing-workbench'] },
          },
          {
            path: 'the-published',
            name: 'the-published',
            component: () => import('@/views/writing-workbench/PublishedList.vue'),
            meta: { pemission: ['/writing-workbench'] },
          },
          {
            path: 'my-approval',
            name: 'my-approval',
            component: () => import('@/views/writing-workbench/MyApproval.vue'),
            meta: { pemission: ['/writing-workbench'] },
          },
        ],
      },
      {
        path: '/indicator-editor',
        name: 'indicator-editor',
        component: () => import('@/views/indicator-editor/index.vue'),
      },
    ],
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
  },
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap: Array<RouteRecordRaw> = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Login.vue'),
      },
    ],
  },

  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404.vue'),
  },
]
