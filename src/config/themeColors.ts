export interface Theme {
  primaryColor: string
  titleColor: string
  bodyColor: string
  func1Color: string
  func2Color: string
  borderColor: string
  cardBackground: string
  pageBackground: string
  headingColor: string
  itemActiveBackground: string
}

export default {
  light: {
    primaryColor: '#007ae4',
    titleColor: '#354052',
    bodyColor: '#97a3b4',
    func1Color: '#58975f',
    func2Color: '#ff5c47',
    borderColor: '#ebebec',
    cardBackground: '#fffffe',
    pageBackground: '#f0f2f5',
    headingColor: '#002257',

    itemActiveBackground: '#f5f5f5',
  },
  dark: {
    primaryColor: '#008bce',
    titleColor: '#ebedee',
    bodyColor: '#808c9a',
    func1Color: '#00cc88',
    func2Color: '#ff3355',
    borderColor: '#162d48',
    cardBackground: '#001832',
    pageBackground: '#001125',
    headingColor: '#ebedec',

    itemActiveBackground: '#1c3a5a',
  },
} as { [key: string]: Theme }
