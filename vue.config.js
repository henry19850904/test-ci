/* eslint-disable no-var-requires */
const path = require('path')
const webpack = require('webpack')
const createThemeColorReplacerPlugin = require('./config/themePlugin')
const GitRevisionPlugin = require('git-revision-webpack-plugin')
const GitRevision = new GitRevisionPlugin()
const buildDate = JSON.stringify(new Date().toLocaleString())

const { VUE_APP_API_BASE_URL: BASE_API, VUE_APP_API_HOST: API_HOST } = process.env

function resolve(dir) {
  return path.join(__dirname, dir)
}

// check Git
function getGitHash() {
  try {
    return GitRevision.version()
  } catch (e) {}
  return 'unknown'
}

const isProd = process.env.NODE_ENV === 'production'

const assetsCDN = {
  // webpack build externals
  externals: {
    vue: 'Vue',
    'vue-router': 'VueRouter',
    vuex: 'Vuex',
    axios: 'axios',
  },
  css: [],
  // https://unpkg.com/browse/vue@2.6.10/
  js: [
    '//cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.min.js',
    '//cdn.jsdelivr.net/npm/vue-router@3.1.3/dist/vue-router.min.js',
    '//cdn.jsdelivr.net/npm/vuex@3.1.1/dist/vuex.min.js',
    '//cdn.jsdelivr.net/npm/axios@0.19.0/dist/axios.min.js',
  ],
}

// vue.config.js
const vueConfig = {
  configureWebpack: {
    // webpack plugins
    plugins: [
      // Ignore all locale files of moment.js
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new webpack.DefinePlugin({
        APP_VERSION: `"${require('./package.json').version}"`,
        GIT_HASH: JSON.stringify(getGitHash()),
        BUILD_DATE: buildDate,
      }),
    ],
    // if prod, add externals
    // externals: isProd ? assetsCDN.externals : {},
  },

  chainWebpack: config => {
    config.resolve.alias.set('@$', resolve('src'))

    // SpreadJS 路径 alias
    config.resolve.alias.set('sj_spread', path.resolve(__dirname, 'public/static/sj_spread'))

    // 方便开发环境调试quill
    // if (!isProd) {
    //   config.resolve.alias.set('quill$', path.resolve(__dirname, 'node_modules/quill/quill.js'))
    //   config.resolve.alias.set('quill/dist', path.resolve(__dirname, 'node_modules/quill/dist/'))
    // }

    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .oneOf('inline')
      .resourceQuery(/inline/)
      .use('vue-svg-icon-loader')
      .loader('vue-svg-icon-loader')
      .end()
      .end()
      .oneOf('external')
      .use('file-loader')
      .loader('file-loader')
      .options({
        name: 'assets/[name].[hash:8].[ext]',
      })

    // if prod is on
    // assets require on cdn
    if (isProd) {
      config.plugin('html').tap(args => {
        args[0].cdn = assetsCDN
        return args
      })
    }
  },

  css: {
    loaderOptions: {
      less: {
        // 保证组件样式按需加载
        modifyVars: {
          'primary-color': '#007ae4', // 主题色
          'link-color': '#007ae4', // 主题色
          'text-color': '#354052', // 主文本色
          'text-color-secondary': '#97a3b4', // 副文本色，弹窗关闭按钮色
          'heading-color': '#002257', //组件头颜色
          'component-background': '#fffffe',
          'border-color-base': '#ebebec',
          'btn-default-border': '#d9d9d9',
          'border-color-split': '#ebebec',
          // 'btn-default-color': '#ebeded',
          'radio-button-color': '#354052',
          'item-active-bg': '#f5f5f5',
          'select-item-selected-bg': '#007ae3',
        },
        // DO NOT REMOVE THIS LINE
        javascriptEnabled: true,
      },
    },
  },

  devServer: {
    port: 8000,
    proxy: {
      [BASE_API]: {
        target: API_HOST,
        changeOrigin: true,
        // pathRewrite: {
        //   '^/api': '/',
        // },
      },
    },
  },

  // disable source map in production
  productionSourceMap: false,

  lintOnSave: undefined,

  // babel-loader no-ignore node_modules/*
  transpileDependencies: [],

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'less',
      patterns: [__dirname, './src/styles/variables.less'],
    },
  },
}

// preview.pro.loacg.com only do not use in your production;
if (process.env.VUE_APP_PREVIEW === 'true') {
  console.log('VUE_APP_PREVIEW', true)
  // add `ThemeColorReplacer` plugin to webpack plugins
  vueConfig.configureWebpack.plugins.push(createThemeColorReplacerPlugin())
}

module.exports = vueConfig
