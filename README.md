# 新华财经研报系统前端

## 目录结构及说明

```
|- docs # 文档
|- mock # mock接口
  |- sever.js # mock服务
  |- models # 放返回数据
    |- mockEx.js # mockjs扩展
    |- news.js # 相关接口模块
  |- db.js # 数据获取入口，需引入models内数据
  |- routerInterceptor.js # 路由拦截，用于重定向接口
|- public # 公共文件
|- src
  |- api # 接口
    |- base.ts # api基类，其他模块接口需继承它
    |- news.ts # 相关api模块
  |- assets # 放图片字体等资源，按照模块新建文件夹放在对应目录下
    |- images
    |- fonts
  |- components # 放公共组件，复杂组件单独建文件夹，命名大写字母开头
    |- collaborative-editor # 协同编辑器组件
      |- index.vue
    |-smooth-dnd # 拖拽组件
      |- index.js
  |- config # 配置文件
    |- routerConfig.ts # 路由配置文件
  |- core # vue指令、插件和一些公共逻辑
    |- directives # 指令
      |- action.js # 权限指令（待修改）
    |- plugins # 插件
    |- lazyUse.ts # 统一处理组件库按需加载，新组件在这里添加
  |- layouts # 布局相关组件
    |- BlackLayout.vue # 布局组件
  |- models # 定义接口返回数据模型，见下方说明
    |- apiResult.ts # api返回结果基础模型
    |- news.ts # 相关接口返回数据模型
  |- router # 路由定义
    |- index.ts
  |- store # 状态管理，需要按业务拆分模块，开启命名空间
    |- modules # store模块
      |- home # 相关模块
        |- index.ts
    |- rootState.ts # 定义根状态和类型
  |- styles # 放公共样式
    |- antDesignModification.less # 覆盖ant-design-vue样式
    |- global.less # 全局样式
    |- mixins.less # 全局mixins
    |- variables.less # 全局less变量
  |- typings # ts声明文件
    |- global.d.ts # 全局类型声明
  |- utils # 工具函数
    |- request.ts # ajax请求
  |- views # 路由页组件
    |- home # 相关视图模块
      |- components # 页面内部组件
      |- hooks # 公共逻辑
        |- useConfig.ts
      |- index.vue # 入口

```

api、models 文件夹说明：

- api 文件夹放后端接口，为每个模块新建 Api 类，继承 base.ts 中的 BaseApi 类，新接口写在类方法中。
- models 文件夹放接口返回参数的模型类，传入各自模块的 Api 类中。

## 开发规范

见[新华财经研报共享平台-编码规范-前端](http://confluence.shangjian.tech:8090/pages/viewpage.action?pageId=67793942)
