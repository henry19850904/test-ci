setTimeout(() => console.log('1'), 0)
var r1 = new Promise(function (resolve, reject) {
  resolve()
})
r1.then(() => {
  var begin = Date.now()
  while (Date.now() - begin < 1000);
  console.log('2')
  new Promise(function (resolve, reject) {
    resolve()
  }).then(() => console.log('3'))
})
console.log('4')
