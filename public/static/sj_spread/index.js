// 引入 SpreadJS 所依赖的库
let loadingPromise;
let loaded = false;

export default function importSpreadJSLib() {
  // 如果之前加载过了，不需要再加载了，直接执行 cb
  if (loaded) return;

  if (!loadingPromise) {
    loadingPromise = new Promise(resolve => {
      const cssArray = [
        "/static/sj_spread/lib/jquery-ui/css/smoothness/jquery-ui-1.10.3.custom.min.css",
        "/static/sj_spread/lib/gcui.css",
        "/static/sj_spread/lib/spread/gc.spread.sheets.excel2016colorful.12.0.0.css",
        "/static/sj_spread/lib/zTreeStyle/css/zTreeStyle.css",
        "/static/sj_spread/widgets/colorpicker/colorpicker.css",
        "/static/sj_spread/widgets/fontpicker/fontpicker.css",
        "/static/sj_spread/widgets/comboframe/comboframe.css",
        "/static/sj_spread/widgets/borderpicker/borderpicker.css",
        "/static/sj_spread/widgets/sliderpanel/sliderpanel.css",
        "/static/sj_spread/dialogs/dialogs.css",
        "/static/sj_spread/dialogs/dialogs2.css",
        "/static/sj_spread/dialogs/chartDialogs.css",
        "/static/sj_spread/formatDialog/formatDialog.css",
        "/static/sj_spread/ribbon/ribbon.css",
        "/static/sj_spread/formulaBar/formulaBar.css",
        "/static/sj_spread/spreadWrapper/spreadWrapper.css",
        "/static/sj_spread/statusBar/statusBar.css",
        "/static/sj_spread/contextMenu/contextMenu.css",
        "/static/sj_spread/fileMenu/fileMenu.css",
        "/static/sj_spread/chart/chartColorPicker/chart-colorPicker.css",
        "/static/sj_spread/chart/addChartElement/chartAddChartElement.css",
        "/static/sj_spread/chart/chartTypePicker/chartTypePicker.css",
        "/static/sj_spread/chart/chartLayoutPicker/chartLayoutPicker.css",
        "/static/sj_spread/shape/insertShape/insertShapePopup.css",
        "/static/sj_spread/shape/shapeStylePreview/shapeStylePreview.css"
      ];

      const jsArray = [
        "/static/sj_spread/lib/jquery-2.0.2.min.js",
        "/static/sj_spread/lib/jquery-ui/js/jquery-ui-1.10.3.custom.min.js",
        "/static/sj_spread/lib/knockout-2.3.0.min.js",
        "/static/sj_spread/lib/zTreeStyle/js/jquery.ztree.all-3.5.min.js",
        "/static/sj_spread/lib/spread/gc.spread.sheets.all.12.0.0.min.js",
        "/static/sj_spread/lib/spread/plugins/gc.spread.sheets.print.12.0.0.min.js",
        "/static/sj_spread/lib/spread/plugins/gc.spread.sheets.pdf.12.0.0.min.js",
        "/static/sj_spread/lib/spread/plugins/gc.spread.sheets.charts.12.0.0.min.js",
        "/static/sj_spread/lib/spread/plugins/gc.spread.sheets.barcode.12.0.0.min.js",
        "/static/sj_spread/lib/spread/plugins/gc.spread.sheets.shapes.12.0.0.min.js",
        "/static/sj_spread/lib/spread/interop/gc.spread.excelio.12.0.0.min.js",
        "/static/sj_spread/lib/spread/resources/zh/gc.spread.sheets.resources.zh.12.0.0.min.js",
        "/static/sj_spread/lib/FileSaver.min.js",
        "/static/sj_spread/common/app.js",
        "/static/sj_spread/common/resources.js",
        "/static/sj_spread/common/resources.cn.js",
        "/static/sj_spread/widgets/gcui/gcui.js",
        "/static/sj_spread/widgets/colorpicker/colorpicker.js",
        "/static/sj_spread/widgets/fontpicker/fontpicker.js",
        "/static/sj_spread/widgets/comboframe/comboframe.js",
        "/static/sj_spread/widgets/borderpicker/borderpicker.js",
        "/static/sj_spread/widgets/sliderpanel/sliderpanel.js",
        "/static/sj_spread/chart/chart-templates.js",
        "/static/sj_spread/common/colorHelper.js",
        "/static/sj_spread/common/util.js",
        "/static/sj_spread/common/asyncLoader.js",
        "/static/sj_spread/common/metadata.js",
        "/static/sj_spread/spreadWrapper/spreadMeta.js",
        "/static/sj_spread/spreadWrapper/spreadWrapper.js",
        "/static/sj_spread/spreadWrapper/spreadActions.js",
        "/static/sj_spread/spreadWrapper/actions.js",
        "/static/sj_spread/spreadWrapper/ceUtility.js",
        "/static/sj_spread/spreadWrapper/spreadUtility.js",
        "/static/sj_spread/dialogs/baseDialog.js",
        "/static/sj_spread/dialogs/dialogs.js",
        "/static/sj_spread/dialogs/dialogs2.js",
        "/static/sj_spread/dialogs/chartDialogs.js",
        "/static/sj_spread/formatDialog/formatDialog.js",
        "/static/sj_spread/formulaBar/formulaBar.js",
        "/static/sj_spread/statusBar/statusBar.js",
        "/static/sj_spread/chartPreviewer/chartPreviewer.js",
        "/static/sj_spread/chart/addChartElement/chartAddChartElement.js",
        "/static/sj_spread/chart/chartColorPicker/chart-colorPicker.js",
        "/static/sj_spread/chart/chartLayoutPicker/chartLayoutPicker.js",
        "/static/sj_spread/chart/chartTypePicker/chartTypePicker.js",
        "/static/sj_spread/chart/chartSliderPanel.js",
        "/static/sj_spread/chart/chartWrapper.js",
        "/static/sj_spread/shape/utils.js",
        "/static/sj_spread/shape/setShapeWidthAndHeight.js",
        "/static/sj_spread/shape/shapeStyleTemplates.js",
        "/static/sj_spread/shape/insertShape/insertShapePopup.js",
        "/static/sj_spread/shape/insertShape/insertShapePreview.js",
        "/static/sj_spread/shape/shapeStylePreview/shapeStylePreview.js",
        "/static/sj_spread/shape/shapeSliderPanel.js",
        "/static/sj_spread/ribbon/ribbon.js",
        "/static/sj_spread/contextMenu/contextMenu.js",
        "/static/sj_spread/fileMenu/fileMenu.js",
      ]; // 最后优先级执行的队列

      // CSS
      for (let css of cssArray) {
        let link = document.createElement("link");
        link.rel = "stylesheet";
        link.href = css;
        document.head.appendChild(link);
      }

      // JS
      remainRequest();
      function remainRequest() {
        if (jsArray.length) {
          let script = document.createElement("script");
          script.src = jsArray.shift();
          document.body.appendChild(script);
          script.onload = remainRequest;
          script.onerror = remainRequest;
        } else {
          resolve();
          loaded = true;
        }
      }
    });
  }
  return loadingPromise;
}