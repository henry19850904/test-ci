# SJ_SPREAD（EXCEL插件）
## 使用方法
通过submodule安装，务必安装到public/static目录下
```
git submodule add ssh://git@bitbucket.shangjian.tech:7999/fron/sj_spread.git public/static/sj_spread
```
如果只是使用excel表格控件，在index.html中引入以下文件
```
<link href="/static/sj_spread/lib/spread/gc.spread.sheets.excel2016colorful.12.0.0.css" rel="stylesheet" type="text/css" />

<script defer src='/static/sj_spread/lib/spread/gc.spread.sheets.all.12.0.0.min.js' type='text/javascript'></script>
<script defer src="/static/sj_spread/lib/spread/interop/gc.spread.excelio.12.0.0.min.js" type='text/javascript'></script>
```

如果需要使用excel在线编辑器

具体参阅：https://gcdn.grapecity.com.cn/forum.php?mod=viewthread&tid=56440&extra=page%3D1%26filter%3Dtypeid%26typeid%3D274

####需要注意:注释掉的3个样式文件有可能会覆盖全局样式，需要单独处理后引用
```
<!-- Libraries -->
<link href="/static/sj_spread/lib/jquery-ui/css/smoothness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/lib/gcui.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/lib/spread/gc.spread.sheets.excel2016colorful.12.0.0.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/lib/zTreeStyle/css/zTreeStyle.css" rel="stylesheet" type="text/css" />

<!--  <link href="/static/sj_spread/common/common.css" rel="stylesheet" type="text/css" />-->
<link href="/static/sj_spread/widgets/colorpicker/colorpicker.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/widgets/fontpicker/fontpicker.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/widgets/comboframe/comboframe.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/widgets/borderpicker/borderpicker.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/widgets/sliderpanel/sliderpanel.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/dialogs/dialogs.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/dialogs/dialogs2.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/dialogs/chartDialogs.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/formatDialog/formatDialog.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/ribbon/ribbon.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/formulaBar/formulaBar.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/spreadWrapper/spreadWrapper.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/statusBar/statusBar.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/contextMenu/contextMenu.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/fileMenu/fileMenu.css" rel="stylesheet" type="text/css" />
<!--  <link href="/static/sj_spread/index/index.css" rel="stylesheet" type="text/css" />-->
<link href="/static/sj_spread/chart/chartColorPicker/chart-colorPicker.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/chart/addChartElement/chartAddChartElement.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/chart/chartTypePicker/chartTypePicker.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/chart/chartLayoutPicker/chartLayoutPicker.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/shape/insertShape/insertShapePopup.css" rel="stylesheet" type="text/css" />
<link href="/static/sj_spread/shape/shapeStylePreview/shapeStylePreview.css" rel="stylesheet" type="text/css" />

<!-- CN special -->
<!--  <link href="/static/sj_spread/common/local.cn.css" rel="stylesheet">-->


<!-- Libraries -->
<script src="/static/sj_spread/lib/jquery-2.0.2.min.js"></script>
<script src="/static/sj_spread/lib/jquery-ui/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/static/sj_spread/lib/knockout-2.3.0.min.js"></script>
<script src="/static/sj_spread/lib/zTreeStyle/js/jquery.ztree.all-3.5.min.js"></script>
<script src="/static/sj_spread/lib/spread/gc.spread.sheets.all.12.0.0.min.js"></script>
<script src="/static/sj_spread/lib/spread/plugins/gc.spread.sheets.print.12.0.0.min.js"></script>
<script src="/static/sj_spread/lib/spread/plugins/gc.spread.sheets.pdf.12.0.0.min.js"></script>
<script src="/static/sj_spread/lib/spread/plugins/gc.spread.sheets.charts.12.0.0.min.js"></script>
<script src="/static/sj_spread/lib/spread/plugins/gc.spread.sheets.barcode.12.0.0.min.js"></script>
<script src="/static/sj_spread/lib/spread/plugins/gc.spread.sheets.shapes.12.0.0.min.js"></script>
<script src="/static/sj_spread/lib/spread/interop/gc.spread.excelio.12.0.0.min.js"></script>
<script src="/static/sj_spread/lib/spread/resources/zh/gc.spread.sheets.resources.zh.12.0.0.min.js"></script>
<script src="/static/sj_spread/lib/FileSaver.min.js"></script>

<script src="/static/sj_spread/common/app.js"></script>
<script src="/static/sj_spread/common/resources.js"></script>
<script src="/static/sj_spread/common/resources.cn.js"></script>
<script src="/static/sj_spread/widgets/gcui/gcui.js"></script>
<script src="/static/sj_spread/widgets/colorpicker/colorpicker.js"></script>
<script src="/static/sj_spread/widgets/fontpicker/fontpicker.js"></script>
<script src="/static/sj_spread/widgets/comboframe/comboframe.js"></script>
<script src="/static/sj_spread/widgets/borderpicker/borderpicker.js"></script>
<script src="/static/sj_spread/widgets/sliderpanel/sliderpanel.js"></script>
<script src="/static/sj_spread/chart/chart-templates.js"></script>
<script src="/static/sj_spread/common/colorHelper.js"></script>
<script src="/static/sj_spread/common/util.js"></script>
<script src="/static/sj_spread/common/asyncLoader.js"></script>
<script src="/static/sj_spread/common/metadata.js"></script>
<script src="/static/sj_spread/spreadWrapper/spreadMeta.js"></script>
<script src="/static/sj_spread/spreadWrapper/spreadWrapper.js"></script>
<script src="/static/sj_spread/spreadWrapper/spreadActions.js"></script>
<script src="/static/sj_spread/spreadWrapper/actions.js"></script>
<script src="/static/sj_spread/spreadWrapper/ceUtility.js"></script>
<script src="/static/sj_spread/spreadWrapper/spreadUtility.js"></script>
<script src="/static/sj_spread/dialogs/baseDialog.js"></script>
<script src="/static/sj_spread/dialogs/dialogs.js"></script>
<script src="/static/sj_spread/dialogs/dialogs2.js"></script>
<script src="/static/sj_spread/dialogs/chartDialogs.js"></script>
<script src="/static/sj_spread/formatDialog/formatDialog.js"></script>
<script src="/static/sj_spread/formulaBar/formulaBar.js"></script>
<script src="/static/sj_spread/statusBar/statusBar.js"></script>
<script src="/static/sj_spread/chartPreviewer/chartPreviewer.js"></script>
<script src="/static/sj_spread/chart/addChartElement/chartAddChartElement.js"></script>
<script src="/static/sj_spread/chart/chartColorPicker/chart-colorPicker.js"></script>
<script src="/static/sj_spread/chart/chartLayoutPicker/chartLayoutPicker.js"></script>
<script src="/static/sj_spread/chart/chartTypePicker/chartTypePicker.js"></script>
<script src="/static/sj_spread/chart/chartSliderPanel.js"></script>
<script src="/static/sj_spread/chart/chartWrapper.js"></script>

<script src="/static/sj_spread/shape/utils.js"></script>
<script src="/static/sj_spread/shape/setShapeWidthAndHeight.js"></script>
<script src="/static/sj_spread/shape/shapeStyleTemplates.js"></script>
<script src="/static/sj_spread/shape/insertShape/insertShapePopup.js"></script>
<script src="/static/sj_spread/shape/insertShape/insertShapePreview.js"></script>
<script src="/static/sj_spread/shape/shapeStylePreview/shapeStylePreview.js"></script>
<script src="/static/sj_spread/shape/shapeSliderPanel.js"></script>
<script src="/static/sj_spread/ribbon/ribbon.js"></script>
<script src="/static/sj_spread/contextMenu/contextMenu.js"></script>
<script src="/static/sj_spread/fileMenu/fileMenu.js"></script>
```
